local Logo = 12781257228
do
	local uikey = game.CoreGui:FindFirstChild("Loading")
	if uikey then
		uikey:Destroy()
	end
end

local Hwid = "?Hwid="..game:GetService("RbxAnalyticsService"):GetClientId()

local Check = {
	["You Web index"] = tostring("https://zeehubkey.newmeec.repl.co/"..Hwid),
	["You Web Discord"] = tostring("https://discord.gg/uDPzRDgqN8"),
	["Logo"] = 12781257228,
	["Name Hub"] = "Zee Hub"
}

_G.Color = Color3.fromRGB(255, 0, 0)

_G.AddKey = {
	["Key"] = "",
}

------------------------------------------------------------------------------------------------------------
----------------------------------------- // ระบบ Key ทั้ง หมด \\ -------------------------------------------
------------------------------------------------------------------------------------------------------------

function LoadKey()
	if readfile and writefile and isfile and isfolder then
		if not isfolder("Zee Hub") then
			makefolder("Zee Hub")
		end
		if not isfolder("Zee Hub/Key/") then
			makefolder("Zee Hub/Key/")
		end
		if not isfile("Zee Hub/Key/PlayerKey.json") then
			writefile("Zee Hub/Key/PlayerKey.json", game:GetService("HttpService"):JSONEncode(_G.AddKey))
		else
			local Decode = game:GetService("HttpService"):JSONDecode(readfile("Zee Hub/Key/PlayerKey.json"))
			for i,v in pairs(Decode) do
				_G.AddKey[i] = v
			end
		end
	else
		return warn("Status : Undetected Executor")
	end
end

function SaveKey()
	if readfile and writefile and isfile and isfolder then
		if not isfile("Zee Hub/Key/PlayerKey.json") then
			LoadKey()
		else
			local Decode = game:GetService("HttpService"):JSONDecode(readfile("Zee Hub/Key/PlayerKey.json"))
			local Array = {}
			for i,v in pairs(_G.AddKey) do
				Array[i] = v
			end
			writefile("Zee Hub/Key/PlayerKey.json", game:GetService("HttpService"):JSONEncode(Array))
		end
	else
		return warn("Status : Undetected Executor")
	end
end

LoadKey()

local Loading = Instance.new("ScreenGui")
local Blur = Instance.new("Frame")
local Main = Instance.new("Frame")
local Logo_Image = Instance.new("ImageLabel")
local Bar = Instance.new("Frame")
local TextBox = Instance.new("TextBox")
local TextLabel_2 = Instance.new("TextLabel")
local NameHub = Instance.new("TextLabel")
local Start = Instance.new("TextButton")
local GetKey = Instance.new("TextButton")
local X = Instance.new("TextButton")
local HowToGetKey = Instance.new("TextButton")
local UICorner4 = Instance.new("UICorner")
local UICorner = Instance.new("UICorner")
local UICorner2 = Instance.new("UICorner")
local UICorner3 = Instance.new("UICorner")
local ButtonColor = Instance.new("UIStroke")
local ButtonColor2 = Instance.new("UIStroke")
local ButtonColor3 = Instance.new("UIStroke")
local ButtonColor4 = Instance.new("UIStroke")
local ButtonColor5 = Instance.new("UIStroke")

--Properties:

Loading.Name = "Loading"
Loading.Parent = game.CoreGui
Loading.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Blur.Name = "Blur"
Blur.Parent = Loading
Blur.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
Blur.BackgroundTransparency = 1
Blur.Size = UDim2.new(1, 0, 1, 0)

Main.Name = "Main"
Main.Parent = Blur
Main.BorderSizePixel = 0
Main.AnchorPoint = Vector2.new(0.5, 0.5)
Main.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
Main.ClipsDescendants = true
Main.Position = UDim2.new(0.5, 0, 0.499241263, 0)
Main.Size = UDim2.new(0, 0, 0, 0)

Main:TweenSize(UDim2.new(0,512,0,245),"Out","Quad",0.4,true)

ButtonColor.Thickness = 3.2
ButtonColor.Name = ""
ButtonColor.Parent = Main
ButtonColor.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
ButtonColor.LineJoinMode = Enum.LineJoinMode.Round
ButtonColor.Color = _G.Color
ButtonColor.Transparency = 0.10

UICorner4.CornerRadius = UDim.new(0, 10)
UICorner4.Parent = Main

Bar.Name = "Bar"
Bar.Parent = Main
Bar.BorderSizePixel = 0
Bar.BackgroundColor3 = _G.Color
Bar.Position = UDim2.new(0, 25, 0, 34)
Bar.Size = UDim2.new(0, 465, 0, 3)

Logo_Image.Name = "Logo_Image"
Logo_Image.Parent = Main
Logo_Image.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Logo_Image.BackgroundTransparency = 1.000
Logo_Image.BorderSizePixel = 0
Logo_Image.Position = UDim2.new(0, 25, 0, 70)
Logo_Image.Size = UDim2.new(0, 120, 0, 120)
Logo_Image.Image = "rbxassetid://"..(Check["Logo"])

NameHub.Name = "NameHub"
NameHub.Parent = Main
NameHub.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
NameHub.BackgroundTransparency = 1.000
NameHub.Position = UDim2.new(0, 155, 0, 0)
NameHub.Size = UDim2.new(0, 200, 0, 50)
NameHub.Font = Enum.Font.ArialBold
NameHub.Text = Check["Name Hub"].."| Youtuber : zuwz "
NameHub.TextColor3 = _G.Color
NameHub.TextSize = 18.000


Start.Name = "Start"
Start.Parent = Main
Start.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
Start.BorderSizePixel = 0
Start.Position = UDim2.new(0, 359, 0, 135)
Start.Size = UDim2.new(0, 105, 0, 45)
Start.Font = Enum.Font.ArialBold
Start.Text = "Okay \n ตกลง"
Start.TextColor3 = Color3.fromRGB(255, 255,255)
Start.TextSize = 16.000

UICorner2.CornerRadius = UDim.new(0, 10)
UICorner2.Parent = Start


GetKey.Name = "GetKey"
GetKey.Parent = Main
GetKey.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
GetKey.BorderSizePixel = 0
GetKey.Position = UDim2.new(0, 195, 0, 135)
GetKey.Size = UDim2.new(0, 105, 0, 45)
GetKey.Font = Enum.Font.ArialBold
GetKey.Text = "GetKey \n หาคีย์"
GetKey.TextColor3 = Color3.fromRGB(255, 255, 255)
GetKey.TextSize = 16.000
GetKey.MouseButton1Click:Connect(function()
	local Hwid = game:GetService("RbxAnalyticsService"):GetClientId()
	setclipboard("https://zeehubkey.newmeec.repl.co/?Hwid="..Hwid)
	game:GetService("StarterGui"):SetCore("SendNotification",{
	    Title = "Copy เสร็จแล้ว",
	    Text = "Zee Hubbbbb",
	     Duration = 20
	})
end)

UICorner.CornerRadius = UDim.new(0, 10)
UICorner.Parent = GetKey

HowToGetKey.Name = "HowToGetKey"
HowToGetKey.Parent = Main
HowToGetKey.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
HowToGetKey.BorderSizePixel = 0
HowToGetKey.Position = UDim2.new(0, 195, 0, 185)
HowToGetKey.Size = UDim2.new(0, 265, 0, 45)
HowToGetKey.Font = Enum.Font.ArialBold
HowToGetKey.Text = "Copy Discord"
HowToGetKey.TextColor3 = Color3.fromRGB(255, 255, 255)
HowToGetKey.TextSize = 14.000
HowToGetKey.MouseButton1Click:Connect(function()
	setclipboard(Check["You Web Discord"])
	wait(.1)
	game:GetService("StarterGui"):SetCore("SendNotification",{
	    Title = "Discord",
	    Text = "Discord copied on your clipboard",
	     Button1 = "Okay",
	     Duration = 20
	})
end)

ButtonColor5.Thickness = 3.2
ButtonColor5.Name = ""
ButtonColor5.Parent = HowToGetKey
ButtonColor5.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
ButtonColor5.LineJoinMode = Enum.LineJoinMode.Round
ButtonColor5.Color = _G.Color
ButtonColor5.Transparency = 1

UICorner4.CornerRadius = UDim.new(0, 10)
UICorner4.Parent = HowToGetKey

TextBox.Parent = Main
TextBox.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
TextBox.BorderSizePixel = 0
TextBox.ClipsDescendants = true
TextBox.Position = UDim2.new(0, 190, 0, 45)
TextBox.Size = UDim2.new(0, 285, 0, 75)
TextBox.ZIndex = 99
TextBox.ClearTextOnFocus = false
TextBox.Font = Enum.Font.ArialBold
TextBox.PlaceholderColor3 = Color3.fromRGB(0, 0, 0)
TextBox.PlaceholderText = "Type here!"
TextBox.Text = ""
TextBox.TextColor3 = Color3.fromRGB(255,255,255)
TextBox.TextSize = 19.00

ButtonColor4.Thickness = 1.6
ButtonColor4.Name = ""
ButtonColor4.Parent = TextBox
ButtonColor4.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
ButtonColor4.LineJoinMode = Enum.LineJoinMode.Round
ButtonColor4.Color = _G.Color
ButtonColor4.Transparency = 0.8

UICorner3.CornerRadius = UDim.new(0, 5)
UICorner3.Parent = TextBox

TextBox.FocusLost:Connect(function()
	_G.Key = TextBox.Text
	_G.AddKey["Key"] = TextBox.Text
end)

local UIKeySysem = game:GetService("CoreGui"):FindFirstChild("Loading") 

function CheckKey(Text)
	register_key = Text
	local HWID_C = game:GetService("RbxAnalyticsService"):GetClientId()
	local request = http_request or request or HttpPost or syn.request
	local Server = request({
		Url = "https://zeehubkey.newmeec.repl.co/verify.php/?Hwid="..HWID_C.."&Key="..register_key,
		Method = "GET"
	}).Body
	
	if string.find(Server,"WHITELIST !") then
		TextBox.TextColor3 = Color3.fromRGB( 0, 255, 0)
		wait(0.5)
		Main:TweenSize(UDim2.new(0,0,0,0),"Out","Quad",0.4,true)
		wait(.2)
		UIKeySysem:Destroy()
	elseif TextBox.Text == "" then
		print("Lol")
	else
		TextBox.Text = "คีย์ผิดโปรดหาคีย์ใหม่"
		TextBox.TextColor3 = Color3.fromRGB( 255, 0, 0)
		wait(1)
		TextBox.Text = "Please enter key"
		TextBox.TextColor3 = Color3.fromRGB(255,255,255)
	end
end

Start.MouseButton1Click:Connect(function()
	CheckKey(_G.AddKey["Key"] or _G.Key)
	SaveKey()
	wait(3)
	TextBox.Text = ""
end)

CheckKey(_G.AddKey["Key"] or _G.Key)
repeat wait() until not game:GetService("CoreGui"):FindFirstChild("Loading")

------------------------------------------------------------------------------------------------------------
-------------------------------------------// BF \\ -----------------------------------------------
------------------------------------------------------------------------------------------------------------

local id = game.PlaceId
if id == 2753915549 or id == 4442272183 or id == 7449423635 then


	_G.Settings = {
		UI = {
			Color = Color3.fromRGB(255, 0, 0),
			Logo2 = 12521641563,
		},
		Main = {
			SelectFastAttack = "Mobile",
			Select_Weapon = "Melee",
			FastAttack = true,
			BringMonster = true,
			BlackScreen = false,
			AutoHaki = true,
			BlackScreen = false,
			WhiteScreen = false,
			PointStats = 1,
			AutoFarm = false,
			AutoFarmFast = false,
			Select_Boss = "",
			AutoFarmBoss = false,
			AutoFarmBossAll = false,
			AutoFarmBossQuest = false,
			Auto_Gear = false,
			Auto_Mini_Yoru = false, 
			AutoSaber = false,
			AutoSaberHop = false,
			AutoAblility = false,
			Auto_New_World = false,
			Auto_Third_World = false,
			AutoFruitMastery = false,
			AutoFarmMasteryGun = false,
			SkillZ = true,
			SkillX = true,
			SkillC = true,
			SkillV = true,
			SkillAll = false,
			HealthMs = 45,
			DelayAttack = 1,
			Distance = 30,
			AutoFactory = false,
			AutoBuyLegendarySword = false,
			Auto_True_Tripls_Katana = false,
			AutoBuyEnchancement = false,
			EnchancementColour_Hop = false,
			AutoMusketeerHat = false,
			Auto_Cake_Prince = false,
			AutoCakePrinceV2 = false,
			Auto_Cursed_Dual_Katana = false,
			Auto_Yama = false,
			AutoFarmBone = false,
			AutoEliteHunter = false,
			AutoEliteHunterHop = false,
			Auto_Full_Moon_Hop = false,
			AutoHolyTorch = false,
			AutoBudySword = false,
			AutoBudySwordHop = false,
			AutoFarmHallow = false,
			AutoFarmBossHallowHop = false,
			AutoObservation = false,
			Select_Stats = "",
			AutoStats = false,
			PointStats = 1,
			SelectIsland = "",
			TeleportIsland = false,
			Select_Buy = "",
			AutoBuyCombat = false,
			Enchanment = false,
			Legendary_Sword = false,
			Chips = "",
			AutoBuyChips = false,
			KillAura = false,
			Next_Islands = false,
			Auto_Awaken = false,
			AutoSartRaid = false,
			BringFruit = false,
			Get_Fruit = false,
			AutoSoulGuitar  = false,
			AutoFullyGodhuman = false,
			AutoFullySuperhuman = false,
			AutoDeathStep = false,
			AutoSharkman = false,
			AutoElectricClaw = false,
			AutoDragonTalon = false,
			Auto_Farm_Chest = false,
			Auto_Farm_Chest_TP = false,
			Tween = 270,
			Mon_Aura = false,
			Mon_Aura_Point = 1000,
			FastAttackO = false,
			SuperFastAttack = false,
			Quest = false,
			Select_Bone_Quest = "nil",
			FastAttackPC = false,
			FastTween = false,
			Auto_Bartilo_Quest = false,
		},
	}
	-- // FN FullMoonHop \\ --
	
	_G.FullMoonHop = nil
	SelectMaterial = nil
	
	_G.Swords = {
		L = {
		["Shisui"] = false,
		["Saddi"] = false,
		["Wando"] = false,
		},
	}
	
	
	if game.PlaceId == 2753915549 then
		W1 = true
	elseif game.PlaceId == 4442272183 then
		W2 = true
	elseif game.PlaceId == 7449423635 then
		W3 = true
	end
	
	if game:GetService("Players").LocalPlayer.PlayerGui.Main:FindFirstChild("ChooseTeam") then
		repeat wait()
			if game:GetService("Players").LocalPlayer.PlayerGui:WaitForChild("Main").ChooseTeam.Visible == true then
				if _G.Team == "Pirate" then
					for i, v in pairs(getconnections(game:GetService("Players").LocalPlayer.PlayerGui.Main.ChooseTeam.Container.Pirates.Frame.ViewportFrame.TextButton.Activated)) do                                                                                                
						v.Function()
					end
				elseif _G.Team == "Marine" then
					for i, v in pairs(getconnections(game:GetService("Players").LocalPlayer.PlayerGui.Main.ChooseTeam.Container.Marines.Frame.ViewportFrame.TextButton.Activated)) do                                                                                                
						v.Function()
					end
				else
					for i, v in pairs(getconnections(game:GetService("Players").LocalPlayer.PlayerGui.Main.ChooseTeam.Container.Pirates.Frame.ViewportFrame.TextButton.Activated)) do                                                                                                
						v.Function()
					end
				end
			end
		until game.Players.LocalPlayer.Team ~= nil and game:IsLoaded()
	end
	
	print("Wait Game Loading")
		
	if game:IsLoaded() then
		pcall(function()
			repeat wait()
				game:GetService("ReplicatedStorage").Effect.Container.Respawn:Destroy()
				game:GetService("ReplicatedStorage").Effect.Container.Death:Destroy()
			until not game:GetService("ReplicatedStorage").Effect.Container:FindFirstChild("Death") or not game:GetService("ReplicatedStorage").Effect.Container:FindFirstChild("Respawn")
		end)
	end
	
	------------ // SaveSetting \\ ------------
	
	function LoadSettings()
		if readfile and writefile and isfile and isfolder then
			if not isfolder("Zee Hub") then
				makefolder("Zee Hub")
			end
			if not isfolder("Zee Hub/Blox Fruits/") then
				makefolder("Zee Hub/Blox Fruits/")
			end
			if not isfile("Zee Hub/Blox Fruits/" .. game.Players.LocalPlayer.Name .. ".json") then
				writefile("Zee Hub/Blox Fruits/" .. game.Players.LocalPlayer.Name .. ".json", game:GetService("HttpService"):JSONEncode(_G.Settings))
			else
				local Decode = game:GetService("HttpService"):JSONDecode(readfile("Zee Hub/Blox Fruits/" .. game.Players.LocalPlayer.Name .. ".json"))
				for i,v in pairs(Decode) do
					_G.Settings[i] = v
				end
			end
		else
			return warn("Status : Undetected Executor")
		end
	end
	
	function SaveSettings()
		if readfile and writefile and isfile and isfolder then
			if not isfile("Zee Hub/Blox Fruits/" .. game.Players.LocalPlayer.Name .. ".json") then
				LoadSettings()
			else
				local Decode = game:GetService("HttpService"):JSONDecode(readfile("Zee Hub/Blox Fruits/" .. game.Players.LocalPlayer.Name .. ".json"))
				local Array = {}
				for i,v in pairs(_G.Settings) do
					Array[i] = v
				end
				writefile("Zee Hub/Blox Fruits/" .. game.Players.LocalPlayer.Name .. ".json", game:GetService("HttpService"):JSONEncode(Array))
			end
		else
			return warn("Status : Undetected Executor")
		end
	end
	
	LoadSettings()
	
	------------ // AutoUpdate \\ ------------
	
	local function QuestCheck()
		local Lvl = game:GetService("Players").LocalPlayer.Data.Level.Value
		if Lvl >= 1 and Lvl <= 9 then
			if tostring(game.Players.LocalPlayer.Team) == "Marines" then
				MobName = "Trainee [Lv. 5]"
				QuestName = "MarineQuest"
				QuestLevel = 1
				Mon = "Trainee"
				NPCPosition = CFrame.new(-2709.67944, 24.5206585, 2104.24585, -0.744724929, -3.97967455e-08, -0.667371571, 4.32403588e-08, 1, -1.07884304e-07, 0.667371571, -1.09201515e-07, -0.744724929)
			elseif tostring(game.Players.LocalPlayer.Team) == "Pirates" then
				MobName = "Bandit [Lv. 5]"
				Mon = "Bandit"
				QuestName = "BanditQuest1"
				QuestLevel = 1
				NPCPosition = CFrame.new(1059.99731, 16.9222069, 1549.28162, -0.95466274, 7.29721794e-09, 0.297689587, 1.05190106e-08, 1, 9.22064114e-09, -0.297689587, 1.19340022e-08, -0.95466274)
			end
			return {
				[1] = QuestLevel,
				[2] = NPCPosition,
				[3] = MobName,
				[4] = QuestName,
				[5] = LevelRequire,
				[6] = Mon,
				[7] = MobCFrame
			}
		end
	
		if Lvl >= 375 and Lvl <= 399 then -- Fishman Warrior
			MobName = "Fishman Warrior [Lv. 375]"
			QuestName = "FishmanQuest"
			QuestLevel = 1
			Mon = "Fishman Warrior"
			NPCPosition = CFrame.new(61122.5625, 18.4716396, 1568.16504, 0.893533468, 3.95251609e-09, 0.448996574, -2.34327455e-08, 1, 3.78297464e-08, -0.448996574, -4.43233645e-08, 0.893533468)
			MobCFrame = CFrame.new(60955.0625, 48.7988472, 1543.7168, -0.831178129, 6.20639318e-09, -0.556006253, 7.20035302e-08, 1, -9.64761853e-08, 0.556006253, -1.20223305e-07, -0.831178129)
			if _G.AutoFarm and (NPCPosition.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude > 3000 then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(61163.8515625, 11.6796875, 1819.7841796875))
			end
			return {
				[1] = QuestLevel,
				[2] = NPCPosition,
				[3] = MobName,
				[4] = QuestName,
				[5] = LevelRequire,
				[6] = Mon,
				[7] = MobCFrame
			}
		end
	
		if Lvl >= 15 and Lvl <= 29 then
			MobName = "Gorilla [Lv. 20]"
			QuestName = "JungleQuest"
			QuestLevel = 2
			Mon = "Gorilla"
			NPCPosition = CFrame.new(-1604.12012, 36.8521118, 154.23732, 0.0648873374, -4.70858913e-06, -0.997892559, 1.41431883e-07, 1, -4.70933674e-06, 0.997892559, 1.64442184e-07, 0.0648873374)
			MobCFrame = CFrame.new(-1142.0293, 40.5877495, -516.118103, 0.55559355, 7.58965513e-08, 0.831454039, 1.24594361e-08, 1, -9.96073553e-08, -0.831454039, 6.57006538e-08, 0.55559355)
			return {
				[1] = QuestLevel,
				[2] = NPCPosition,
				[3] = MobName,
				[4] = QuestName,
				[5] = LevelRequire,
				[6] = Mon,
				[7] = MobCFrame
			}
		end
	
		if Lvl >= 210 and Lvl <= 249 then
			MobName = "Dangerous Prisoner [Lv. 210]"
			QuestName = "PrisonerQuest"
			QuestLevel = 2
			Mon = "Dangerous Prisoner"
			NPCPosition = CFrame.new(5308.93115, 1.65517521, 475.120514, -0.0894274712, -5.00292918e-09, -0.995993316, 1.60817859e-09, 1, -5.16744869e-09, 0.995993316, -2.06384709e-09, -0.0894274712)
			MobCFrame = CFrame.new(5433.39307, 88.678093, 514.986877, 0.879988372, 0, -0.474995494, 0, 1, 0, 0.474995494, 0, 0.879988372)
			return {
				[1] = QuestLevel,
				[2] = NPCPosition,
				[3] = MobName,
				[4] = QuestName,
				[5] = LevelRequire,
				[6] = Mon,
				[7] = MobCFrame
			}
		end
	
		if Lvl >= 400 and Lvl <= 449 then -- Fishman Commando
			MobName = "Fishman Commando [Lv. 400]"
			QuestName = "FishmanQuest"
			QuestLevel = 2
			Mon = "Fishman Commando"
			NPCPosition = CFrame.new(61122.5625, 18.4716396, 1568.16504, 0.893533468, 3.95251609e-09, 0.448996574, -2.34327455e-08, 1, 3.78297464e-08, -0.448996574, -4.43233645e-08, 0.893533468)
			MobCFrame = CFrame.new(61872.3008, 75.5976562, 1394.83484, -0.922134459, 4.36911973e-09, -0.38686946, -2.54707899e-08, 1, 7.20052e-08, 0.38686946, 7.62523484e-08, -0.922134459)
			if _G.AutoFarm and (NPCPosition.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude > 3000 then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(61163.8515625, 11.6796875, 1819.7841796875))
			end
			return {
				[1] = QuestLevel,
				[2] = NPCPosition,
				[3] = MobName,
				[4] = QuestName,
				[5] = LevelRequire,
				[6] = Mon,
				[7] = MobCFrame
			}
		end
		
		--game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(61163.8515625, 11.6796875, 1819.7841796875))
		local GuideModule = require(game:GetService("ReplicatedStorage").GuideModule)
		local Quests = require(game:GetService("ReplicatedStorage").Quests)
		for i,v in pairs(GuideModule["Data"]["NPCList"]) do
			for i1,v1 in pairs(v["Levels"]) do
				if Lvl >= v1 then
					if not LevelRequire then
						LevelRequire = 0
					end
					if v1 > LevelRequire then
						NPCPosition = i["CFrame"]
						QuestLevel = i1
						LevelRequire = v1
					end
					if #v["Levels"] == 3 and QuestLevel == 3 then
						NPCPosition = i["CFrame"]
						QuestLevel = 2
						LevelRequire = v["Levels"][2]
					end
				end
			end
		end
		for i,v in pairs(Quests) do
			for i1,v1 in pairs(v) do
				if v1["LevelReq"] == LevelRequire and i ~= "CitizenQuest" then
					QuestName = i
					for i2,v2 in pairs(v1["Task"]) do
						MobName = i2
						Mon = string.split(i2," [Lv. ".. v1["LevelReq"] .. "]")[1]
					end
				end
			end
		end
		if QuestName == "MarineQuest2" then
			QuestName = "MarineQuest2"
			QuestLevel = 1
			MobName = "Chief Petty Officer [Lv. 120]"
			Mon = "Chief Petty Officer"
			LevelRequire = 120
		elseif QuestName == "ImpelQuest" then
			QuestName = "PrisonerQuest"
			QuestLevel = 2
			MobName = "Dangerous Prisoner [Lv. 190]"
			Mon = "Dangerous Prisoner"
			LevelRequire = 210
			NPCPosition = CFrame.new(5310.60547, 0.350014925, 474.946594, 0.0175017118, 0, 0.999846935, 0, 1, 0, -0.999846935, 0, 0.0175017118)
		elseif QuestName == "SkyExp1Quest" then
			if QuestLevel == 1 then
				NPCPosition = CFrame.new(-4721.88867, 843.874695, -1949.96643, 0.996191859, -0, -0.0871884301, 0, 1, -0, 0.0871884301, 0, 0.996191859)
			elseif QuestLevel == 2 then
				NPCPosition = CFrame.new(-7859.09814, 5544.19043, -381.476196, -0.422592998, 0, 0.906319618, 0, 1, 0, -0.906319618, 0, -0.422592998)
			end
		elseif QuestName == "Area2Quest" and QuestLevel == 2 then
			QuestName = "Area2Quest"
			QuestLevel = 1
			MobName = "Swan Pirate [Lv. 775]"
			Mon = "Swan Pirate"
			LevelRequire = 775
		end
		MobName = MobName:sub(1,#MobName)
		if not MobName:find("Lv") then
			for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
				MonLV = string.match(v.Name, "%d+")
				if v.Name:find(MobName) and #v.Name > #MobName and tonumber(MonLV) <= Lvl + 50 then
					MobName = v.Name
				end
			end
		end
		if not MobName:find("Lv") then
			for i,v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do
				MonLV = string.match(v.Name, "%d+")
				if v.Name:find(MobName) and #v.Name > #MobName and tonumber(MonLV) <= Lvl + 50 then
					MobName = v.Name
					Mon = a
				end
			end
		end
		for _,v in pairs(workspace._WorldOrigin.EnemySpawns:GetChildren()) do
			if v.Name == MobName then
				MobCFrame = v.CFrame * CFrame.new(0,30,0)
			end
		end
	
		return {
			[1] = QuestLevel,
			[2] = NPCPosition,
			[3] = MobName,
			[4] = QuestName,
			[5] = LevelRequire,
			[6] = Mon,
			[7] = MobCFrame
		}
	end
	
	------------ // QuestBoss \\ ------------
	
	function CheckBossQuest()
		if _G.Settings.Main.Select_Boss == "Saber Expert [Lv. 200] [Boss]" then
			MsBoss = "Saber Expert [Lv. 200] [Boss]"
			NameBoss = "Saber Expert"
			CFrameBoss = CFrame.new(- 1458.89502, 29.8870335, - 50.633564, 0.858821094, 1.13848939e-08, 0.512275636, - 4.85649254e-09, 1, - 1.40823326e-08, - 0.512275636, 9.6063415e-09, 0.858821094)
		elseif _G.Settings.Main.Select_Boss == "The Saw [Lv. 100] [Boss]" then
			MsBoss = "The Saw [Lv. 100] [Boss]"
			NameBoss = "The Saw"
			CFrameBoss = CFrame.new(- 683.519897, 13.8534927, 1610.87854, - 0.290192783, 6.88365773e-08, 0.956968188, 6.98413629e-08, 1, - 5.07531119e-08, - 0.956968188, 5.21077759e-08, - 0.290192783)
		elseif _G.Settings.Main.Select_Boss == "Greybeard [Lv. 750] [Raid Boss]" then
			MsBoss = "Greybeard [Lv. 750] [Raid Boss]"
			NameBoss = "Greybeard"
			CFrameBoss = CFrame.new(- 4955.72949, 80.8163834, 4305.82666, - 0.433646321, - 1.03394289e-08, 0.901083171, - 3.0443168e-08, 1, - 3.17633075e-09, - 0.901083171, - 2.88092288e-08, - 0.433646321)
		elseif _G.Settings.Main.Select_Boss == "The Gorilla King [Lv. 25] [Boss]" then
			MsBoss = "The Gorilla King [Lv. 25] [Boss]"
			NameBoss = "The Gorilla King"
			NameQuestBoss = "JungleQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 1604.12012, 36.8521118, 154.23732, 0.0648873374, - 4.70858913e-06, - 0.997892559, 1.41431883e-07, 1, - 4.70933674e-06, 0.997892559, 1.64442184e-07, 0.0648873374)
			CFrameBoss = CFrame.new(- 1223.52808, 6.27936459, - 502.292664, 0.310949147, - 5.66602516e-08, 0.950426519, - 3.37275488e-08, 1, 7.06501808e-08, - 0.950426519, - 5.40241736e-08, 0.310949147)
		elseif _G.Settings.Main.Select_Boss == "Bobby [Lv. 55] [Boss]" then
			MsBoss = "Bobby [Lv. 55] [Boss]"
			NameBoss = "Bobby"
			NameQuestBoss = "BuggyQuest1"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 1139.59717, 4.75205183, 3825.16211, - 0.959730506, - 7.5857054e-09, 0.280922383, - 4.06310328e-08, 1, - 1.11807175e-07, - 0.280922383, - 1.18718916e-07, - 0.959730506)
			CFrameBoss = CFrame.new(- 1147.65173, 32.5966301, 4156.02588, 0.956680477, - 1.77109952e-10, - 0.29113996, 5.16530874e-10, 1, 1.08897802e-09, 0.29113996, - 1.19218679e-09, 0.956680477)
		elseif _G.Settings.Main.Select_Boss == "Yeti [Lv. 110] [Boss]" then
			MsBoss = "Yeti [Lv. 110] [Boss]"
			NameBoss = "Yeti"
			NameQuestBoss = "SnowQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(1384.90247, 87.3078308, - 1296.6825, 0.280209213, 2.72035177e-08, - 0.959938943, - 6.75690828e-08, 1, 8.6151708e-09, 0.959938943, 6.24481444e-08, 0.280209213)
			CFrameBoss = CFrame.new(1221.7356, 138.046906, - 1488.84082, 0.349343032, - 9.49245944e-08, 0.936994851, 6.29478194e-08, 1, 7.7838429e-08, - 0.936994851, 3.17894653e-08, 0.349343032)
		elseif _G.Settings.Main.Select_Boss == "Mob Leader [Lv. 120] [Boss]" then
			MsBoss = "Mob Leader [Lv. 120] [Boss]"
			NameBoss = "Mob Leader"
			CFrameBoss = CFrame.new(- 2848.59399, 7.4272871, 5342.44043, - 0.928248107, - 8.7248246e-08, 0.371961564, - 7.61816636e-08, 1, 4.44474857e-08, - 0.371961564, 1.29216433e-08, - 0.92824)
		elseif _G.Settings.Main.Select_Boss == "Vice Admiral [Lv. 130] [Boss]" then
			MsBoss = "Vice Admiral [Lv. 130] [Boss]"
			NameBoss = "Vice Admiral"
			NameQuestBoss = "MarineQuest2"
			LevelQuestBoss = 2
			CFrameQuestBoss = CFrame.new(- 5035.42285, 28.6520386, 4324.50293, - 0.0611100644, - 8.08395768e-08, 0.998130739, - 1.57416586e-08, 1, 8.00271849e-08, - 0.998130739, - 1.08217701e-08, - 0.0611100644)
			CFrameBoss = CFrame.new(- 5078.45898, 99.6520691, 4402.1665, - 0.555574954, - 9.88630566e-11, 0.831466436, - 6.35508286e-08, 1, - 4.23449258e-08, - 0.831466436, - 7.63661632e-08, - 0.555574954)
		elseif _G.Settings.Main.Select_Boss == "Warden [Lv. 175] [Boss]" then
			MsBoss = "Warden [Lv. 175] [Boss]"
			NameBoss = "Warden"
			NameQuestBoss = "ImpelQuest"
			LevelQuestBoss = 1
			CFrameQuestBoss = CFrame.new(4851.35059, 5.68744135, 743.251282, - 0.538484037, - 6.68303741e-08, - 0.842635691, 1.38001752e-08, 1, - 8.81300792e-08, 0.842635691, - 5.90851599e-08, - 0.538484037)
			CFrameBoss = CFrame.new(5232.5625, 5.26856995, 747.506897, 0.943829298, - 4.5439414e-08, 0.330433697, 3.47818627e-08, 1, 3.81658154e-08, - 0.330433697, - 2.45289105e-08, 0.943829298)
		elseif _G.Settings.Main.Select_Boss == "Chief Warden [Lv. 200] [Boss]" then
			MsBoss = "Chief Warden [Lv. 200] [Boss]"
			NameBoss = "Chief Warden"
			NameQuestBoss = "ImpelQuest"
			LevelQuestBoss = 2
			CFrameQuestBoss = CFrame.new(4851.35059, 5.68744135, 743.251282, - 0.538484037, - 6.68303741e-08, - 0.842635691, 1.38001752e-08, 1, - 8.81300792e-08, 0.842635691, - 5.90851599e-08, - 0.538484037)
			CFrameBoss = CFrame.new(5232.5625, 5.26856995, 747.506897, 0.943829298, - 4.5439414e-08, 0.330433697, 3.47818627e-08, 1, 3.81658154e-08, - 0.330433697, - 2.45289105e-08, 0.943829298)
		elseif _G.Settings.Main.Select_Boss == "Swan [Lv. 225] [Boss]" then
			MsBoss = "Swan [Lv. 225] [Boss]"
			NameBoss = "Swan"
			NameQuestBoss = "ImpelQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(4851.35059, 5.68744135, 743.251282, - 0.538484037, - 6.68303741e-08, - 0.842635691, 1.38001752e-08, 1, - 8.81300792e-08, 0.842635691, - 5.90851599e-08, - 0.538484037)
			CFrameBoss = CFrame.new(5232.5625, 5.26856995, 747.506897, 0.943829298, - 4.5439414e-08, 0.330433697, 3.47818627e-08, 1, 3.81658154e-08, - 0.330433697, - 2.45289105e-08, 0.943829298)
		elseif _G.Settings.Main.Select_Boss == "Magma Admiral [Lv. 350] [Boss]" then
			MsBoss = "Magma Admiral [Lv. 350] [Boss]"
			NameBoss = "Magma Admiral"
			NameQuestBoss = "MagmaQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 5317.07666, 12.2721891, 8517.41699, 0.51175487, - 2.65508806e-08, - 0.859131515, - 3.91131572e-08, 1, - 5.42026761e-08, 0.859131515, 6.13418294e-08, 0.51175487)
			CFrameBoss = CFrame.new(- 5530.12646, 22.8769703, 8859.91309, 0.857838571, 2.23414389e-08, 0.513919294, 1.53689133e-08, 1, - 6.91265853e-08, - 0.513919294, 6.71978384e-08, 0.857838571)
		elseif _G.Settings.Main.Select_Boss == "Fishman Lord [Lv. 425] [Boss]" then
			MsBoss = "Fishman Lord [Lv. 425] [Boss]"
			NameBoss = "Fishman Lord"
			NameQuestBoss = "FishmanQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(61123.0859, 18.5066795, 1570.18018, 0.927145958, 1.0624845e-07, 0.374700129, - 6.98219367e-08, 1, - 1.10790765e-07, - 0.374700129, 7.65569368e-08, 0.927145958)
			CFrameBoss = CFrame.new(61351.7773, 31.0306778, 1113.31409, 0.999974668, 0, - 0.00714713801, 0, 1.00000012, 0, 0.00714714266, 0, 0.999974549)
		elseif _G.Settings.Main.Select_Boss == "Wysper [Lv. 500] [Boss]" then
			MsBoss = "Wysper [Lv. 500] [Boss]"
			NameBoss = "Wysper"
			NameQuestBoss = "SkyExp1Quest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 7862.94629, 5545.52832, - 379.833954, 0.462944925, 1.45838088e-08, - 0.886386991, 1.0534996e-08, 1, 2.19553424e-08, 0.886386991, - 1.95022007e-08, 0.462944925)
			CFrameBoss = CFrame.new(- 7925.48389, 5550.76074, - 636.178345, 0.716468513, - 1.22915289e-09, 0.697619379, 3.37381434e-09, 1, - 1.70304748e-09, - 0.697619379, 3.57381835e-09, 0.716468513)
		elseif _G.Settings.Main.Select_Boss == "Thunder God [Lv. 575] [Boss]" then
			MsBoss = "Thunder God [Lv. 575] [Boss]"
			NameBoss = "Thunder God"
			NameQuestBoss = "SkyExp2Quest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 7902.78613, 5635.99902, - 1411.98706, - 0.0361216255, - 1.16895912e-07, 0.999347389, 1.44533963e-09, 1, 1.17024491e-07, - 0.999347389, 5.6715117e-09, - 0.0361216255)
			CFrameBoss = CFrame.new(- 7917.53613, 5616.61377, - 2277.78564, 0.965189934, 4.80563429e-08, - 0.261550069, - 6.73089886e-08, 1, - 6.46515304e-08, 0.261550069, 8.00056768e-08, 0.965189934)
		elseif _G.Settings.Main.Select_Boss == "Cyborg [Lv. 675] [Boss]" then
			MsBoss = "Cyborg [Lv. 675] [Boss]"
			NameBoss = "Cyborg"
			NameQuestBoss = "FountainQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(5253.54834, 38.5361786, 4050.45166, - 0.0112687312, - 9.93677887e-08, - 0.999936521, 2.55291371e-10, 1, - 9.93769547e-08, 0.999936521, - 1.37512213e-09, - 0.0112687312)
			CFrameBoss = CFrame.new(6041.82813, 52.7112198, 3907.45142, - 0.563162148, 1.73805248e-09, - 0.826346457, - 5.94632716e-08, 1, 4.26280238e-08, 0.826346457, 7.31437524e-08, - 0.563162148)
			-- New World
		elseif _G.Settings.Main.Select_Boss == "Diamond [Lv. 750] [Boss]" then
			MsBoss = "Diamond [Lv. 750] [Boss]"
			NameBoss = "Diamond"
			NameQuestBoss = "Area1Quest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 424.080078, 73.0055847, 1836.91589, 0.253544956, - 1.42165932e-08, 0.967323601, - 6.00147771e-08, 1, 3.04272909e-08, - 0.967323601, - 6.5768397e-08, 0.253544956)
			CFrameBoss = CFrame.new(- 1736.26587, 198.627731, - 236.412857, - 0.997808516, 0, - 0.0661673471, 0, 1, 0, 0.0661673471, 0, - 0.997808516)
		elseif _G.Settings.Main.Select_Boss == "Jeremy [Lv. 850] [Boss]" then
			MsBoss = "Jeremy [Lv. 850] [Boss]"
			NameBoss = "Jeremy"
			NameQuestBoss = "Area2Quest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(632.698608, 73.1055908, 918.666321, - 0.0319722369, 8.96074881e-10, - 0.999488771, 1.36326533e-10, 1, 8.92172336e-10, 0.999488771, - 1.07732087e-10, - 0.0319722369)
			CFrameBoss = CFrame.new(2203.76953, 448.966034, 752.731079, - 0.0217453763, 0, - 0.999763548, 0, 1, 0, 0.999763548, 0, - 0.0217453763)
		elseif _G.Settings.Main.Select_Boss == "Fajita [Lv. 925] [Boss]" then
			MsBoss = "Fajita [Lv. 925] [Boss]"
			NameBoss = "Fajita"
			NameQuestBoss = "MarineQuest3"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 2442.65015, 73.0511475, - 3219.11523, - 0.873540044, 4.2329841e-08, - 0.486752301, 5.64383384e-08, 1, - 1.43220786e-08, 0.486752301, - 3.99823996e-08, - 0.873540044)
			CFrameBoss = CFrame.new(- 2297.40332, 115.449463, - 3946.53833, 0.961227536, - 1.46645796e-09, - 0.275756449, - 2.3212845e-09, 1, - 1.34094433e-08, 0.275756449, 1.35296352e-08, 0.961227536)
		elseif _G.Settings.Main.Select_Boss == "Don Swan [Lv. 1000] [Boss]" then
			MsBoss = "Don Swan [Lv. 1000] [Boss]"
			NameBoss = "Don Swan"
			CFrameBoss = CFrame.new(2288.802, 15.1870775, 863.034607, 0.99974072, - 8.41247214e-08, - 0.0227668174, 8.4774733e-08, 1, 2.75850098e-08, 0.0227668174, - 2.95079072e-08, 0.99974072)
		elseif _G.Settings.Main.Select_Boss == "Smoke Admiral [Lv. 1150] [Boss]" then
			MsBoss = "Smoke Admiral [Lv. 1150] [Boss]"
			NameBoss = "Smoke Admiral"
			NameQuestBoss = "IceSideQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 6059.96191, 15.9868021, - 4904.7373, - 0.444992423, - 3.0874483e-09, 0.895534337, - 3.64098796e-08, 1, - 1.4644522e-08, - 0.895534337, - 3.91229982e-08, - 0.444992423)
			CFrameBoss = CFrame.new(- 5115.72754, 23.7664986, - 5338.2207, 0.251453817, 1.48345061e-08, - 0.967869282, 4.02796978e-08, 1, 2.57916977e-08, 0.967869282, - 4.54708946e-08, 0.251453817)
		elseif _G.Settings.Main.Select_Boss == "Cursed Captain [Lv. 1325] [Raid Boss]" then
			MsBoss = "Cursed Captain [Lv. 1325] [Raid Boss]"
			NameBoss = "Cursed Captain"
			CFrameBoss = CFrame.new(916.928589, 181.092773, 33422, - 0.999505103, 9.26310495e-09, 0.0314563364, 8.42916226e-09, 1, - 2.6643713e-08, - 0.0314563364, - 2.63653774e-08, - 0.999505103)
		elseif _G.Settings.Main.Select_Boss == "Darkbeard [Lv. 1000] [Raid Boss]" then
			MsBoss = "Darkbeard [Lv. 1000] [Raid Boss]"
			NameBoss = "Darkbeard"
			CFrameBoss = CFrame.new(3876.00366, 24.6882591, - 3820.21777, - 0.976951957, 4.97356325e-08, 0.213458836, 4.57335361e-08, 1, - 2.36868622e-08, - 0.213458836, - 1.33787044e-08, - 0.976951957)
		elseif _G.Settings.Main.Select_Boss == "Order [Lv. 1250] [Raid Boss]" then
			MsBoss = "Order [Lv. 1250] [Raid Boss]"
			NameBoss = "Order"
			CFrameBoss = CFrame.new(- 6221.15039, 16.2351036, - 5045.23584, - 0.380726993, 7.41463495e-08, 0.924687505, 5.85604774e-08, 1, - 5.60738549e-08, - 0.924687505, 3.28013137e-08, - 0.380726993)
		elseif _G.Settings.Main.Select_Boss == "Awakened Ice Admiral [Lv. 1400] [Boss]" then
			MsBoss = "Awakened Ice Admiral [Lv. 1400] [Boss]"
			NameBoss = "Awakened Ice Admiral"
			NameQuestBoss = "FrostQuest"
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(5669.33203, 28.2118053, - 6481.55908, 0.921275556, - 1.25320829e-08, 0.388910472, 4.72230788e-08, 1, - 7.96414241e-08, - 0.388910472, 9.17372489e-08, 0.921275556)
			CFrameBoss = CFrame.new(6407.33936, 340.223785, - 6892.521, 0.49051559, - 5.25310213e-08, - 0.871432424, - 2.76146022e-08, 1, - 7.58250565e-08, 0.871432424, 6.12576301e-08, 0.49051559)
		elseif _G.Settings.Main.Select_Boss == "Tide Keeper [Lv. 1475] [Boss]" then
			MsBoss = "Tide Keeper [Lv. 1475] [Boss]"
			NameBoss = "Tide Keeper"
			NameQuestBoss = "ForgottenQuest"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 3053.89648, 236.881363, - 10148.2324, - 0.985987961, - 3.58504737e-09, 0.16681771, - 3.07832915e-09, 1, 3.29612559e-09, - 0.16681771, 2.73641976e-09, - 0.985987961)
			CFrameBoss = CFrame.new(- 3570.18652, 123.328949, - 11555.9072, 0.465199202, - 1.3857326e-08, 0.885206044, 4.0332897e-09, 1, 1.35347511e-08, - 0.885206044, - 2.72606271e-09, 0.465199202)
			-- Thire World
		elseif _G.Settings.Main.Select_Boss == "Stone [Lv. 1550] [Boss]" then
			MsBoss = "Stone [Lv. 1550] [Boss]"
			NameBoss = "Stone"
			NameQuestBoss = "PiratePortQuest"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 290, 44, 5577)
			CFrameBoss = CFrame.new(- 1085, 40, 6779)
		elseif _G.Settings.Main.Select_Boss == "Island Empress [Lv. 1675] [Boss]" then
			MsBoss = "Island Empress [Lv. 1675] [Boss]"
			NameBoss = "Island Empress"
			NameQuestBoss = "AmazonQuest2"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(5443, 602, 752)
			CFrameBoss = CFrame.new(5659, 602, 244)
		elseif _G.Settings.Main.Select_Boss == "Kilo Admiral [Lv. 1750] [Boss]" then
			MsBoss = "Kilo Admiral [Lv. 1750] [Boss]"
			NameBoss = "Kilo Admiral"
			NameQuestBoss = "MarineTreeIsland"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(2178, 29, - 6737)
			CFrameBoss = CFrame.new(2846, 433, - 7100)
		elseif _G.Settings.Main.Select_Boss == "Captain Elephant [Lv. 1875] [Boss]" then
			MsBoss = "Captain Elephant [Lv. 1875] [Boss]"
			NameBoss = "Captain Elephant"
			NameQuestBoss = "DeepForestIsland"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 13232, 333, - 7631)
			CFrameBoss = CFrame.new(- 13221, 325, - 8405)
		elseif _G.Settings.Main.Select_Boss == "Beautiful Pirate [Lv. 1950] [Boss]" then
			MsBoss = "Beautiful Pirate [Lv. 1950] [Boss]"
			NameBoss = "Beautiful Pirate"
			NameQuestBoss = "DeepForestIsland2"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 12686, 391, - 9902)
			CFrameBoss = CFrame.new(5182, 23, - 20)
		elseif _G.Settings.Main.Select_Boss == "Cake Queen [Lv. 2175] [Boss]" then
			MsBoss = "Cake Queen [Lv. 2175] [Boss]"
			NameBoss = "Cake Queen"
			NameQuestBoss = "IceCreamIslandQuest"             
			LevelQuestBoss = 3
			CFrameQuestBoss = CFrame.new(- 716, 382, - 11010)
			CFrameBoss = CFrame.new(- 821, 66, - 10965)
		elseif _G.Settings.Main.Select_Boss == "rip_indra True Form [Lv. 5000] [Raid Boss]" then
			MsBoss = "rip_indra True Form [Lv. 5000] [Raid Boss]"
			NameBoss = "rip_indra True Form"
			CFrameBoss = CFrame.new(- 5359, 424, - 2735)
		elseif _G.Settings.Main.Select_Boss == "Longma [Lv. 2000] [Boss]" then
			MsBoss = "Longma [Lv. 2000] [Boss]"
			NameBoss = "Longma"
			CFrameBoss = CFrame.new(- 10248.3936, 353.79129, - 9306.34473)
		elseif _G.Settings.Main.Select_Boss == "Soul Reaper [Lv. 2100] [Raid Boss]" then
			MsBoss = "Soul Reaper [Lv. 2100] [Raid Boss]"
			NameBoss = "Soul Reaper"
			CFrameBoss = CFrame.new(- 9515.62109, 315.925537, 6691.12012)
		end
	end
	
	------------ // FN Main \\ ------------
	
	function UnEquipWeapon(Weapon)
		if game.Players.LocalPlayer.Character:FindFirstChild(Weapon) then
			_G.NotAutoEquip = true
			wait(.5)
			game.Players.LocalPlayer.Character:FindFirstChild(Weapon).Parent = game.Players.LocalPlayer.Backpack
			wait(.1)
			_G.NotAutoEquip = false
		end
	end
	
	function EquipWeapon(ToolSe)
		if game.Players.LocalPlayer.Backpack:FindFirstChild(ToolSe) then
			local tool = game.Players.LocalPlayer.Backpack:FindFirstChild(ToolSe)
			wait(.4)
			game.Players.LocalPlayer.Character.Humanoid:EquipTool(tool)
		end
	end
	
	local tweenService = game:GetService("TweenService")
	
	local function tweenModel(model, CF)
		TweenPlay = (CF.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude
		local info = TweenInfo.new(TweenPlay/70, Enum.EasingStyle.Linear)
		local CFrameValue = Instance.new("CFrameValue")
		CFrameValue.Value = model:GetPrimaryPartCFrame()
	
		CFrameValue:GetPropertyChangedSignal("Value"):Connect(function()
			model:SetPrimaryPartCFrame(CFrameValue.Value)
		end)
		
		local tween = tweenService:Create(CFrameValue, info, {Value = CF})
		tween:Play()
		
		tween.Completed:Connect(function()
			CFrameValue:Destroy()
		end)
	end
	
	
	spawn(function()
		game:GetService("RunService").Stepped:Connect(function()
			if TweenButton or _G.Auto_Bartilo_Quest or _G.AutoFarmBoneQuest or _G.Auto_Bowl or _G.MaterialFarm or Auto_Sea_King or _G.AutoElectricClaw or _G.AutoSharkman or _G.AutoFullySuperhuman or _G.Mon_Aura or _G.AutoFullyGodhuman or _G.Auto_Mini_Yoru or _G.Clip or Auto_Gear or _G.AutoObservation or Auto_Mirage_Island or _G.Next_Islands or _G.AutoTushitaBoss or _G.AutoHolyTorch or Auto_Cursed_Dual_Katana or _G.AutoEctoplasm or _G.Auto_Cake_Prince or _G.AutoFarmBone or _G.AutoEliteHunter or _G.AutoCakePrinceV2 or _G.AutoRengoku or _G.Auto_RaceV2 or _G.Auto_Rainbow_Haki or _G.AutoMusketeerHat or _G.Auto_True_Tripls_Katana or _G.AutoFactory or _G.TeleportIsland or _G.AutoPole or _G.AutoFruitMastery or _G.AutoFarmMasteryGun or _G.AutoFarm or _G.AutoFarmBossAll or _G.AutoSaber or _G.AutoFarmBossQuest or _G.AutoFarmBoss or _G.Auto_New_World or _G.Auto_Third_World or _G.Auto_Farm_Chest then
				for _, v in pairs(game.Players.LocalPlayer.Character:GetDescendants()) do
					if v:IsA("BasePart") then
						v.CanCollide = false    
					end
				end
				game:GetService'VirtualUser':CaptureController()
				game:GetService'VirtualUser':Button1Down(Vector2.new(1280, 672))
			end
		end)
	end)
	
	spawn(function()
		while task.wait() do
			pcall(function()
				if TweenButton or _G.Auto_Bartilo_Quest or _G.AutoFarmBoneQuest or _G.Auto_Bowl or _G.MaterialFarm or Auto_Sea_King or _G.AutoElectricClaw or _G.AutoSharkman or _G.AutoFullySuperhuman or _G.Mon_Aura or _G.AutoFullyGodhuman or _G.Auto_Mini_Yoru or _G.Clip or Auto_Gear or _G.AutoObservation or Auto_Mirage_Island or _G.Next_Islands or _G.AutoTushitaBoss or _G.AutoHolyTorch or Auto_Cursed_Dual_Katana or _G.AutoEctoplasm or _G.Auto_Cake_Prince or _G.AutoFarmBone or _G.AutoEliteHunter or _G.AutoCakePrinceV2 or _G.AutoRengoku or _G.Auto_RaceV2 or _G.Auto_Rainbow_Haki or _G.AutoMusketeerHat or _G.Auto_True_Tripls_Katana or _G.AutoFactory or _G.TeleportIsland or _G.AutoPole or _G.AutoFruitMastery or _G.AutoFarmMasteryGun or _G.AutoFarm or _G.AutoFarmBossAll or _G.AutoSaber or _G.AutoFarmBossQuest or _G.AutoFarmBoss or _G.Auto_New_World or _G.Auto_Third_World or _G.Auto_Farm_Chest then
					if not game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip") then
						local Noclip = Instance.new("BodyVelocity")
						Noclip.Name = "BodyClip"
						Noclip.Parent = game:GetService("Players").LocalPlayer.Character.HumanoidRootPart
						Noclip.MaxForce = Vector3.new(100000,100000,100000)
						Noclip.Velocity = Vector3.new(0,0,0)
					end
				else
					game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip"):Destroy()
				end
			end)
		end
	end)
	
	spawn(function()
		while wait() do
			if _G.StertScript then
				if not game.Players.LocalPlayer.Character:FindFirstChild("Highlight") then
					local Highlight = Instance.new("Highlight")
					Highlight.FillColor = Color3.new(0, 255, 0)
					Highlight.OutlineColor = Color3.new(0,255,0)
					Highlight.Parent = game.Players.LocalPlayer.Character
				end
			else
				if game.Players.LocalPlayer.Character:FindFirstChild('Highlight') then
					game.Players.LocalPlayer.Character:FindFirstChild('Highlight'):Destroy()
				end
			end
		end
	end)
	
	local function GetIsLand(...)
		local RealtargetPos = {...}
		local targetPos = RealtargetPos[1]
		local RealTarget
		if type(targetPos) == "vector" then
			RealTarget = targetPos
		elseif type(targetPos) == "userdata" then
			RealTarget = targetPos.Position
		elseif type(targetPos) == "number" then
			RealTarget = CFrame.new(unpack(RealtargetPos))
			RealTarget = RealTarget.p
		end
	
		local ReturnValue
		local CheckInOut = math.huge;
		if game.Players.LocalPlayer.Team then
			for i,v in pairs(game.Workspace._WorldOrigin.PlayerSpawns:FindFirstChild(tostring(game.Players.LocalPlayer.Team)):GetChildren()) do 
				local ReMagnitude = (RealTarget - v:GetModelCFrame().p).Magnitude;
				if ReMagnitude < CheckInOut then
					CheckInOut = ReMagnitude;
					ReturnValue = v.Name
				end
			end
			if ReturnValue then
				return ReturnValue
			end 
		end
	end
	
	------------ // Tween \\ ------------
	function toposition(Point)
		TweeSpeed = _G.Settings.Main.Tween or 225
		local LocalPlayer = game.Players.LocalPlayer 
		TweenPlay = (Point.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude
		pcall(function() 
				tot = game:GetService("TweenService"):Create(LocalPlayer.Character.HumanoidRootPart,TweenInfo.new(TweenPlay/TweeSpeed, Enum.EasingStyle.Linear),{CFrame = Point})
		end)
		
		if TweenPlay > 2000 and _G.Bypass then
			pcall(function()
				tot:Cancel()
				fkwarp = false
	
				if game:GetService("Players")["LocalPlayer"].Data:FindFirstChild("SpawnPoint").Value == tostring(GetIsLand(Point)) then 
					wait(.1)
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TeleportToSpawn")
				elseif game:GetService("Players")["LocalPlayer"].Data:FindFirstChild("LastSpawnPoint").Value == tostring(GetIsLand(Point)) then
					game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid"):ChangeState(15)
					wait(0.1)
					repeat wait() until game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid").Health > 0
				else
					if game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid").Health > 0 then
						if fkwarp == false then
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
						end
						fkwarp = true
					end
					wait(.08)
					game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid"):ChangeState(15)
					repeat wait() until game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid").Health > 0
					wait(.1)
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("SetSpawnPoint")
				end
				wait(0.2)
	
				return
			end)
		end
		
		tot:Play()
	
		if TweenPlay <= 200 then
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
		end
		if not Auto_Mirage_Island then
			if LocalPlayer.Character.Humanoid.Sit == true then 
				LocalPlayer.Character.Humanoid.Sit = false 
			end
		end
		_G.StertScript = true
		if _G.StopTween == true then tot:Cancel();_G.Clip = false end
		if _G.StopTween then
			tot:Cancel()
			BringMobFarm = false
			UseSkillGun = false
			UseSkill = false
		elseif game.Players.LocalPlayer.Character.Humanoid.Health > 0 then
			tot:Play()
		elseif game.Players.LocalPlayer.Character.Humanoid.Health <= 0 then
			tot:Cancel()
			BringMobFarm = false
			UseSkillGun = false
			UseSkill = false
		end
	end	
	
	function StopTween(target)
		if not target then
			_G.StopTween = true
			_G.StertScript = false
			task.wait()
				toposition(game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.CFrame)
			task.wait()
			BringMobFarm = false
			UseSkillGun = false
			UseSkill = false
			toposition(game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.CFrame)
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
			_G.StopTween = false
			_G.StertScript = false
			_G.Clip = false
			if game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip") then
				game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip"):Destroy()
			end
		end
	end
	
	function GetWeaponInventory(Weaponname)
		for i,v in pairs(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("getInventory")) do
			if type(v) == "table" then
				if v.Type == "Sword" then
					if v.Name == Weaponname then
						return true
					end
				end
			end
		end
		return false
	end
	
	function TPPlayer(Point)
		TweeSpeed = _G.Settings.Main.Tween or 300
		local LocalPlayer = game.Players.LocalPlayer 
		TweenPlay = (Point.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude
		pcall(function() 
				tot = game:GetService("TweenService"):Create(LocalPlayer.Character.HumanoidRootPart,TweenInfo.new(TweenPlay/TweeSpeed, Enum.EasingStyle.Linear),{CFrame = Point})
		end);tot:Play()
		if TweenPlay >= 1200 then
			game.Players.LocalPlayer.Character.Head:Destroy()
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point * CFrame.new(0,50,0)
			wait(.2)
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
			wait(.1)
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point * CFrame.new(0,50,0)
			game.Players.LocalPlayer.Character.HumanoidRootPart.Anchored = true
			wait(.1)
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
			wait(0.5)
			game.Players.LocalPlayer.Character.HumanoidRootPart.Anchored = false
			_G.Clip = false
		elseif TweenPlay <= 300 then
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
		end
		if _G.StopTween == true then tot:Cancel();_G.Clip = false end
		if _G.StopTween then
			tot:Cancel()
			BringMobFarm = false
			UseSkillGun = false
			UseSkill = false
		elseif game.Players.LocalPlayer.Character.Humanoid.Health > 0 then
			tot:Play()
		end
	end	
	
	getgenv().TP = function(Pos)
		Distance = (Pos.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude
		if Distance < 250 then
			Speed = 225
		elseif Distance >= 1000 then
			Speed = 150
		end
		game:GetService("TweenService"):Create(
			game:GetService("Players").LocalPlayer.Character.HumanoidRootPart,
			TweenInfo.new(Distance/Speed, Enum.EasingStyle.Linear),
			{CFrame = Pos}
		):Play()
		_G.Clip = true
		wait(Distance/Speed)
		_G.Clip = false
		return
	end
	
	------------ // ByPass \\ ------------
	
	function Bypass(mm)
		print("155522233374")
		print(mm)
	end
	
	------------ // Afk \\ ------------
	
	local vu = game:GetService("VirtualUser")
	game:GetService("Players").LocalPlayer.Idled:connect(function()
		vu:Button2Down(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
		wait(1)
		vu:Button2Up(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
	end)
	
	------------ // AttackMs \\ ------------
	
	function MasteryAttackDelay()
	local Delay = wait
		--game:GetService'VirtualUser':CaptureController()
		Delay(_G.Settings.Main.DelayAttack)
		--game:GetService'VirtualUser':Button1Down(Vector2.new(1280, 672))
	end
	
	------------ // Box \\ ------------
	
	function Box_eventory(Items)
		for i,v in pairs(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("getInventoryWeapons")) do
			if type(v) == "table" then
				if v.Type == "Sword" then
					if v.Name == Items then
						wait()
					end
				end
			end
		end
	end
	
	------------ // Bypass 2 \\ ------------
	
	function BTP(Position)
		game.Players.LocalPlayer.Character.Head:Destroy()
		game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Position
		wait(1)
		game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Position
		game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("SetSpawnPoint")
	end
	
	------------ // Hop \\ ------------
	
	function Hop()
		local PlaceID = game.PlaceId
		local AllIDs = {}
		local foundAnything = ""
		local actualHour = os.date("!*t").hour
		local Deleted = false
		function TPReturner()
			local Site;
			if foundAnything == "" then
				Site = game.HttpService:JSONDecode(game:HttpGet('https://games.roblox.com/v1/games/' .. PlaceID .. '/servers/Public?sortOrder=Asc&limit=100'))
			else
				Site = game.HttpService:JSONDecode(game:HttpGet('https://games.roblox.com/v1/games/' .. PlaceID .. '/servers/Public?sortOrder=Asc&limit=100&cursor=' .. foundAnything))
			end
			local ID = ""
			if Site.nextPageCursor and Site.nextPageCursor ~= "null" and Site.nextPageCursor ~= nil then
				foundAnything = Site.nextPageCursor
			end
			local num = 0;
			for i,v in pairs(Site.data) do
				local Possible = true
				ID = tostring(v.id)
				if tonumber(v.maxPlayers) > tonumber(v.playing) then
					for _,Existing in pairs(AllIDs) do
						if num ~= 0 then
							if ID == tostring(Existing) then
								Possible = false
							end
						else
							if tonumber(actualHour) ~= tonumber(Existing) then
								local delFile = pcall(function()
									AllIDs = {}
									table.insert(AllIDs, actualHour)
								end)
							end
						end
						num = num + 1
					end
					if Possible == true then
						table.insert(AllIDs, ID)
						wait()
						pcall(function()
							wait()
							game:GetService("TeleportService"):TeleportToPlaceInstance(PlaceID, ID, game.Players.LocalPlayer)
						end)
						wait(4)
					end
				end
			end
		end
		function Teleport() 
			while wait() do
				pcall(function()
					TPReturner()
					if foundAnything ~= "" then
						TPReturner()
					end
				end)
			end
		end
		Teleport()
	end
	
	------------ // Box 2 \\ ------------
	
	function Box_Eventory_Style(Style)
		ReturnText = ""
		for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
			if v:IsA("Tool") then
				if v.ToolTip == Style then
					ReturnText = v.Name
				end
			end
		end
		for i ,v in pairs(game.Players.LocalPlayer.Character:GetChildren()) do
			if v:IsA("Tool") then
				if v.ToolTip == Style then
					ReturnText = v.Name
				end
			end
		end
		if ReturnText ~= "" then
			return ReturnText
		else
			return "Not Have"
		end
	end
	
	function LoadItem(FN,Item)
		game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(FN,Item)
	end
	
	function GetWeaponInventory(Weaponname)
		for i,v in pairs(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("getInventory")) do
			if type(v) == "table" then
				if v.Type == "Sword" then
					if v.Name == Weaponname then
						return true
					end
				end
			end
		end
		return false
	end
	
	------------ // Code \\ ------------
	
	Code = {
		"EXP_5B",
		"CONTROL",
		"UPDATE11",
		"XMASEXP",
		"1BILLION",
		"ShutDownFix2",
		"UPD14",
		"STRAWHATMAINE",
		"TantaiGaming",
		"Colosseum",
		"Axiore",
		"Sub2Daigrock",
		"Sky Island 3",
		"Sub2OfficialNoobie",
		"SUB2NOOBMASTER123",
		"THEGREATACE",
		"Fountain City",
		"BIGNEWS",
		"FUDD10",
		"SUB2GAMERROBOT_EXP1",
		"UPD15",
		"2BILLION",
		"UPD16",
		"3BVISITS",
		"fudd10_v2",
		"Starcodeheo",
		"Magicbus",
		"JCWK",
		"Bluxxy",
		"Sub2Fer999",
		"Enyu_is_Pro"
	}
	spawn(function()
		while wait(0) do
			if _G.AutoFarm then
				MyLevel = game.Players.localPlayer.Data.Level.value
				if MyLevel >= 15 then
					for i,v in pairs(Code) do
						pcall(function()
							local args = {
								[1] = v
							}
							game:GetService("ReplicatedStorage").Remotes.Redeem:InvokeServer(unpack(args))
						end)
					end
					break
				end
			end
		end
	end)
	
	------------ // ESP \\ ------------
	
	spawn(function()
		while wait() do
			if FlowerESP then
				UpdateFlowerChams() 
			end
			if DevilFruitESP then
				UpdateDevilChams() 
			end
			if ChestESP then
				UpdateChestChams() 
			end
			if ESPPlayer then
				UpdatePlayerChams()
			end
		end
	end)
	function isnil(thing)
		return (thing == nil)
	end
	local function round(n)
		return math.floor(tonumber(n) + 0.5)
	end
	Number = math.random(1, 1000000)
	function UpdatePlayerChams()
		for i,v in pairs(game:GetService'Players':GetChildren()) do
			pcall(function()
				if not isnil(v.Character) then
					if ESPPlayer then
						if not isnil(v.Character.Head) and not v.Character.Head:FindFirstChild('NameEsp'..Number) then
							local bill = Instance.new('BillboardGui',v.Character.Head)
							bill.Name = 'NameEsp'..Number
							bill.ExtentsOffset = Vector3.new(0, 1, 0)
							bill.Size = UDim2.new(1,200,1,30)
							bill.Adornee = v.Character.Head
							bill.AlwaysOnTop = true
							local name = Instance.new('TextLabel',bill)
							name.Font = "GothamBold"
							name.FontSize = "Size14"
							name.TextWrapped = true
							name.Text = (v.Name ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Character.Head.Position).Magnitude/3) ..' M')
							name.Size = UDim2.new(1,0,1,0)
							name.TextYAlignment = 'Top'
							name.BackgroundTransparency = 1
							name.TextStrokeTransparency = 0.5
							if v.Team == game.Players.LocalPlayer.Team then
								name.TextColor3 = Color3.fromRGB(28, 255, 255)
							else
								name.TextColor3 = Color3.fromRGB(28, 255, 255)
							end
						else
							v.Character.Head['NameEsp'..Number].TextLabel.Text = (v.Name ..' \n '.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Character.Head.Position).Magnitude/3) ..' M\nHealth : ' .. round(v.Character.Humanoid.Health*100/v.Character.Humanoid.MaxHealth) .. '%')
						end
					else
						if v.Character.Head:FindFirstChild('NameEsp'..Number) then
							v.Character.Head:FindFirstChild('NameEsp'..Number):Destroy()
						end
					end
				end
			end)
		end
	end
	function UpdateChestChams() 
		for i,v in pairs(game.Workspace:GetChildren()) do
			pcall(function()
				if string.find(v.Name,"Chest") then
					if ChestESP then
						if string.find(v.Name,"Chest") then
							if not v:FindFirstChild('NameEsp'..Number) then
								local bill = Instance.new('BillboardGui',v)
								bill.Name = 'NameEsp'..Number
								bill.ExtentsOffset = Vector3.new(0, 1, 0)
								bill.Size = UDim2.new(1,200,1,30)
								bill.Adornee = v
								bill.AlwaysOnTop = true
								local name = Instance.new('TextLabel',bill)
								name.Font = "GothamBold"
								name.FontSize = "Size14"
								name.TextWrapped = true
								name.Size = UDim2.new(1,0,1,0)
								name.TextYAlignment = 'Top'
								name.BackgroundTransparency = 1
								name.TextStrokeTransparency = 0.5
								if v.Name == "Chest1" then
									name.TextColor3 = Color3.fromRGB(255, 255, 255)
									name.Text = ("Chest 1" ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
								end
								if v.Name == "Chest2" then
									name.TextColor3 = Color3.fromRGB(255, 255, 51)
									name.Text = ("Chest 2" ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
								end
								if v.Name == "Chest3" then
									name.TextColor3 = Color3.fromRGB(28, 255, 255)
									name.Text = ("Chest 3" ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
								end
							else
								v['NameEsp'..Number].TextLabel.Text = (v.Name ..'   \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
							end
						end
					else
						if v:FindFirstChild('NameEsp'..Number) then
							v:FindFirstChild('NameEsp'..Number):Destroy()
						end
					end
				end
			end)
		end
	end
	function UpdateDevilChams() 
		for i,v in pairs(game.Workspace:GetChildren()) do
			pcall(function()
				if DevilFruitESP then
					if string.find(v.Name, "Fruit") then   
						if not v.Handle:FindFirstChild('NameEsp'..Number) then
							local bill = Instance.new('BillboardGui',v.Handle)
							bill.Name = 'NameEsp'..Number
							bill.ExtentsOffset = Vector3.new(0, 1, 0)
							bill.Size = UDim2.new(1,200,1,30)
							bill.Adornee = v.Handle
							bill.AlwaysOnTop = true
							local name = Instance.new('TextLabel',bill)
							name.Font = "GothamBold"
							name.FontSize = "Size14"
							name.TextWrapped = true
							name.Size = UDim2.new(1,0,1,0)
							name.TextYAlignment = 'Top'
							name.BackgroundTransparency = 1
							name.TextStrokeTransparency = 0.5
							name.TextColor3 = Color3.fromRGB(28, 255, 255)
							name.Text = (v.Name ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Handle.Position).Magnitude/3) ..' M')
						else
							v.Handle['NameEsp'..Number].TextLabel.Text = (v.Name ..'   \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Handle.Position).Magnitude/3) ..' M')
						end
					end
				else
					if v.Handle:FindFirstChild('NameEsp'..Number) then
						v.Handle:FindFirstChild('NameEsp'..Number):Destroy()
					end
				end
			end)
		end
	end
	function UpdateFlowerChams() 
		for i,v in pairs(game.Workspace:GetChildren()) do
			pcall(function()
				if v.Name == "Flower2" or v.Name == "Flower1" then
					if FlowerESP then 
						if not v:FindFirstChild('NameEsp'..Number) then
							local bill = Instance.new('BillboardGui',v)
							bill.Name = 'NameEsp'..Number
							bill.ExtentsOffset = Vector3.new(0, 1, 0)
							bill.Size = UDim2.new(1,200,1,30)
							bill.Adornee = v
							bill.AlwaysOnTop = true
							local name = Instance.new('TextLabel',bill)
							name.Font = "GothamBold"
							name.FontSize = "Size14"
							name.TextWrapped = true
							name.Size = UDim2.new(1,0,1,0)
							name.TextYAlignment = 'Top'
							name.BackgroundTransparency = 1
							name.TextStrokeTransparency = 0.5
							name.TextColor3 = Color3.fromRGB(255, 0, 0)
							if v.Name == "Flower1" then 
								name.Text = ("Blue Flower" ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
								name.TextColor3 = Color3.fromRGB(0, 0, 255)
							end
							if v.Name == "Flower2" then
								name.Text = ("Red Flower" ..' \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
								name.TextColor3 = Color3.fromRGB(255, 0, 0)
							end
						else
							v['NameEsp'..Number].TextLabel.Text = (v.Name ..'   \n'.. round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M')
						end
					else
						if v:FindFirstChild('NameEsp'..Number) then
							v:FindFirstChild('NameEsp'..Number):Destroy()
						end
					end
				end   
			end)
		end
	end
	
	function ESPGear()
		for i,v in pairs(game:GetService("Workspace").Map:FindFirstChild('MysticIsland'):GetChildren()) do
			pcall(function()
				if v.Name == 'Part' then
					if v.ClassName == 'MeshPart' then
						if Esp_Gear then
							if not v:FindFirstChild('NameEsp') then
								local bill = Instance.new('BillboardGui',v)
								bill.Name = 'NameEsp'
								bill.ExtentsOffset = Vector3.new(0, 1, 0)
								bill.Size = UDim2.new(1,200,1,30)
								bill.Adornee = v
								bill.AlwaysOnTop = true
								local name = Instance.new('TextLabel',bill)
								name.Font = "GothamBold"
								name.FontSize = "Size14"
								name.TextWrapped = true
								name.Size = UDim2.new(1,0,1,0)
								name.TextYAlignment = 'Top'
								name.BackgroundTransparency = 1
								name.TextStrokeTransparency = 0.5
								name.TextColor3 = Color3.fromRGB(28, 255, 255)
								name.Text = ("Gear" ..' \n'.." [ "..round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M'.." ] ")
							else
								v["NameEsp"].TextLabel.Text = ("Gear" ..' \n'.." [ "..round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M'.." ] ")
							end
						else
							for i,v in pairs(game:GetService("Workspace").Map:FindFirstChild('MysticIsland'):GetChildren()) do
								if v:FindFirstChild('NameEsp') then
									v:FindFirstChild('NameEsp'):Destroy()
								end
							end
						end
					end
				end
			end)
		end
	end
	
	function ESPMirageIsland()
		pcall(function()
			if ESP_Mirage_Island then
				for i,v in pairs(game:GetService("Workspace").Map.MysticIsland:GetChildren()) do
					pcall(function()
						if v.Name == 'Center' then
							if not v:FindFirstChild('EspMirage') then
								local bill = Instance.new('BillboardGui',v)
								bill.Name = 'EspMirage'
								bill.ExtentsOffset = Vector3.new(0, 1, 0)
								bill.Size = UDim2.new(1,200,1,30)
								bill.Adornee = v
								bill.AlwaysOnTop = true
								local name = Instance.new('TextLabel',bill)
								name.Font = "GothamBold"
								name.FontSize = "Size14"
								name.TextWrapped = true
								name.Size = UDim2.new(1,0,1,0)
								name.TextYAlignment = 'Top'
								name.BackgroundTransparency = 1
								name.TextStrokeTransparency = 0.5
								name.TextColor3 = Color3.fromRGB(28, 255, 255)
								name.Text = ("Mirage Island" ..' \n'.." [ "..round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M'.." ] ")
							else
								v.EspMirage.TextLabel.Text = ("Mirage Island" ..' \n'.." [ "..round((game:GetService('Players').LocalPlayer.Character.Head.Position - v.Position).Magnitude/3) ..' M'.." ] ")
							end
						end
					end)
				end
			else
				for i,v in pairs(game:GetService("Workspace").Map.MysticIsland:GetChildren()) do
					if v.Center:FindFirstChild('EspMirage') then
						v.Center:FindFirstChild('EspMirage'):Destroy()
					end
				end
			end
		end)
	end
	
	---------------------------------------------------
	
	function Box_eventory(Items)
		for i,v in pairs(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("getInventoryWeapons")) do
			if type(v) == "table" then
				if v.Type == "Sword" then
					if v.Name == Items then
						wait()
					end
				end
			end
		end
	end
	
	function Bypass(Position)
		pcall(function()
			_G.StopTween = true
			fkwarp = false
	
			if game:GetService("Players")["LocalPlayer"].Data:FindFirstChild("SpawnPoint").Value == tostring(GetIsLand(Point)) then 
				wait(.1)
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TeleportToSpawn")
			elseif game:GetService("Players")["LocalPlayer"].Data:FindFirstChild("LastSpawnPoint").Value == tostring(GetIsLand(Point)) then
				game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid"):ChangeState(15)
				wait(0.1)
				repeat wait() until game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid").Health > 0
			else
				if game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid").Health > 0 then
					if fkwarp == false then
						game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
					end
					fkwarp = true
				end
				wait(.08)
				game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid"):ChangeState(15)
				repeat wait() until game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid").Health > 0
				wait(.1)
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("SetSpawnPoint")
			end
			wait(0.2)
			_G.Clip = false
			if game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip") then
				game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip"):Destroy()
			end
			_G.Clip = false
			_G.StopTween = false
			return
		end)
	end
	
	-- // Check Sword \\ --
	
	function Check_Sword(Sword_Name)
		for i, v in pairs(game:GetService("ReplicatedStorage").Remotes['CommF_']:InvokeServer("getInventory")) do
			if (v.Type == "Sword") then
				if v.Name == Sword_Name then
					return true
				end
			end
		end
	end
	
	local UIConfig = {Bind = Enum.KeyCode.RightControl}

	local ScreenGui = Instance.new("ScreenGui")
	local ImageButton = Instance.new("ImageButton")
	local UICorner = Instance.new("UICorner")
			
	ScreenGui.Name = "ImageButton"
	ScreenGui.Parent = game.CoreGui
	ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

	ImageButton.Parent = ScreenGui
	ImageButton.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
	ImageButton.BorderSizePixel = 0
	ImageButton.Position = UDim2.new(0.120833337, 0, 0.0952890813, 0)
	ImageButton.Size = UDim2.new(0, 50, 0, 50)
	ImageButton.Draggable = true
	ImageButton.Image = "http://www.roblox.com/asset/?id=12781257228"
	ImageButton.MouseButton1Down:connect(function()
		game:GetService("VirtualInputManager"):SendKeyEvent(true,305,false,game)
		game:GetService("VirtualInputManager"):SendKeyEvent(false,305,false,game)
	end)
	UICorner.Parent = ImageButton
	
	local UserInputService = game:GetService("UserInputService")
	local TweenService = game:GetService("TweenService")
	local RunService = game:GetService("RunService")
	local LocalPlayer = game:GetService("Players").LocalPlayer
	local Mouse = LocalPlayer:GetMouse()
	local LWADGAW = false
	function dragify(Frame, object)
		dragToggle = nil
		dragSpeed = .25
		dragInput = nil
		dragStart = nil
		dragPos = nil
	
		function updateInput(input)
			Delta = input.Position - dragStart
			Position =
				UDim2.new(startPos.X.Scale, startPos.X.Offset + Delta.X, startPos.Y.Scale, startPos.Y.Offset + Delta.Y)
			game:GetService("TweenService"):Create(object, TweenInfo.new(dragSpeed), {Position = Position}):Play()
		end
	
		Frame.InputBegan:Connect(
			function(input)
				if
					(input.UserInputType == Enum.UserInputType.MouseButton1 or
						input.UserInputType == Enum.UserInputType.Touch)
				then
					dragToggle = true
					dragStart = input.Position
					startPos = object.Position
					input.Changed:Connect(
						function()
							if (input.UserInputState == Enum.UserInputState.End) then
								dragToggle = false
							end
						end
					)
				end
			end
		)
	
		Frame.InputChanged:Connect(
			function(input)
				if
					(input.UserInputType == Enum.UserInputType.MouseMovement or
						input.UserInputType == Enum.UserInputType.Touch)
				then
					dragInput = input
				end
			end
		)
	
		game:GetService("UserInputService").InputChanged:Connect(
		function(input)
			if (input == dragInput and dragToggle) then
				updateInput(input)
			end
		end
		)
	end
	
	library = {}
	
	if game:GetService("CoreGui"):FindFirstChild("Zee Hub UI") then
		game:GetService("CoreGui"):FindFirstChild("Zee Hub UI"):Destroy()
	end
	
	local ZeeHubUI = Instance.new("ScreenGui")
	ZeeHubUI.Name = "Zee Hub UI"
	ZeeHubUI.Parent = game:GetService("CoreGui")
	ZeeHubUI.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
	
	function library:Evil()
		local Main = Instance.new("Frame")
		local UICorner = Instance.new("UICorner")
		local UIStroke = Instance.new("UIStroke")
	
		Main.Name = "Main"
		Main.Parent = ZeeHubUI
		Main.AnchorPoint = Vector2.new(0.5, 0.5)
		Main.BackgroundColor3 = Color3.fromRGB(10, 11, 12)
		Main.BorderColor3 = Color3.fromRGB(255, 0, 4)
		Main.BorderSizePixel = 2
		Main.ClipsDescendants = true
		Main.Position = UDim2.new(0.5, 0, 0.5, 0)
	
		Main.Size = UDim2.new(0, 600, 0, 2) wait(0.1)
		Main:TweenSize(UDim2.new(0,600,0,335),"Out","Back",.5,true) wait(.2)
		Main.ClipsDescendants = false
	
		spawn(function()
			while wait() do
				if LWADGAW == true then
					dragify(Main,Main)
				end
			end
		end)
		dragify(Main,Main)
	
		local uitoggled = false
		UserInputService.InputBegan:Connect(
			function(io, p)
				if io.KeyCode == UIConfig.Bind then
					if uitoggled == false then
						Main.ClipsDescendants = true
						Main:TweenSize(UDim2.new(0, 0, 0, 0), Enum.EasingDirection.Out, Enum.EasingStyle.Quart, 0.4, true)
						uitoggled = true
						Main.Visible = false
					else
						Main.Visible = true wait() Main:TweenSize(UDim2.new(0, 600, 0, 335), Enum.EasingDirection.Out, Enum.EasingStyle.Quart,0.4,true)
						wait(0.2)
						Main.ClipsDescendants = true
						uitoggled = false
					end
				end
			end
		)
	
		UICorner.CornerRadius = UDim.new(0, 5)
		UICorner.Parent = Main
	
		UIStroke.Thickness = 1.5
		UIStroke.Parent = Main
		UIStroke.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
		UIStroke.LineJoinMode = Enum.LineJoinMode.Round
		UIStroke.Color = Color3.fromRGB(255, 0, 4)
		UIStroke.Transparency = 0.10
	
		local Main2 = Instance.new("Frame")
		local UICorner_2 = Instance.new("UICorner")
		local UIStroke_2 = Instance.new("UIStroke")
	
		Main2.Name = "Main2"
		Main2.Parent = Main
		Main2.Active = true
		Main2.AnchorPoint = Vector2.new(0.5, 0.5)
		Main2.BackgroundColor3 = Color3.fromRGB(2, 2, 2)
		Main2.BorderColor3 = Color3.fromRGB(0, 0, 0)
		Main2.BorderSizePixel = 0
		Main2.Position = UDim2.new(0.502000034, 0, 0.512455523, 0)
		Main2.Size = UDim2.new(0, 575, 0, 300)
	
		UICorner_2.CornerRadius = UDim.new(0, 5)
		UICorner_2.Parent = Main2
	
		UIStroke_2.Thickness = 1.5
		UIStroke_2.Parent = Main2
		UIStroke_2.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
		UIStroke_2.LineJoinMode = Enum.LineJoinMode.Round
		UIStroke_2.Color = Color3.fromRGB(255, 0, 4)
		UIStroke_2.Transparency = 0.10
	
		local Top = Instance.new("Frame")
		local UICorner_3 = Instance.new("UICorner")
		local Logo = Instance.new("ImageLabel")
		local UICorner_4 = Instance.new("UICorner")
	
		Top.Name = "Top"
		Top.Parent = Main2
		Top.AnchorPoint = Vector2.new(0.5, 0.5)
		Top.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
		Top.BorderColor3 = Color3.fromRGB(0, 0, 0)
		Top.BorderSizePixel = 0
		Top.Position = UDim2.new(0.0544226132, 0, 0.500217974, 0)
		Top.Size = UDim2.new(0, 55, 0, 258)
	
		UICorner_3.CornerRadius = UDim.new(0, 5)
		UICorner_3.Parent = Top
	
		Logo.Name = "Logo"
		Logo.Parent = Top
		Logo.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		Logo.BackgroundTransparency = 1.000
		Logo.BorderColor3 = Color3.fromRGB(0, 0, 0)
		Logo.BorderSizePixel = 0
		Logo.Position = UDim2.new(0.124675401, 0, 0.0270120781, 0)
		Logo.Size = UDim2.new(0, 40, 0, 40)
		Logo.Image = "rbxassetid://12781257228"
	
		UICorner_4.CornerRadius = UDim.new(0, 3)
		UICorner_4.Parent = Logo
	
		local Bottom = Instance.new("Frame")
		local ImageButton = Instance.new("ImageButton")
		local NameandTap = Instance.new("TextLabel")
	
		--{0, 500}, {0, 250}
	
		Bottom.Name = "Bottom"
		Bottom.Parent = Main2
		Bottom.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		Bottom.BackgroundTransparency = 1.000
		Bottom.BorderColor3 = Color3.fromRGB(0, 0, 0)
		Bottom.BorderSizePixel = 0
		Bottom.Position = UDim2.new(0.106530689, 0, 0.135464936, 0)
		Bottom.Size = UDim2.new(0, 500, 0, 250)
	
		ImageButton.Parent = Bottom
		ImageButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		ImageButton.BackgroundTransparency = 1.000
		ImageButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
		ImageButton.BorderSizePixel = 0
		ImageButton.Position = UDim2.new(0.933514178, 10, -0.157381549, 0)
		ImageButton.Size = UDim2.new(0, 30, 0, 30)
		ImageButton.Image = "rbxassetid://14553226015"
	
		NameandTap.Name = "Name and Tap"
		NameandTap.Parent = Bottom
		NameandTap.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		NameandTap.BackgroundTransparency = 1.000
		NameandTap.BorderColor3 = Color3.fromRGB(0, 0, 0)
		NameandTap.BorderSizePixel = 0
		NameandTap.Position = UDim2.new(0.0154185025, 0, -0.121076234, -5)
		NameandTap.Size = UDim2.new(0, 104, 0, 21)
		NameandTap.Font = Enum.Font.FredokaOne
		NameandTap.Text = "Zee Tab 1/5"
		NameandTap.TextColor3 = Color3.fromRGB(255, 255, 255)
		NameandTap.TextSize = 14.000
		NameandTap.TextXAlignment = Enum.TextXAlignment.Left
	
		local TabContainer = Instance.new("ScrollingFrame")
		local UICorner_5 = Instance.new("UICorner")
		local UIPadding = Instance.new("UIPadding")
		local UIListLayout = Instance.new("UIListLayout")
	
		TabContainer.Name = "Tap"
		TabContainer.Parent = Top
		TabContainer.Active = true
		TabContainer.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		TabContainer.BackgroundTransparency = 1.000
		TabContainer.BorderColor3 = Color3.fromRGB(0, 0, 0)
		TabContainer.BorderSizePixel = 0
		TabContainer.Position = UDim2.new(0.127272725, 0, 0.224806204, 0)
		TabContainer.Size = UDim2.new(0, 40, 0, 192)
		TabContainer.CanvasSize = UDim2.new(0, 0, 2, UIListLayout.AbsoluteContentSize.Y)
		TabContainer.ScrollBarThickness = 0
	
		UIPadding.Parent = TabContainer
	
		UIListLayout.Parent = TabContainer
		UIListLayout.Padding = UDim.new(0,5)
		UIListLayout.SortOrder = Enum.SortOrder.LayoutOrder
	
		UIListLayout:GetPropertyChangedSignal("AbsoluteContentSize"):Connect(
		function()
			TabContainer.CanvasSize = UDim2.new(0, 0, 0, UIListLayout.AbsoluteContentSize.Y)
		end)
	
		addtap = {}
		TapOpen = false
		function addtap:CraftTab(Name, Icon)
			NameandTap.Text = "Zee Tab Have Tap | Main"
			local TapName = Name
	
			local TabButton = Instance.new("ImageButton")
	
			TabButton.Name = "TabButton"
			TabButton.Parent = TabContainer
			TabButton.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
			TabButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
			TabButton.BorderSizePixel = 0
			TabButton.Position = UDim2.new(0.649999976, 0, -0.020833334, 0)
			TabButton.Size = UDim2.new(0, 40, 0, 40)
			TabButton.Image = "rbxassetid://"..Icon or 14553226015
	
			UICorner_5.CornerRadius = UDim.new(0, 3)
			UICorner_5.Parent = TabButton
	
			local Page = Instance.new("ScrollingFrame")
			local Left = Instance.new("ScrollingFrame")
			local Right = Instance.new("ScrollingFrame")
			local UIListLayout_5 = Instance.new("UIListLayout")
			local UIPadding_5 = Instance.new("UIPadding")
	
			Page.Name = "Page"
			Page.Parent = Bottom
			Page.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
			Page.BackgroundTransparency = 1.000
			Page.Size = UDim2.new(0, 500, 0, 250)
			Page.ScrollBarThickness = 0
			Page.CanvasSize = UDim2.new(0, 0, 0, 0)
			Page.Visible = false
	
			Left.Name = "Left"
			Left.Parent = Page
			Left.Active = true
			Left.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
			Left.BackgroundTransparency = 1
			Left.Size = UDim2.new(0, 250, 0, 250)
			Left.ScrollBarThickness = 0
			Left.CanvasSize = UDim2.new(0, 0, 0, 0)
	
			Right.Name = "Right"
			Right.Parent = Page
			Right.Active = true
			Right.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
			Right.BackgroundTransparency = 1
			Right.Size = UDim2.new(0, 250, 0, 250)
			Right.ScrollBarThickness = 0
			Right.CanvasSize = UDim2.new(0, 0, 0, 0)
	
			local LeftList = Instance.new("UIListLayout")
			local RightList = Instance.new("UIListLayout")
	
			LeftList.Parent = Left
			LeftList.SortOrder = Enum.SortOrder.LayoutOrder
			LeftList.Padding = UDim.new(0, 5)
	
			RightList.Parent = Right
			RightList.SortOrder = Enum.SortOrder.LayoutOrder
			RightList.Padding = UDim.new(0, 5)
	
			UIListLayout_5.Parent = Page
			UIListLayout_5.FillDirection = Enum.FillDirection.Horizontal
			UIListLayout_5.SortOrder = Enum.SortOrder.LayoutOrder
			UIListLayout_5.Padding = UDim.new(0, 3)
	
			UIPadding_5.Parent = Page
	
			if TapOpen == false then
				TapOpen = true
				Page.Visible = true
				TabButton.ImageColor3 = Color3.fromRGB(255, 255, 255)
				TabButton.ImageTransparency = 0
			end
	
			TabButton.MouseButton1Click:Connect(
				function()
					for _, x in next, TabContainer:GetChildren() do
						if x.Name == "TabButton" then
							for index, y in next, Bottom:GetChildren() do
								if y.Name ~= "ImageButton" and y.Name ~= "Name and Tap" then
									y.Visible = false
								end
							end
						end
					end
					Page.Visible = true
					NameandTap.Text = "Zee Tab Have Tap | "..TapName
				end
			)
	
			local function GetType(value)
				if value == 1 or value == "Left" then
					return Left
				elseif value == 2 or value == "Right" then
					return Right
				else
					return Left
				end
			end
	
			game:GetService("RunService").Stepped:Connect(function()
				pcall(function()
					Right.CanvasSize = UDim2.new(0,0,0,RightList.AbsoluteContentSize.Y + 5)
					Left.CanvasSize = UDim2.new(0,0,0,LeftList.AbsoluteContentSize.Y + 5)
				end)
			end)
	
			local sections = {}
			function sections:CraftPage(Name,side)
	
				if side == nil then
					return Left
				end
	
				local Section = Instance.new("Frame")
				local UICorner_5 = Instance.new("UICorner")
				local Top_2 = Instance.new("Frame")
				local Line = Instance.new("Frame")
				local Sectionname = Instance.new("TextLabel")
				local SectionContainer = Instance.new("Frame")
				local SectionContainer_2 = Instance.new("Frame")
				local UIListLayout_2 = Instance.new("UIListLayout")
				local UIPadding_2 = Instance.new("UIPadding")
	
				Section.Name = "Section"
				Section.Parent = GetType(side)
				Section.BackgroundColor3 = Color3.fromRGB(0, 0, 10)
				Section.ClipsDescendants = true
				Section.Transparency = 0.1
				Section.Size = UDim2.new(1, 0, 0, 40)
	
				UICorner_5.CornerRadius = UDim.new(0, 5)
				UICorner_5.Parent = Section
	
				Top_2.Name = "Top"
				Top_2.Parent = Section
				Top_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
				Top_2.BackgroundTransparency = 1.000
				Top_2.BorderColor3 = Color3.fromRGB(27, 42, 53)
				Top_2.Size = UDim2.new(0, 238, 0, 31)
	
				Line.Name = "Line"
				Line.Parent = Top_2
				Line.BackgroundColor3 = Color3.fromRGB(255, 0, 0)
				Line.BorderSizePixel = 0
				Line.Size = UDim2.new(0, 274, 0, 1)
	
				SectionContainer.Name = "SectionContainer"
				SectionContainer.Parent = Top_2
				SectionContainer.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
				SectionContainer.BackgroundTransparency = 1.000
				SectionContainer.BorderSizePixel = 0
				SectionContainer.Position = UDim2.new(0, 0, 0.796416223, 0)
				SectionContainer.Size = UDim2.new(0, 239, 0, 270)
	
				SectionContainer_2.Name = "SectionContainer_2"
				SectionContainer_2.Parent = Top_2
				SectionContainer_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
				SectionContainer_2.BackgroundTransparency = 1.000
				SectionContainer_2.BorderSizePixel = 0
				SectionContainer_2.Position = UDim2.new(0, 0, 0.796416223, 0)
				SectionContainer_2.Size = UDim2.new(0, 239, 0, 270)
	
				UIListLayout_2.Parent = SectionContainer
				UIListLayout_2.SortOrder = Enum.SortOrder.LayoutOrder
				UIListLayout_2.Padding = UDim.new(0, 10)
	
				UIPadding_2.Parent = SectionContainer
				UIPadding_2.PaddingLeft = UDim.new(0, 5)
	
				UIListLayout_2:GetPropertyChangedSignal("AbsoluteContentSize"):Connect(function()
					Section.Size = UDim2.new(1, 0, 0, UIListLayout_2.AbsoluteContentSize.Y + 35)
				end)
	
				local function_main = {}
				function function_main:Toggle(Text,Value,callback)
					local Toggle = Instance.new("Frame")
					local UICorner_6 = Instance.new("UICorner")
					local Text1 = Instance.new("TextLabel")
					local Text2 = Instance.new("TextLabel")
					local TextButton = Instance.new("TextButton")
					local MainToggle = Instance.new("Frame")
					local UICorner_7 = Instance.new("UICorner")
					local UICorner_8 = Instance.new("UICorner")
					local Frame = Instance.new("Frame")
	
	
					Toggle.Name = "Toggle"
					Toggle.Parent = SectionContainer
					Toggle.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
					Toggle.BorderColor3 = Color3.fromRGB(0, 0, 0)
					Toggle.BorderSizePixel = 0
					Toggle.Position = UDim2.new(0, 0, -0.0269058291, 0)
					Toggle.Size = UDim2.new(1, 0, 0, 69)
	
					UICorner_6.CornerRadius = UDim.new(0, 3)
					UICorner_6.Parent = Toggle
	
					Text1.Name = "Text1"
					Text1.Parent = Toggle
					Text1.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Text1.BackgroundTransparency = 1.000
					Text1.BorderColor3 = Color3.fromRGB(0, 0, 0)
					Text1.BorderSizePixel = 0
					Text1.Position = UDim2.new(0.0328638479, 0, 0, 0)
					Text1.Size = UDim2.new(1, 0, 0, 29)
					Text1.Font = Enum.Font.FredokaOne
					Text1.Text = Text
					Text1.TextColor3 = Color3.fromRGB(255, 255, 255)
					Text1.TextSize = 14.000
					Text1.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
					Text1.TextWrapped = true
					Text1.TextXAlignment = Enum.TextXAlignment.Left
	
					Text2.Name = "Text2"
					Text2.Parent = Toggle
					Text2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Text2.BackgroundTransparency = 1.000
					Text2.BorderColor3 = Color3.fromRGB(0, 0, 0)
					Text2.BorderSizePixel = 0
					Text2.Position = UDim2.new(0.0281690136, 0, 0.420289844, 0)
					Text2.Size = UDim2.new(0, 175, 0, 40)
					Text2.Font = Enum.Font.Highway
					Text2.Text = Text1.Text .." = false"
					Text2.TextColor3 = Color3.fromRGB(132, 132, 132)
					Text2.TextSize = 12.000
					Text2.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
					Text2.TextWrapped = true
					Text2.TextXAlignment = Enum.TextXAlignment.Left
	
					TextButton.Parent = Toggle
					TextButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					TextButton.BackgroundTransparency = 1.000
					TextButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
					TextButton.BorderSizePixel = 0
					TextButton.Position = UDim2.new(0.64319247, 10, 0.427115828, 0)
					TextButton.Size = UDim2.new(0, 76, 0, 39)
					TextButton.Font = Enum.Font.SourceSans
					TextButton.Text = ""
					TextButton.TextColor3 = Color3.fromRGB(0, 0, 0)
					TextButton.TextSize = 14.000
	
					MainToggle.Name = "Main Toggle"
					MainToggle.Parent = Toggle
					MainToggle.BackgroundColor3 = Color3.fromRGB(42, 42, 42)
					MainToggle.BorderColor3 = Color3.fromRGB(0, 0, 0)
					MainToggle.BorderSizePixel = 0
					MainToggle.Position = UDim2.new(0.676056325, 10, 0.536231875, 0)
					MainToggle.Size = UDim2.new(0, 59, 0, 24)
	
					UICorner_7.Parent = MainToggle
	
					Frame.Parent = MainToggle
					Frame.AnchorPoint = Vector2.new(0.5, 0.5)
					Frame.BackgroundColor3 = Color3.fromRGB(255, 0, 4)
					Frame.BorderColor3 = Color3.fromRGB(0, 0, 0)
					Frame.BorderSizePixel = 0
					Frame.Position = UDim2.new(0.200000003, 0, 0.5, 0)
					Frame.Size = UDim2.new(0, 20, 0, 20)
	
					UICorner_8.CornerRadius = UDim.new(0, 100)
					UICorner_8.Parent = Frame
					
					local Toggle_D = false
					if Value == true then
						Toggle_D = true
						Frame.BackgroundColor3 = Color3.fromRGB(0, 255, 4)
						TweenService:Create(
							Frame,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Position = UDim2.new(0.75, 0, 0.5, 0)}
						):Play()
						Text2.Text = Text1.Text .." = True"
						callback(true)
					end
	
					TextButton.MouseButton1Click:Connect(function()
						if Toggle_D == false then
							Toggle_D = true
							Frame.BackgroundColor3 = Color3.fromRGB(0, 255, 4)
							TweenService:Create(
								Frame,
								TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
								{Position = UDim2.new(0.75, 0, 0.5, 0)}
							):Play()
							Text2.Text = Text1.Text .." = True"
							callback(true)
						else
							Toggle_D = false
							Frame.BackgroundColor3 = Color3.fromRGB(255, 0, 4)
							TweenService:Create(
								Frame,
								TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
								{Position = UDim2.new(0.2, 0, 0.5, 0)}
							):Play()
							Text2.Text = Text1.Text .." = False"
							callback(false)
						end
					end)
				end
				function function_main:Button(Text,callback)
					local Button = Instance.new("Frame")
					local UICorner_12 = Instance.new("UICorner")
					local ClickButton = Instance.new("TextButton")
					local UICorner_13 = Instance.new("UICorner")
	
					Button.Name = "Button"
					Button.Parent = SectionContainer
					Button.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
					Button.BorderColor3 = Color3.fromRGB(0, 0, 0)
					Button.BorderSizePixel = 0
					Button.Position = UDim2.new(0, 0, 0.318385661, 0)
					Button.Size = UDim2.new(1, 0, 0, 42)
	
					UICorner_12.CornerRadius = UDim.new(0, 3)
					UICorner_12.Parent = Button
	
					ClickButton.Name = "ClickButton"
					ClickButton.Parent = Button
					ClickButton.AnchorPoint = Vector2.new(0.5, 0.5)
					ClickButton.BackgroundColor3 = Color3.fromRGB(255, 0, 4)
					ClickButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
					ClickButton.BorderSizePixel = 0
					ClickButton.Position = UDim2.new(0.5, 0, 0.5, 0)
					ClickButton.Size = UDim2.new(0.95, 0, 0, 30)
					ClickButton.Font = Enum.Font.FredokaOne
					ClickButton.TextColor3 = Color3.fromRGB(255, 255, 255)
					ClickButton.TextSize = 14.000
					ClickButton.Text = Text
					ClickButton.TextWrapped = true
	
					UICorner_13.CornerRadius = UDim.new(0, 3)
					UICorner_13.Parent = ClickButton
	
					ClickButton.MouseButton1Click:Connect(function()
						ClickButton.TextSize = 0
						TweenService:Create(
							ClickButton,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{TextSize = 12}
						):Play()
						callback()
					end)
				end
				function function_main:Label(text)
					local textas = {}
					local Label = Instance.new("Frame")
					local Text = Instance.new("TextLabel")
					Label.Name = "Label"
					Label.Parent = SectionContainer
					Label.AnchorPoint = Vector2.new(0.5, 0.5)
					Label.BackgroundTransparency = 1.000
					Label.Size = UDim2.new(1, 0, 0, 30)
	
					Text.Name = "Text"
					Text.Parent = Label
					Text.AnchorPoint = Vector2.new(0.5, 0.5)
					Text.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Text.BackgroundTransparency = 1.000
					Text.Position = UDim2.new(0.5, 0, 0.5, 0)
					Text.Size = UDim2.new(0, 53, 0, 12)
					Text.ZIndex = 16
					Text.Font = Enum.Font.GothamBold
					Text.Text = text
					Text.TextColor3 = Color3.fromRGB(255, 255, 255)
					Text.TextSize = 12.000
					function textas.Refresh(newtext)
						Text.Text = newtext
					end
					return textas
				end
				function function_main:MultiDropdown(Name, list, default, callback)
					local Dropfunc = {}
	
					local MainDropDown = Instance.new("Frame")
					local UICorner_7 = Instance.new("UICorner")
					local MainDropDown_2 = Instance.new("Frame")
					local UICorner_8 = Instance.new("UICorner")
					local v = Instance.new("TextButton")
					local Text_2 = Instance.new("TextLabel")
					local ImageButton = Instance.new("ImageButton")
					local Scroll_Items = Instance.new("ScrollingFrame")
					local UIListLayout_3 = Instance.new("UIListLayout")
					local UIPadding_3 = Instance.new("UIPadding")
	
					MainDropDown.Name = "MainDropDown"
					MainDropDown.Parent = SectionContainer
					MainDropDown.BackgroundColor3 = Color3.fromRGB(12, 12, 12)
					MainDropDown.BackgroundTransparency = 0
					MainDropDown.BorderSizePixel = 0
					MainDropDown.ClipsDescendants = true
					MainDropDown.Size = UDim2.new(1, 0, 0, 35)
					MainDropDown.ZIndex = 16
	
					UICorner_7.CornerRadius = UDim.new(0, 4)
					UICorner_7.Parent = MainDropDown
	
					MainDropDown_2.Name = "MainDropDown"
					MainDropDown_2.Parent = MainDropDown
					MainDropDown_2.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
					MainDropDown_2.BackgroundTransparency = 0
					MainDropDown_2.BorderSizePixel = 0
					MainDropDown_2.ClipsDescendants = true
					MainDropDown_2.Size = UDim2.new(1, 0, 0, 30)
					MainDropDown_2.ZIndex = 16
	
					UICorner_8.CornerRadius = UDim.new(0, 4)
					UICorner_8.Parent = MainDropDown_2
	
					v.Name = "v"
					v.Parent = MainDropDown_2
					v.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					v.BackgroundTransparency = 1.000
					v.BorderSizePixel = 0
					v.Size = UDim2.new(1, 0, 1, 0)
					v.ZIndex = 16
					v.AutoButtonColor = false
					v.Font = Enum.Font.GothamBold
					v.Text = ""
					v.TextColor3 = Color3.fromRGB(255, 255, 255)
					v.TextSize = 12.000
					function getpro()
						if default then
							for i, v in next, default do
								if table.find(list, v) then
									callback(default)
									return Name .. " : " .. table.concat(default, ", ")
								else
									return Name
								end
							end
						else
							return Name
						end
					end
	
					Text_2.Name = "Text"
					Text_2.Parent = MainDropDown_2
					Text_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Text_2.BackgroundTransparency = 1.000
					Text_2.Position = UDim2.new(0, 10, 0, 10)
					Text_2.Size = UDim2.new(0, 62, 0, 12)
					Text_2.ZIndex = 16
					Text_2.Font = Enum.Font.GothamBold
					Text_2.Text = getpro()
					Text_2.TextColor3 = Color3.fromRGB(255, 255, 255)
					Text_2.TextSize = 12.000
					Text_2.TextXAlignment = Enum.TextXAlignment.Left
	
					ImageButton.Parent = MainDropDown_2
					ImageButton.AnchorPoint = Vector2.new(0, 0.5)
					ImageButton.BackgroundTransparency = 1.000
					ImageButton.Position = UDim2.new(1, -25, 0.5, 0)
					ImageButton.Size = UDim2.new(0, 12, 0, 12)
					ImageButton.ZIndex = 16
					ImageButton.Image = "http://www.roblox.com/asset/?id=6282522798"
					local DropTable = {}
					Scroll_Items.Name = "Scroll_Items"
					Scroll_Items.Parent = MainDropDown
					Scroll_Items.Active = true
					Scroll_Items.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Scroll_Items.BackgroundTransparency = 1.000
					Scroll_Items.BorderSizePixel = 0
					Scroll_Items.Position = UDim2.new(0, 0, 0, 35)
					Scroll_Items.Size = UDim2.new(1, 0, 1, -35)
					Scroll_Items.ZIndex = 16
					Scroll_Items.CanvasSize = UDim2.new(0, 0, 0, 265)
					Scroll_Items.ScrollBarThickness = 0
	
					UIListLayout_3.Parent = Scroll_Items
					UIListLayout_3.SortOrder = Enum.SortOrder.LayoutOrder
					UIListLayout_3.Padding = UDim.new(0, 5)
					UIListLayout_2:GetPropertyChangedSignal("AbsoluteContentSize"):Connect(
					function()
						Scroll_Items.CanvasSize = UDim2.new(1, 0, 0, UIListLayout_2.AbsoluteContentSize.Y+40)
					end
					)
					UIPadding_3.Parent = Scroll_Items
					UIPadding_3.PaddingLeft = UDim.new(0, 10)
					UIPadding_3.PaddingTop = UDim.new(0, 5)
	
					function Dropfunc:TogglePanel(state)
						TweenService:Create(
							MainDropDown,
							TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
							{Size = state and UDim2.new(1, 0, 0, 200) or UDim2.new(1, 0, 0, 30)}
						):Play()
						TweenService:Create(
							ImageButton,
							TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
							{Rotation = state and 180 or 0}
						):Play()
					end
					local Tof = false
					ImageButton.MouseButton1Click:Connect(
						function()
							Tof = not Tof
							Dropfunc:TogglePanel(Tof)
						end
					)
					v.MouseButton1Click:Connect(
						function()
							Tof = not Tof
							Dropfunc:TogglePanel(Tof)
						end
					)
					function Dropfunc:Add(Text)
						local _5 = Instance.new("TextButton")
						local UICorner_9 = Instance.new("UICorner")
						_5.Name = Text
						_5.Parent = Scroll_Items
						_5.BackgroundColor3 = Color3.fromRGB(25, 25, 25)
						_5.BorderSizePixel = 0
						_5.ClipsDescendants = true
						_5.Size = UDim2.new(1, -10, 0, 30)
						_5.ZIndex = 17
						_5.AutoButtonColor = false
						_5.Font = Enum.Font.GothamBold
						_5.Text = Text
						_5.TextColor3 = Color3.fromRGB(255, 255, 255)
						_5.TextSize = 12.000
	
						UICorner_9.CornerRadius = UDim.new(0, 4)
						UICorner_9.Parent = _5
						_5.MouseButton1Click:Connect(
							function()
								if not table.find(DropTable, Text) then
									table.insert(DropTable, Text)
									callback(DropTable, Text)
									Text_2.Text = Name .. " : " .. table.concat(DropTable, ", ")
									TweenService:Create(
										_5,
										TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
										{TextColor3 = Color3.fromRGB(255, 0, 0)}
									):Play()
								else
									TweenService:Create(
										_5,
										TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
										{TextColor3 = Color3.fromRGB(255, 255, 255)}
									):Play()
									for i2, v2 in pairs(DropTable) do
										if v2 == Text then
											table.remove(DropTable, i2)
											Text_2.Text = Name .. " : " .. table.concat(DropTable, ", ")
										end
									end
									callback(DropTable, Text)
								end
							end
						)
					end
					function Dropfunc:Clear()
						for i, v in next, Scroll_Items:GetChildren() do
							if v:IsA("TextButton")  then 
								v:Destroy()
	
							end
						end 
					end
	
					for i, v in next, list do
						Dropfunc:Add(v)
					end
					return Dropfunc
				end
				function function_main:Dropdown(Name, list, default, callback)
					local Dropfunc = {}
	
					local MainDropDown = Instance.new("Frame")
					local UICorner_7 = Instance.new("UICorner")
					local MainDropDown_2 = Instance.new("Frame")
					local UICorner_8 = Instance.new("UICorner")
					local v = Instance.new("TextButton")
					local Text_2 = Instance.new("TextLabel")
					local ImageButton = Instance.new("ImageButton")
					local Scroll_Items = Instance.new("ScrollingFrame")
					local UIListLayout_3 = Instance.new("UIListLayout")
					local UIPadding_3 = Instance.new("UIPadding")
	
					MainDropDown.Name = "MainDropDown"
					MainDropDown.Parent = SectionContainer
					MainDropDown.BackgroundColor3 = Color3.fromRGB(12, 12, 12)
					MainDropDown.BackgroundTransparency = 0
					MainDropDown.BorderSizePixel = 0
					MainDropDown.ClipsDescendants = true
					MainDropDown.Size = UDim2.new(1, 0, 0, 35)
					MainDropDown.ZIndex = 16
	
					UICorner_7.CornerRadius = UDim.new(0, 4)
					UICorner_7.Parent = MainDropDown
	
					MainDropDown_2.Name = "MainDropDown"
					MainDropDown_2.Parent = MainDropDown
					MainDropDown_2.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
					MainDropDown_2.BackgroundTransparency = 0
					MainDropDown_2.BorderSizePixel = 0
					MainDropDown_2.ClipsDescendants = true
					MainDropDown_2.Size = UDim2.new(1, 0, 0, 30)
					MainDropDown_2.ZIndex = 16
	
					UICorner_8.CornerRadius = UDim.new(0, 4)
					UICorner_8.Parent = MainDropDown_2
	
					v.Name = "v"
					v.Parent = MainDropDown_2
					v.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					v.BackgroundTransparency = 1.000
					v.BorderSizePixel = 0
					v.Size = UDim2.new(1, 0, 1, 0)
					v.ZIndex = 16
					v.AutoButtonColor = false
					v.Font = Enum.Font.GothamBold
					v.Text = ""
					v.TextColor3 = Color3.fromRGB(255, 255, 255)
					v.TextSize = 12.000
					function getpro()
						if default then
							if table.find(list, default) then
								callback(default)
								return Name .. " : " .. default
							else
								return Name .. " : "
							end
						else
							return Name .. " : "
						end
					end
					Text_2.Name = "Text"
					Text_2.Parent = MainDropDown_2
					Text_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Text_2.BackgroundTransparency = 1.000
					Text_2.Position = UDim2.new(0, 10, 0, 10)
					Text_2.Size = UDim2.new(0, 62, 0, 12)
					Text_2.ZIndex = 16
					Text_2.Font = Enum.Font.GothamBold
					Text_2.Text = getpro()
					Text_2.TextColor3 = Color3.fromRGB(255, 255, 255)
					Text_2.TextSize = 12.000
					Text_2.TextXAlignment = Enum.TextXAlignment.Left
	
					ImageButton.Parent = MainDropDown_2
					ImageButton.AnchorPoint = Vector2.new(0, 0.5)
					ImageButton.BackgroundTransparency = 1.000
					ImageButton.Position = UDim2.new(1, -25, 0.5, 0)
					ImageButton.Size = UDim2.new(0, 12, 0, 12)
					ImageButton.ZIndex = 16
					ImageButton.Image = "http://www.roblox.com/asset/?id=6282522798"
	
					Scroll_Items.Name = "Scroll_Items"
					Scroll_Items.Parent = MainDropDown
					Scroll_Items.Active = true
					Scroll_Items.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Scroll_Items.BackgroundTransparency = 1.000
					Scroll_Items.BorderSizePixel = 0
					Scroll_Items.Position = UDim2.new(0, 0, 0, 35)
					Scroll_Items.Size = UDim2.new(1, 0, 1, -35)
					Scroll_Items.ZIndex = 16
					Scroll_Items.CanvasSize = UDim2.new(0, 0, 0, 265)
					Scroll_Items.ScrollBarThickness = 0
	
					UIListLayout_3.Parent = Scroll_Items
					UIListLayout_3.SortOrder = Enum.SortOrder.LayoutOrder
					UIListLayout_3.Padding = UDim.new(0, 5)
					UIListLayout_2:GetPropertyChangedSignal("AbsoluteContentSize"):Connect(
					function()
						Scroll_Items.CanvasSize = UDim2.new(1, 0, 0, UIListLayout_2.AbsoluteContentSize.Y+40)
					end
					)
					UIPadding_3.Parent = Scroll_Items
					UIPadding_3.PaddingLeft = UDim.new(0, 10)
					UIPadding_3.PaddingTop = UDim.new(0, 5)
	
					function Dropfunc:TogglePanel(state)
						TweenService:Create(
							MainDropDown,
							TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
							{Size = state and UDim2.new(1, 0, 0, 299) or UDim2.new(1, 0, 0, 30)}
						):Play()
						TweenService:Create(
							ImageButton,
							TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
							{Rotation = state and 180 or 0}
						):Play()
					end
					local Tof = false
					ImageButton.MouseButton1Click:Connect(
						function()
							Tof = not Tof
							Dropfunc:TogglePanel(Tof)
						end
					)
					v.MouseButton1Click:Connect(
						function()
							Tof = not Tof
							Dropfunc:TogglePanel(Tof)
						end
					)
					function Dropfunc:Clear()
						for i, v in next, Scroll_Items:GetChildren() do
							if v:IsA("TextButton") then 
								v:Destroy()
							end
						end
					end
	
					function Dropfunc:Add(Text)
						local _5 = Instance.new("TextButton")
						local UICorner_9 = Instance.new("UICorner")
						_5.Name = Text
						_5.Parent = Scroll_Items
						_5.BackgroundColor3 = Color3.fromRGB(25, 25, 25)
						_5.BorderSizePixel = 0
						_5.ClipsDescendants = true
						_5.Size = UDim2.new(1, -10, 0, 30)
						_5.ZIndex = 17
						_5.AutoButtonColor = false
						_5.Font = Enum.Font.GothamBold
						_5.Text = Text
						_5.TextColor3 = Color3.fromRGB(255, 255, 255)
						_5.TextSize = 12.000
	
						UICorner_9.CornerRadius = UDim.new(0, 4)
						UICorner_9.Parent = _5
	
						_5.MouseButton1Click:Connect(
							function()
								if _x == nil then
									Tof = false
									Dropfunc:TogglePanel(Tof)
									callback(Text)
									Text_2.Text = Name .. " : "..Text
									_x = nil
								end
							end
						)
					end
					for i, v in next, list do
						Dropfunc:Add(v)
					end
					return Dropfunc
				end
				function function_main:Slider(text,floor,min,max,de,callback)
					local SliderFrame = Instance.new("Frame")
					local LabelNameSlider = Instance.new("TextLabel")
					local ShowValueFrame = Instance.new("Frame")
					local CustomValue = Instance.new("TextBox")
					local ShowValueFrameUICorner = Instance.new("UICorner")
					local ValueFrame = Instance.new("Frame")
					local ValueFrameUICorner = Instance.new("UICorner")
					local PartValue = Instance.new("Frame")
					local PartValueUICorner = Instance.new("UICorner")
					local MainValue = Instance.new("Frame")
					local MainValueUICorner = Instance.new("UICorner")
					local sliderfunc = {}
	
					SliderFrame.Name = text
					SliderFrame.Parent = SectionContainer
					SliderFrame.BackgroundTransparency = 0
					SliderFrame.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
					SliderFrame.Position = UDim2.new(0.109489053, 0, 0.708609283, 0)
					SliderFrame.Size = UDim2.new(1, 0, 0, 45)
	
					local UIStroke96 = Instance.new("UIStroke")
					UIStroke96.Thickness = 1.2
					UIStroke96.Parent = SliderFrame
					UIStroke96.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
					UIStroke96.LineJoinMode = Enum.LineJoinMode.Round
					UIStroke96.Color = Color3.fromRGB(255, 0, 0)
					UIStroke96.Transparency = 0
	
					LabelNameSlider.Name = "LabelNameSlider"
					LabelNameSlider.Parent = SliderFrame
					LabelNameSlider.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					LabelNameSlider.BackgroundTransparency = 1.000
					LabelNameSlider.Position = UDim2.new(0.0729926974, 0, 0.0396823473, 0)
					LabelNameSlider.Size = UDim2.new(0, 182, 0, 25)
					LabelNameSlider.Font = Enum.Font.GothamBold
					LabelNameSlider.Text = tostring(text)
					LabelNameSlider.TextColor3 = Color3.fromRGB(255, 255, 255)
					LabelNameSlider.TextSize = 11.000
					LabelNameSlider.TextXAlignment = Enum.TextXAlignment.Left
	
					ShowValueFrame.Name = "ShowValueFrame"
					ShowValueFrame.Parent = SliderFrame
					ShowValueFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
					ShowValueFrame.Position = UDim2.new(0.733576655, 0, 0.0656082779, 0)
					ShowValueFrame.Size = UDim2.new(0, 58, 0, 21)
	
					CustomValue.Name = "CustomValue"
					CustomValue.Parent = ShowValueFrame
					CustomValue.AnchorPoint = Vector2.new(0.5, 0.5)
					CustomValue.BackgroundColor3 = Color3.fromRGB(10, 10, 10)
					CustomValue.Position = UDim2.new(0.5, 0, 0.5, 0)
					CustomValue.Size = UDim2.new(0, 55, 0, 21)
					CustomValue.Font = Enum.Font.GothamBold
					CustomValue.Text = ""
					CustomValue.TextColor3 = Color3.fromRGB(255, 255, 255)
					CustomValue.TextSize = 11.000
	
					local UIStroke965 = Instance.new("UIStroke")
					UIStroke965.Thickness = 1.2
					UIStroke965.Parent = CustomValue
					UIStroke965.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
					UIStroke965.LineJoinMode = Enum.LineJoinMode.Round
					UIStroke965.Color = Color3.fromRGB(255, 0, 0)
					UIStroke965.Transparency = 0.03
	
					SliderFrame.MouseEnter:Connect(function()
						TweenService:Create(
							UIStroke965,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Transparency = 0.5}
						):Play()
					end)
	
					SliderFrame.MouseLeave:Connect(function()
						TweenService:Create(
							UIStroke965,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Transparency = 0.10}
						):Play()
					end)
	
					ShowValueFrameUICorner.CornerRadius = UDim.new(0, 4)
					ShowValueFrameUICorner.Name = "ShowValueFrameUICorner"
					ShowValueFrameUICorner.Parent = ShowValueFrame
	
					ValueFrame.Name = "ValueFrame"
					ValueFrame.Parent = SliderFrame
					ValueFrame.AnchorPoint = Vector2.new(0.5, 0.5)
					ValueFrame.BackgroundColor3 = Color3.fromRGB(35, 35, 35)
					ValueFrame.Position = UDim2.new(0.5, 0, 0.8, 0)
					ValueFrame.Size = UDim2.new(0, 200, 0, 5)
	
					ValueFrameUICorner.CornerRadius = UDim.new(0, 30)
					ValueFrameUICorner.Name = "ValueFrameUICorner"
					ValueFrameUICorner.Parent = ValueFrame
	
					PartValue.Name = "PartValue"
					PartValue.Parent = ValueFrame
					PartValue.AnchorPoint = Vector2.new(0.5, 0.5)
					PartValue.BackgroundColor3 = Color3.fromRGB(35, 35, 35)
					PartValue.BackgroundTransparency = 1.000
					PartValue.Position = UDim2.new(0.5, 0, 0.8, 0)
					PartValue.Size = UDim2.new(0, 200, 0, 5)
	
					PartValueUICorner.CornerRadius = UDim.new(0, 30)
					PartValueUICorner.Name = "PartValueUICorner"
					PartValueUICorner.Parent = PartValue
	
					MainValue.Name = "MainValue"
					MainValue.Parent = ValueFrame
					MainValue.BackgroundColor3 = Color3.fromRGB(255, 0, 0)
					MainValue.Size = UDim2.new((de or 0) / max, 0, 0, 5)
					MainValue.BorderSizePixel = 0
	
					MainValueUICorner.CornerRadius = UDim.new(0, 30)
					MainValueUICorner.Name = "MainValueUICorner"
					MainValueUICorner.Parent = MainValue
	
	
					local ConneValue = Instance.new("Frame")
					ConneValue.Name = "ConneValue"
					ConneValue.Parent = PartValue
					ConneValue.AnchorPoint = Vector2.new(0.7, 0.7)
					ConneValue.BackgroundColor3 = Color3.fromRGB(255, 0, 0)
					ConneValue.Position = UDim2.new((de or 0)/max, 0.5, 0.5,0, 0)
					ConneValue.Size = UDim2.new(0, 10, 0, 10)
					ConneValue.BorderSizePixel = 0
		
					local UICorner = Instance.new("UICorner")
					UICorner.CornerRadius = UDim.new(0, 10)
					UICorner.Parent = ConneValue
	
	
					if floor == true then
						CustomValue.Text =  tostring(de and string.format("%0.2f",(de / max) * (max - min) + min) or 0)
					else
						CustomValue.Text =  tostring(de and math.floor((de / max) * (max - min) + min) or 0)
					end
	
					local function move(input)
						LWADGAW = true
						local pos =
							UDim2.new(
								math.clamp((input.Position.X - ValueFrame.AbsolutePosition.X) / ValueFrame.AbsoluteSize.X, 0, 1),
								0,
								0.5,
								0
							)
						local pos1 =
							UDim2.new(
								math.clamp((input.Position.X - ValueFrame.AbsolutePosition.X) / ValueFrame.AbsoluteSize.X, 0, 1),
								0,
								0,
								5
							)
						MainValue:TweenSize(pos1, "Out", "Sine", 0.2, true)
						ConneValue:TweenPosition(pos, "Out", "Sine", 0.2, true)
						if floor == true then
							local value = string.format("%.0f",((pos.X.Scale * max) / max) * (max - min) + min)
							CustomValue.Text = tostring(value)
							callback(value)
						else
							local value = math.floor(((pos.X.Scale * max) / max) * (max - min) + min)
							CustomValue.Text = tostring(value)
							callback(value)
						end
						wait(0.5)
						LWADGAW = false
					end
					local dragging = false
					ConneValue.InputBegan:Connect(
						function(input)
							if input.UserInputType == Enum.UserInputType.MouseButton1 then
								dragging = true
							end
						end)
					ConneValue.InputEnded:Connect(
						function(input)
							if input.UserInputType == Enum.UserInputType.MouseButton1 then
								dragging = false
							end
						end)
					SliderFrame.InputBegan:Connect(
						function(input)
							if input.UserInputType == Enum.UserInputType.MouseButton1 then
								dragging = true
							end
						end)
					SliderFrame.InputEnded:Connect(
						function(input)
							if input.UserInputType == Enum.UserInputType.MouseButton1 then
								dragging = false
							end
						end)
					ValueFrame.InputBegan:Connect(
						function(input)
							if input.UserInputType == Enum.UserInputType.MouseButton1 then
								dragging = true
							end
						end)
					ValueFrame.InputEnded:Connect(
						function(input)
							if input.UserInputType == Enum.UserInputType.MouseButton1 then
								dragging = false
							end
						end)
						game:GetService("UserInputService").InputChanged:Connect(function(input)
							if dragging and input.UserInputType == Enum.UserInputType.MouseMovement then
								move(input)
							end
						end)
						CustomValue.FocusLost:Connect(function()
							if CustomValue.Text == "" then
								CustomValue.Text  = de
							end
							if  tonumber(CustomValue.Text) > max then
								CustomValue.Text  = max
							end
							MainValue:TweenSize(UDim2.new((CustomValue.Text or 0) / max, 0, 0, 5), "Out", "Sine", 0.2, true)
							ConneValue:TweenPosition(UDim2.new((CustomValue.Text or 0)/max, 0,0.6, 0) , "Out", "Sine", 0.2, true)
							if floor == true then
								CustomValue.Text = tostring(CustomValue.Text and string.format("%.0f",(CustomValue.Text / max) * (max - min) + min) )
							else
								CustomValue.Text = tostring(CustomValue.Text and math.floor( (CustomValue.Text / max) * (max - min) + min) )
							end
							pcall(callback, CustomValue.Text)
						end)
						
						function sliderfunc:Update(value)
							MainValue:TweenSize(UDim2.new((value or 0) / max, 0, 0, 5), "Out", "Sine", 0.2, true)
							ConneValue:TweenPosition(UDim2.new((value or 0)/max, 0.5, 0.5,0, 0) , "Out", "Sine", 0.2, true)
							CustomValue.Text = value
							pcall(function()
								callback(value)
							end)
						end
					return sliderfunc
				end
				function function_main:Textbox(Name,Placeholder,callback)
					local Textbox = Instance.new("Frame")
					local UICorner_16 = Instance.new("UICorner")
					local Text_5 = Instance.new("TextLabel")
					local TextboxHoler = Instance.new("Frame")
					local UICorner_17 = Instance.new("UICorner")
					local HeadTitle = Instance.new("TextBox")

					Textbox.Name = Name
					Textbox.Parent = SectionContainer
					Textbox.BackgroundColor3 = Color3.fromRGB(1, 2, 3)
					Textbox.BackgroundTransparency = 0.700
					Textbox.BorderSizePixel = 0
					Textbox.ClipsDescendants = true
					Textbox.Size = UDim2.new(1, 0, 0, 60)
					Textbox.ZIndex = 16

					UICorner_16.CornerRadius = UDim.new(0, 4)
					UICorner_16.Parent = Textbox

					Text_5.Name = "Text"
					Text_5.Parent = Textbox
					Text_5.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
					Text_5.BackgroundTransparency = 1.000
					Text_5.Position = UDim2.new(0, 10, 0, 10)
					Text_5.Size = UDim2.new(0, 43, 0, 12)
					Text_5.ZIndex = 16
					Text_5.Font = Enum.Font.GothamBold
					Text_5.Text = Name
					Text_5.TextColor3 = Color3.fromRGB(255, 0, 0)
					Text_5.TextSize = 11.000
					Text_5.TextXAlignment = Enum.TextXAlignment.Left

					TextboxHoler.Name = "TextboxHoler"
					TextboxHoler.Parent = Textbox
					TextboxHoler.AnchorPoint = Vector2.new(0.5, 0.5)
					TextboxHoler.BackgroundColor3 = Color3.fromRGB(13, 13, 15)
					TextboxHoler.BackgroundTransparency = 1.000
					TextboxHoler.BorderSizePixel = 0
					TextboxHoler.Position = UDim2.new(0.5, 0, 0.5, 13)
					TextboxHoler.Size = UDim2.new(0.970000029, 0, 0, 30)

					UICorner_17.CornerRadius = UDim.new(0, 8)
					UICorner_17.Parent = TextboxHoler

					HeadTitle.Name = "HeadTitle"
					HeadTitle.Parent = TextboxHoler
					HeadTitle.AnchorPoint = Vector2.new(0.5, 0.5)
					HeadTitle.BackgroundColor3 = Color3.fromRGB(22, 22, 22)
					HeadTitle.BackgroundTransparency = 1.000
					HeadTitle.BorderSizePixel = 0
					HeadTitle.ClipsDescendants = true
					HeadTitle.Position = UDim2.new(0.5, 3, 0.5, 0)
					HeadTitle.Size = UDim2.new(0.949999988, 0, 0, 25)
					HeadTitle.ZIndex = 16
					HeadTitle.Font = Enum.Font.GothamBold
					HeadTitle.PlaceholderColor3 = Color3.fromRGB(255, 255, 255)
					HeadTitle.PlaceholderText = Placeholder
					HeadTitle.Text = ""
					HeadTitle.TextColor3 = Color3.fromRGB(255, 255, 255)
					HeadTitle.TextSize = 13.000
					HeadTitle.TextXAlignment = Enum.TextXAlignment.Center
					
					local ButtonColor44 = Instance.new("UIStroke")
					
					ButtonColor44.Thickness = 0.9
					ButtonColor44.Name = ""
					ButtonColor44.Parent = HeadTitle
					ButtonColor44.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
					ButtonColor44.LineJoinMode = Enum.LineJoinMode.Round
					ButtonColor44.Color = Color3.fromRGB(255, 0, 0)
					ButtonColor44.Transparency = 0.2

					Textbox.MouseEnter:Connect(function()
						TweenService:Create(
							ButtonColor44,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Thickness = 1}
						):Play()
						TweenService:Create(
							ButtonColor44,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Transparency = 0.5}
						):Play()
					end)

					Textbox.MouseLeave:Connect(function()
						TweenService:Create(
							ButtonColor44,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Thickness = 0.9}
						):Play()
						TweenService:Create(
							ButtonColor44,
							TweenInfo.new(0.4,Enum.EasingStyle.Quad,Enum.EasingDirection.Out),
							{Transparency = 0.2}
						):Play()
					end)
					
					HeadTitle.FocusLost:Connect(
					function(ep)
						if ep then
							if #HeadTitle.Text > 0 then
								callback(HeadTitle.Text)
								HeadTitle.Text = HeadTitle.Text
							end
						end
					end)
				end
				return function_main
			end
			return sections
		end
		return addtap
	end
	--return library
	
	local Win = library:Evil()
	local tab1 = Win:CraftTab('Main',12870309893)
		local page1 = tab1:CraftPage('Main',1)
		
		page1:Label('AutoFarm')
		
		_G.Settings.Main.Distance = 30
		page1:Toggle('Auto Farm \n ฟาร์มแบบอัตโนมัติ',_G.Settings.Main.AutoFarm,function(value) -- Mobile
			_G.AutoFarm = value
			_G.Settings.Main.AutoFarm = value
			StopTween(_G.AutoFarm)
			SaveSettings()
		end)
		
		page1:Toggle('Auto Farm Fast \n ฟาร์มฆ่าคนกลับเกาะลอยฟ้า',_G.Settings.Main.AutoFarmFast,function(a) -- Mobile
			_G.AutoFarmFast = a
			_G.Settings.Main.AutoFarmFast = a
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
				local MyLevel = game.Players.LocalPlayer.Data.Level.Value
				local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
				pcall(function()
					if _G.AutoFarm then
						if _G.AutoFarmFast  and (MyLevel >= 15 and MyLevel <= 300) then
							if MyLevel >= 15 and MyLevel <= 300 then
								AutoFarmFast()
							end
						else
							if QuestC.Visible == true then
								if game:GetService("Workspace").Enemies:FindFirstChild(QuestCheck()[3]) then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == QuestCheck()[3] then
											if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
												repeat task.wait()
													if not string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, QuestCheck()[6]) then
														game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
													else
														PosMon = v.HumanoidRootPart.CFrame
														v.HumanoidRootPart.Size = Vector3.new(60,60,60)
														v.HumanoidRootPart.CanCollide = false
														v.Humanoid.WalkSpeed = 0
														v.Head.CanCollide = false
														BringMobFarm = true
														EquipWeapon(_G.Select_Weapon)
														v.HumanoidRootPart.Transparency = 1
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 30, 5))
	
														if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO then
															game:GetService("VirtualUser"):CaptureController()
															game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
														end
													end
												until not _G.AutoFarm or not v.Parent or v.Humanoid.Health <= 0 or QuestC.Visible == false or not v:FindFirstChild("HumanoidRootPart")
											end
										end
									end
								else
									for i,v in pairs(workspace._WorldOrigin.EnemySpawns:GetChildren()) do
										if not _G.AutoFarmFast and v.Name == QuestCheck()[6] then local CFrameEnemySpawns = v.CFrame  wait(0.5)
											toposition(CFrameEnemySpawns * CFrame.new(0, 30, 5))
										end
									end
								end
							else
								repeat wait() toposition(QuestCheck()[2]) until (QuestCheck()[2].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarm
								if (QuestCheck()[2].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 1 then
									BringMobFarm = false
									wait(0.2)
									game:GetService('ReplicatedStorage').Remotes.CommF_:InvokeServer("StartQuest", QuestCheck()[4], QuestCheck()[1]) wait(0.5)
								end
								for i,v in pairs(workspace._WorldOrigin.EnemySpawns:GetChildren()) do
									if not _G.AutoFarmFast and v.Name == QuestCheck()[6] then local CFrameEnemySpawns = v.CFrame
										toposition(CFrameEnemySpawns * CFrame.new(0, 30, 5))
									end
								end
							end
						end
					end
				end)
			end
		end)
		function AutoFarmFast()
			local PlayersAll = game.Players:GetPlayers()
			local PlayerLevel = game.Players.LocalPlayer.Data.Level.Value
			_G.ChackPlayer = 0
			local quest = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text
			local Player = string.split(quest," ")[2]
			getgenv().SelectPly = string.split(quest," ")[2]
			pcall(function()
				local MyLevel = game.Players.LocalPlayer.Data.Level.Value
				local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
				CFrameMon = CFrame.new(-4837.64258, 850.10199, -1840.58374, -0.430530697, -4.42848638e-08, -0.90257591, -3.08042516e-08, 1, -3.43712756e-08, 0.90257591, 1.30052875e-08, -0.430530697)
	
				if MyLevel >= 15 and MyLevel <= 69 then
					BringMobFarm = false
					for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
						if v.Name == "God's Guard [Lv. 450]" then
							if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
								repeat task.wait()
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
									v.HumanoidRootPart.CanCollide = false
									v.Humanoid.WalkSpeed = 0
									v.Head.CanCollide = false
									BringMobFarm = true
									EquipWeapon(_G.Select_Weapon)
									PosMon = v.HumanoidRootPart.CFrame
									v.HumanoidRootPart.Size = Vector3.new(60,60,60)
									v.HumanoidRootPart.Transparency = 1
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 30, 5))
									if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
										game:GetService("VirtualUser"):CaptureController()
										game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
									end
								until not v.Parent or not _G.AutoFarmFast or v.Humanoid.Health < 0
							end
						else
						BringMobFarm = false
						if _G.AutoFarm and _G.AutoFarmFast and (CFrameMon.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude > 500 then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-4607.82275, 872.54248, -1667.55688))
						end
							toposition(CFrameMon)
						end
					end
				elseif MyLevel >= 70 and MyLevel <= 309 then
					if game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("PlayerHunter")
					end
					if QuestC.Visible == false then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("PlayerHunter")
					elseif QuestC.Visible == true then
						if string.find(quest,"Defeat") then
							if game.Players[getgenv().SelectPly].Data.Level.Value >= 20 and game.Players[getgenv().SelectPly].Data.Level.Value <= MyLevel + 50 then
								repeat task.wait()
									if not game.Players.LocalPlayer.Character:FindFirstChild("HasBuso") then
										game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Buso")
									end
									if game:GetService("Players").LocalPlayer.PlayerGui.Main.PvpDisabled.Visible == true then
										game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EnablePvp")
									end
									EquipWeapon(_G.Select_Weapon)
									_G.FastAttack = false
									TPPlayer(game:GetService("Players")[getgenv().SelectPly].Character.HumanoidRootPart.CFrame*CFrame.new(0,15,5))
									wait(0.2)
									TPPlayer(game:GetService("Players")[getgenv().SelectPly].Character.HumanoidRootPart.CFrame*CFrame.new(0,0,2))
									game:GetService("Players")[getgenv().SelectPly].Character.HumanoidRootPart.Size = Vector3.new(120,120,120)
	
									game:service('VirtualInputManager'):SendKeyEvent(true, "X", false, game)
									game:service('VirtualInputManager'):SendKeyEvent(false, "X", false, game)
	
									game:service('VirtualInputManager'):SendKeyEvent(true, "Z", false, game)
									game:service('VirtualInputManager'):SendKeyEvent(false, "Z", false, game)
	
									if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
										game:GetService("VirtualUser"):CaptureController()
										game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
									end
								until game.Players[getgenv().SelectPly].Character.Humanoid.Health <= 0 or not game.Players[getgenv().SelectPly].Character.Humanoid.Health or not AutoFarmFast()
								_G.AutoFarmFast = false
								_G.FastAttack = true
								_G.KillPlayerQ = true
								if not game.Players:FindFirstChild(Player) then
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("PlayerHunter")
								end
							else
								for i,v in pairs(PlayersAll) do
									if v.Data.Level.Value >= 20 and v.Data.Level.Value <= PlayerLevel + 50 then
										game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("PlayerHunter")
										print(v)
									else
										_G.ChackPlayer = _G.ChackPlayer + 1
										if _G.ChackPlayer >= 12 then
											_G.AutoFarmFast = false
										else
											if _G.ChackPlayer >= 15 then 
												_G.AutoFarmFast = false
											end
											print("Chack Player ".._G.ChackPlayer)
										end
									end
								end
							end
						end
					end
				end
			end)
		end
	
	
		page1:Toggle('Auto Farm Mon Aura \n ออโต้ฟาร์มมอนรอบๆ',_G.Settings.Main.Mon_Aura,function(a) -- Mobile
			_G.Mon_Aura = a
			_G.Settings.Main.Mon_Aura = a
			SaveSettings()
		end)
		
		page1:Label('Auto World')
		
		page1:Toggle('Auto New World \n อัตโนมัติไปโลก2',_G.Settings.Main.Auto_New_World,function(a) -- Mobile
			_G.Auto_New_World = a
			_G.Settings.Main.Auto_New_World = a
			StopTween(_G.Auto_New_World)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					if _G.Auto_New_World and W1 then
						if game.Players.LocalPlayer.Data.Level.Value >= 700 then
							_G.AutoFarm = false
							BringMobFarm = false
							if game.Workspace.Map.Ice.Door.CanCollide == true and game.Workspace.Map.Ice.Door.Transparency == 0 then
								wait(.5)
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("DressrosaQuestProgress","Detective")
								EquipWeapon("Key")
								repeat wait() toposition(CFrame.new(1347.7124, 37.3751602, -1325.6488)) until (CFrame.new(1347.7124, 37.3751602, -1325.6488).Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 3 or not _G.Settings.Main["Auto New World"]
							elseif game.Workspace.Map.Ice.Door.CanCollide == false and game.Workspace.Map.Ice.Door.Transparency == 1 then
								if game:GetService("Workspace").Enemies:FindFirstChild("Ice Admiral [Lv. 700] [Boss]") then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == "Ice Admiral [Lv. 700] [Boss]" and v.Humanoid.Health > 0 then
											repeat wait()
												if not game.Players.LocalPlayer.Character:FindFirstChild("HasBuso") then
													game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Buso")
												end
												v.HumanoidRootPart.Size = Vector3.new(60,60,60)
												v.HumanoidRootPart.Transparency = 1
												v.Humanoid.JumpPower = 0
												v.Humanoid.WalkSpeed = 0
												v.HumanoidRootPart.CanCollide = false
												v.Humanoid:ChangeState(11)
												EquipWeapon(_G.Select_Weapon)
												game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelDressrosa")
												
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 30, 5))
												
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until v.Humanoid.Health <= 0 or not v.Parent
											game:GetService("ReplicatedStorage"):InvokeServer("TravelDressrosa")
										end
									end
								else
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelDressrosa")
									toposition(CFrame.new(1347.7124, 37.3751602, -1325.6488))
								end
							else
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelDressrosa")
							end
						else
							if _G.Settings.Main.AutoFarm == true then
								_G.AutoFarm = true
							end
						end
					end
				end)
			end
		end)
		
		page1:Toggle('Auto Third World \n อัตโนมัติไปโลก3',_G.Settings.Main.Auto_Third_World,function(a) -- Mobile
			_G.AutoThirdSea = a
			_G.Settings.Main.Auto_Third_World = a
			StopTween(_G.AutoThirdSea)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoThirdSea then
					pcall(function()
						if _G.AutoThirdSea then
							_G.AutoFarm = false
							if game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ZQuestProgress","Check") == 0 then
								toposition(CFrame.new(-1926.3221435547, 12.819851875305, 1738.3092041016))
								if (CFrame.new(-1926.3221435547, 12.819851875305, 1738.3092041016).Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 10 then
									wait(1.5)
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ZQuestProgress","Begin")
								end
								wait(1.8)
								if game:GetService("Workspace").Enemies:FindFirstChild("rip_indra [Lv. 1500] [Boss]") then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == "rip_indra [Lv. 1500] [Boss]" then
											OldCFrameThird = v.HumanoidRootPart.CFrame
											repeat task.wait()
												EquipWeapon(_G.Select_Weapon)
												if AttackRandomType == 1 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												elseif AttackRandomType == 2 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
												elseif AttackRandomType == 3 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
												else
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												end
												v.HumanoidRootPart.CFrame = OldCFrameThird
												v.HumanoidRootPart.Size = Vector3.new(50,50,50)
												v.HumanoidRootPart.CanCollide = false
												v.Humanoid.WalkSpeed = 0
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
												game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelZou")
												sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
											until _G.AutoThirdSea == false or v.Humanoid.Health <= 0 or not v.Parent
										end
									end
								elseif not game:GetService("Workspace").Enemies:FindFirstChild("rip_indra [Lv. 1500] [Boss]") and (CFrame.new(-26880.93359375, 22.848554611206, 473.18951416016).Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 1000 then
									toposition(CFrame.new(-26880.93359375, 22.848554611206, 473.18951416016))
								end
							end
						end
					end)
				end
			end
		end)
		
		page1:Label('Auto Farm Chest')
		
		page1:Toggle('Auto Farm Chest Tween \n อัตโนมัติเก็บกล่องแบบทวีน',_G.Settings.Main.Auto_Farm_Chest,function(a) -- Mobile
			_G.Auto_Farm_Chest = a
			_G.Settings.Main.Auto_Farm_Chest = a
			StopTween(_G.Auto_Farm_Chest)
			SaveSettings()
		end)
		
		_G.Poo = 1500
		spawn(function()
			while wait() do 
				if _G.Auto_Farm_Chest then
					for i,v in pairs(game:GetService("Workspace"):GetChildren()) do 
						if v.Name:find("Chest") then
							if game:GetService("Workspace"):FindFirstChild(v.Name) then
								repeat wait()
									if game:GetService("Workspace"):FindFirstChild(v.Name) then
										toposition(v.CFrame)
									end
								until _G.Auto_Farm_Chest  == false or not v.Parent
								break
							end
						end
					end
				end
			end
		end)
		
		page1:Toggle('Auto Farm Chest TP \n อัตโนมัติเก็บกล่องแบบวาป',_G.Settings.Main.Auto_Farm_Chest_TP,function(a) -- Mobile
			_G.Auto_Farm_Chest_TP = a
			_G.Settings.Main.Auto_Farm_Chest_TP = a
			StopTween(_G.Auto_Farm_Chest_TP)
			SaveSettings()
		end)
		
		_G.Po = 1500
		spawn(function()
			while wait() do 
				if _G.Auto_Farm_Chest_TP then
					for i,v in pairs(game:GetService("Workspace"):GetChildren()) do 
						if v.Name:find("Chest") then
							if game:GetService("Workspace"):FindFirstChild(v.Name) then
								--if (v.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 9000+_G.Po then
									repeat wait()
										if game:GetService("Workspace"):FindFirstChild(v.Name) then
											game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.CFrame
										end
									until _G.Auto_Farm_Chest_TP == false or not v.Parent
									_G.Po = _G.Po + 1000
									break
								--end
							end
						end
					end
				end
			end
		end)
		
		page1:Label('Auto Farm Mastery')
		
		page1:Toggle('Auto Farm Mastery \n อัตโนมัติผลปีศาจ',_G.Settings.Main.AutoFruitMastery,function(a) -- Mobile
			_G.AutoFruitMastery = a
			_G.Settings.Main.AutoFruitMastery = a
			UseSkill = false
			StopTween(_G.AutoFruitMastery)
			SaveSettings()
		end)
		
		function EquipBloxFruit()
			for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
				if v.ToolTip == "Blox Fruit" then
				   if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
						 EquipWeapon(v.Name)
					end
				end
			end
		end
	
		spawn(function()
			while task.wait() do
				pcall(function()
					if _G.AutoFruitMastery then
						local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
						BringMobFarm = false
						if QuestC.Visible == false then
						repeat wait() toposition(QuestCheck()[2]) until (QuestCheck()[2].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarm
						if (QuestCheck()[2].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 500 * 50 / 50 + 2500 then
							wait(.2)
							BringMobFarm = false
							wait(.2)
							game:GetService('ReplicatedStorage').Remotes.CommF_:InvokeServer("StartQuest", QuestCheck()[4], QuestCheck()[1])
							wait(0.5)
							WaitMon = true
						end
						repeat wait() toposition(QuestCheck()[7]) until (QuestCheck()[7].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarm
							elseif QuestC.Visible == true then
									if game:GetService("Workspace").Enemies:FindFirstChild(QuestCheck()[3]) then
										for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
											if v.Name == QuestCheck()[3] and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
											repeat task.wait()
												if v.Humanoid.Health <= v.Humanoid.MaxHealth * _G.Settings.Main.HealthMs/100 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 0))
													for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
														if v.ToolTip == "Blox Fruit" then
														   if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
																EquipWeapon(v.Name)
															end
														end
													end
													UseSkill = true
													PosMon = v.HumanoidRootPart.CFrame
													v.HumanoidRootPart.Size = Vector3.new(60,60,60)
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid.WalkSpeed = 0
													v.Head.CanCollide = false
													BringMobFarm = true
													v.HumanoidRootPart.TranAsparency = 1
												else
													UseSkill = false
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 0))
													PosMon = v.HumanoidRootPart.CFrame
													v.HumanoidRootPart.Size = Vector3.new(60,60,60)
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid.WalkSpeed = 0
													v.Head.CanCollide = false
													BringMobFarm = true
													EquipWeapon(_G.Select_Weapon)
													v.HumanoidRootPart.Transparency = 1
													if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
														game:GetService("VirtualUser"):CaptureController()
														game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
													end
												end
											until not _G.AutoFruitMastery or QuestC.Visible == false or not v.Parent or v.Humanoid.Health < 0
										end
									end
								end
								if not string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, QuestCheck()[6]) then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
							end
						end
					end
				end)
			end
		end)
	
		page1:Toggle('AutoFarm Mastery Gun \n อัตโนมัติฟาร์มปืน',_G.Settings.Main.AutoFarmMasteryGun,function(a) -- Mobile
			_G.AutoFarmMasteryGun = a
			_G.Settings.Main.AutoFarmMasteryGun = a
			StopTween(_G.AutoFarmMasteryGun)
			SaveSettings()
		end)
		spawn(function()
			while task.wait() do
				pcall(function()
					if _G.AutoFarmMasteryGun then
						local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
						BringMobFarm = false
						if QuestC.Visible == false then
						repeat wait() toposition(QuestCheck()[2]) until (QuestCheck()[2].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarm
						if (QuestCheck()[2].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 500 * 50 / 50 + 2500 then
							wait(.2)
							BringMobFarm = false
							wait(.2)
							game:GetService('ReplicatedStorage').Remotes.CommF_:InvokeServer("StartQuest", QuestCheck()[4], QuestCheck()[1])
							wait(0.5)
							WaitMon = true
						end
						repeat wait() toposition(QuestCheck()[7]) until (QuestCheck()[7].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarm
							elseif QuestC.Visible == true then
									if game:GetService("Workspace").Enemies:FindFirstChild(QuestCheck()[3]) then
										for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
											if v.Name == QuestCheck()[3] and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
											repeat task.wait()
												if v.Humanoid.Health <= v.Humanoid.MaxHealth * _G.Settings.Main.HealthMs/100 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 1, _G.Settings.Main.Distance))
													EquipWeapon(SelectGun)
													UseSkill = true
													PosMon = v.HumanoidRootPart.CFrame
													v.HumanoidRootPart.Size = Vector3.new(60,60,60)
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid.WalkSpeed = 0
													v.Head.CanCollide = false
													BringMobFarm = true
													v.HumanoidRootPart.TranAsparency = 1
												else
													UseSkill = false
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 1, _G.Settings.Main.Distance))
													PosMon = v.HumanoidRootPart.CFrame
													v.HumanoidRootPart.Size = Vector3.new(60,60,60)
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid.WalkSpeed = 0
													v.Head.CanCollide = false
													BringMobFarm = true
													EquipWeapon(_G.Select_Weapon)
													v.HumanoidRootPart.Transparency = 1
													if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
														game:GetService("VirtualUser"):CaptureController()
														game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
													end
												end
											until not _G.AutoFarmMasteryGun or QuestC.Visible == false or not v.Parent or v.Humanoid.Health < 0
										end
									end
								end
								if not string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, QuestCheck()[6]) then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
							end
						end
					end
				end)
			end
		end)
		spawn(function()
			while task.wait() do
				pcall(function()
					 for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do
						 if v:IsA("Tool") then
							 if v:FindFirstChild("RemoteFunctionShoot") then
								 SelectGun = v.Name
							end
						end
					end
				end)
			end
		end)
	
		page1:Label('Auto Farm Boss')
		
		local Boss = {}
		
		for i, v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do
			if string.find(v.Name, "Boss") then
				if v.Name == "Ice Admiral [Lv. 700] [Boss]" then
					else
					table.insert(Boss, v.Name)
				end
			end
		end
		
		if _G.Settings.Main.Select_Boss == nil then
			_G.Settings.Main.Select_Boss = ""
		end
		
		local BossName = page1:Dropdown("Select Boss \n เลือกบอส",Boss,_G.Settings.Main.Select_Boss,function(a)
			_G.Settings.Main.Select_Boss = a
			SaveSettings()
		end)
		
		page1:Button('Refresh Boss \n รีเซ็ตบอส',function()
		BossName:Clear()
			for i, v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do
				if string.find(v.Name, "Boss") then
					BossName:Add(v.Name) 
				end
			end
		end)
		
		page1:Toggle('Auto Farm Boss \n อัตโนมัติตีบอส',_G.Settings.Main.AutoFarmBoss,function(a) -- Mobile
			_G.AutoFarmBoss = a
			_G.Settings.Main.AutoFarmBoss = a
			StopTween(_G.AutoFarmBoss)
			SaveSettings()
		end)
		
		spawn(function()
			while task.wait() do
				if _G.AutoFarmBoss then
					pcall(function()
					BringMobFarm = false
						for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
							if v.Name == _G.Settings.Main.Select_Boss then
								if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
									repeat task.wait()
										EquipWeapon(_G.Select_Weapon)
										v.HumanoidRootPart.CanCollide = false
										v.Humanoid.WalkSpeed = 0
										v.Head.CanCollide = false
										v.HumanoidRootPart.Size = Vector3.new(60,60,60)
										if AttackRandomType == 1 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										elseif AttackRandomType == 2 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
										elseif AttackRandomType == 3 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
										else
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										end
										if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
											game:GetService("VirtualUser"):CaptureController()
											game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
										end
									until not _G.AutoFarmBoss or not v.Parent or v.Humanoid.Health <= 0
								end
							end
						end
						BringMobFarm = false
						EquipMelee = false
						for i,v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do 
							if v.Name == _G.Settings.Main.Select_Boss then
								toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
							end
						end
					end)
				end
			end
		end)
		
		page1:Toggle('Auto Farm Boss Quest \n อัตโนมัติตีบอสรับเควส',_G.Settings.Main.AutoFarmBossQuest,function(a) -- Mobile
			_G.AutoFarmBossQuest = a
			_G.Settings.Main.AutoFarmBossQuest = a
			StopTween(_G.AutoFarmBossQuest)
			SaveSettings()
		end)
		
		local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
		spawn(function()
			while task.wait() do
				if _G.AutoFarmBossQuest then
					pcall(function()
					CheckBossQuest()
					if not string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, NameBoss) then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
				   end
					if QuestC.Visible == false then
					   repeat wait() toposition(CFrameQuestBoss) until (CFrameQuestBoss.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarm
						 if (CFrameQuestBoss.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 500 * 50 / 50 + 2500 then
							 wait(1.5)
							 game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("StartQuest", NameQuestBoss, LevelQuestBoss)
						end
					elseif QuestC.Visible == true then
					 CheckBossQuest()
						for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
							if v.Name == MsBoss then
								if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
									repeat task.wait()
										EquipWeapon(_G.Select_Weapon)
										v.HumanoidRootPart.CanCollide = false
										v.Humanoid.WalkSpeed = 0
										v.Head.CanCollide = false
										v.HumanoidRootPart.Size = Vector3.new(60,60,60)
										if AttackRandomType == 1 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										elseif AttackRandomType == 2 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
										elseif AttackRandomType == 3 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
										else
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										end
										if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
											game:GetService("VirtualUser"):CaptureController()
											game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
										end
									until not _G.AutoFarmBossQuest or not v.Parent or v.Humanoid.Health <= 0
								end
							end
						end
						BringMobFarm = false
						EquipMelee = false
						for i,v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do 
							if v.Name == _G.Settings.Main.Select_Boss then
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
								end
							end
						end
					end)
				end
			end
		end)
		
		page1:Toggle('Auto Farm Boss All \n อัตโนมัติตีบอสทั้งหมด',_G.Settings.Main.AutoFarmBossAll,function(a) -- Mobile
			_G.AutoFarmBossAll = a
			_G.Settings.Main.AutoFarmBossAll = a
			StopTween(_G.AutoFarmBossAll)
			SaveSettings()
		end)
		
		spawn(function()
			while task.wait() do
				if _G.AutoFarmBossAll then
					pcall(function()
					BringMobFarm = false
					EquipWeapon(_G.Select_Weapon)
						for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
							if string.find(v.Name,"Boss") then
								if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
									repeat task.wait()
										--EquipWeapon(_G.Select_Weapon)
										v.HumanoidRootPart.CanCollide = false
										v.Humanoid.WalkSpeed = 0
										v.Head.CanCollide = false
										EquipWeapon(_G.Select_Weapon)
										v.HumanoidRootPart.Size = Vector3.new(60,60,60)
										if AttackRandomType == 1 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										elseif AttackRandomType == 2 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
										elseif AttackRandomType == 3 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
										else
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										end
										if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
											game:GetService("VirtualUser"):CaptureController()
											game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
										end
									until not _G.AutoFarmBossAll or not v.Parent or v.Humanoid.Health <= 0
								end
							end
						end
						BringMobFarm = false
						EquipMelee = false
						for i,v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do 
							if string.find(v.Name,"Boss") then
								toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
							end
						end
					end)
				end
			end
		end)
		
		
		page1:Label('FarmKen')
		
		page1:Toggle('Auto Farm Observation \n อัตโนมัฮาคิสังเกด',_G.Settings.Main.AutoFarmBossAll,function(a) -- Mobile
			_G.AutoObservation = a
			_G.Settings.Main.AutoObservation = a
			StopTween(_G.AutoObservation)
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while wait() do
					if _G.AutoObservation then
						if game:GetService("Players").LocalPlayer.VisionRadius.Value >= 999999 then
							wait()
						else
						CheckQuest()
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if v.Name == Mon then
									if game:GetService("Players").LocalPlayer.PlayerGui.ScreenGui:FindFirstChild("ImageLabel") then
										repeat task.wait()
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(3, 0, 0))
										until _G.AutoObservation == false or not game:GetService("Players").LocalPlayer.PlayerGui.ScreenGui:FindFirstChild("ImageLabel")
									else
										repeat task.wait()
											if not game:GetService("Players").LocalPlayer.PlayerGui.ScreenGui:FindFirstChild("ImageLabel") then
												game:GetService('VirtualUser'):CaptureController()
												game:GetService('VirtualUser'):SetKeyDown('0x65')
												wait(2)
												game:GetService('VirtualUser'):SetKeyUp('0x65')
											end
											toposition(CFrameMon)
										until _G.AutoObservation == false or game:GetService("Players").LocalPlayer.PlayerGui.ScreenGui:FindFirstChild("ImageLabel")
									end
								else
									toposition(CFrameMon)
								end
							end
						end
					end
				end
			end)
		end)
		
		if W1 then
		page1:Label('World : 1')
		
		page1:Toggle('Auto Saber \n ออโต้หาดายแช็ง',_G.Settings.Main.AutoSaber,function(a) -- Mobile
			_G.AutoSaber = a
			_G.Settings.Main.AutoSaber = a
			StopTween(_G.AutoSaber)
			SaveSettings()
		end)
		
	
		spawn(function()
			while task.wait() do
				pcall(function()
					if _G.AutoSaber and game.Players.LocalPlayer.Data.Level.Value >= 200 and Check_Sword("Saber") == nil then
						if _G.AutoFarm == true then
							_G.AutoFarm = false
							toposition(game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.CFrame)
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
							_G.AutoFarm = false
						end
						if game:GetService("Workspace").Map.Jungle.Final.Part.Transparency == 0 then
							if game:GetService("Workspace").Map.Jungle.QuestPlates.Door.Transparency == 0 then
								if (CFrame.new(-1480.06018, 47.9773636, 4.53454018, -0.386713833, 1.11673025e-07, 0.922199786, 7.96717785e-08, 1, -8.76847395e-08, -0.922199786, 3.95643944e-08, -0.386713833).Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 100 then
									toposition(game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.CFrame)
									task.wait(1)
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").Map.Jungle.QuestPlates.Plate1.Button.CFrame
									task.wait(1)
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").Map.Jungle.QuestPlates.Plate2.Button.CFrame
									task.wait(1)
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").Map.Jungle.QuestPlates.Plate3.Button.CFrame
									task.wait(1)
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").Map.Jungle.QuestPlates.Plate4.Button.CFrame
									task.wait(1)
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").Map.Jungle.QuestPlates.Plate5.Button.CFrame
									task.wait(1) 
								end
							local CFrameSaber = CFrame.new(-1480.06018, 47.9773636, 4.53454018, -0.386713833, 1.11673025e-07, 0.922199786, 7.96717785e-08, 1, -8.76847395e-08, -0.922199786, 3.95643944e-08, -0.386713833)
							if _G.Settings.Main.AutoSaber and (CFrameSaber.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude > 1200 then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
								toposition(CFrameSaber)
							end
							toposition(CFrameSaber)
						else
							if game:GetService("Workspace").Map.Desert.Burn.Part.Transparency == 0 then
								if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Torch") or game.Players.LocalPlayer.Character:FindFirstChild("Torch") then
									EquipWeapon("Torch")
									toposition(CFrame.new(1113.7229, 5.04679585, 4350.33691, -0.541527212, 5.27007726e-09, 0.840683222, 8.74004868e-08, 1, 5.00303372e-08, -0.840683222, 1.00568911e-07, -0.541527212))
									UnEquipWeapon("Torch")
									EquipWeapon("Torch")
									task.wait(0.5)
								else
									toposition(CFrame.new(-1610.56824, 12.1773882, 162.830322, -0.907543361, -2.88120088e-08, -0.419958383, -4.66550922e-08, 1, 3.22163096e-08, 0.419958383, 4.88308949e-08, -0.907543361))                 
								end
							else
								if game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","SickMan") ~= 0 then
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","GetCup")
									task.wait(0.5)
									EquipWeapon("Cup")
									task.wait(0.5)
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","FillCup",game:GetService("Players").LocalPlayer.Character.Cup)
									task.wait(0)
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","SickMan") 
								else
									if game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","RichSon") == nil then
										game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","RichSon")
									elseif  game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","RichSon") == 0 then
										if game:GetService("Workspace").Enemies:FindFirstChild("Mob Leader [Lv. 120] [Boss]") or game:GetService("ReplicatedStorage"):FindFirstChild("Mob Leader [Lv. 120] [Boss]") then
											for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
												if v.Name == "Mob Leader [Lv. 120] [Boss]" then
													if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
														repeat task.wait()
															EquipWeapon(_G.Select_Weapon)
															v.HumanoidRootPart.CanCollide = false
															v.Humanoid.WalkSpeed = 0
															v.Head.CanCollide = false
															v.HumanoidRootPart.Size = Vector3.new(100,100,100)
															v.HumanoidRootPart.Transparency = 1
															EquipWeapon(_G.Select_Weapon)
															toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
															if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then game:GetService'VirtualUser':CaptureController() game:GetService'VirtualUser':Button1Down(Vector2.new(1280, 672)) end
														until v.Humanoid.Health <= 0 or _G.AutoSaber == false
													end
												end
											for i,v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do 
												if v.Name == "Mob Leader [Lv. 120] [Boss]" then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
												end
											end
										end
									end
								elseif game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","RichSon") == 1 then
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","RichSon")
									task.wait(0.5)
									EquipWeapon("Relic")
									task.wait(0.5)
									toposition(CFrame.new(-1406.37512, 29.9773273, 4.45027685, 0.877344251, -3.82776442e-08, 0.479861468, 4.93218133e-09, 1, 7.07504668e-08, -0.479861468, -5.9705755e-08, 0.877344251))
								end
							end
						end
					end
						else
							if game:GetService("Workspace").Enemies:FindFirstChild("Saber Expert [Lv. 200] [Boss]") or game:GetService("ReplicatedStorage"):FindFirstChild("Saber Expert [Lv. 200] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Saber Expert [Lv. 200] [Boss]" then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.Size = Vector3.new(60,60,60)
											v.HumanoidRootPart.Transparency = 1
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,30,5))
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then game:GetService'VirtualUser':CaptureController() game:GetService'VirtualUser':Button1Down(Vector2.new(1280, 672)) end
										until v.Humanoid.Health <= 0 or _G.AutoSaber == false
										if _G.Settings.Main.AutoFarm == true then
											_G.AutoSaber = false
											_G.Settings.Main.AutoSaber = false
											_G.AutoFarm = true
										end
										if v.Humanoid.Health <= 0 then
											game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("ProQuestProgress","PlaceRelic")
										end
									end
								end
							else 
								if _G.Settings.Main.AutoFarm == true then
									_G.AutoSaber = false
									_G.Settings.Main.AutoSaber = false
									_G.AutoFarm = true
								else
									toposition(CFrame.new(-1445.08325, 23.9773273, -73.7635269, 0.555137634, 2.11396189e-09, 0.831758499, 1.55808237e-08, 1, -1.29406112e-08, -0.831758499, 2.01433039e-08, 0.555137634))
								end
							end
						end
					elseif _G.AutoSaber and Check_Sword("Saber") == true then
						if _G.Settings.Main.AutoFarm == true then
							_G.AutoFarm = true
							_G.AutoRengoku = false
							_G.Settings.Main.AutoRengoku = false
							SaveSettings()
						end
					end
				end)
			end
		end)
		
		page1:Toggle('Auto Pole \n ออโต้หา โพล์',_G.Settings.Main.AutoPole,function(a) -- Mobile
			_G.AutoPole = a
			_G.Settings.Main.AutoPole = a
			StopTween(_G.AutoPole)
			SaveSettings()
		end)
		
		spawn(function()
			while task.wait() do
				if _G.AutoPole and Check_Sword("Pole") == nil then
					_G.AutoFarm = false
					pcall(function()
						for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
							if v.Name == "Thunder God [Lv. 575] [Boss]" then
								if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
									repeat task.wait()
										--EquipWeapon(_G.Select_Weapon)
										v.HumanoidRootPart.CanCollide = false
										v.Humanoid.WalkSpeed = 0
										v.Head.CanCollide = false
										v.HumanoidRootPart.Size = Vector3.new(100,100,100)
										v.HumanoidRootPart.Transparency = 1
										EquipWeapon(_G.Select_Weapon)
										if AttackRandomType == 1 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										elseif AttackRandomType == 2 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
										elseif AttackRandomType == 3 then
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
										else
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
										end
										if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
											game:GetService("VirtualUser"):CaptureController()
											game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
										end
									until not _G.AutoPole or not v.Parent or v.Humanoid.Health <= 0
								end
							end
						end
						EquipMelee = false
						for i,v in pairs(game:GetService("ReplicatedStorage"):GetChildren()) do 
							if v.Name == "Thunder God [Lv. 575] [Boss]"  then
								toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
							end
						end
					end)
				elseif _G.AutoPole and Check_Sword("Pole") == true then
					if _G.Settings.Main.AutoFarm == true then
						_G.AutoFarm = true
						_G.AutoRengoku = false
						_G.Settings.Main.AutoRengoku = false
						SaveSettings()
					end
				end
			end
		end)
		
		page1:Toggle('Auto Ablility \n ซื้อทุกฮาคิ',_G.Settings.Main.AutoAblility,function(a) -- Mobile
			_G.AutoAblility = a
			_G.Settings.Main.AutoAblility = a
			SaveSettings()
		end)
		
		task.spawn(function()
			while wait() do
				pcall(function()
					if _G.AutoAblility then
						local Beli = game:GetService("Players").LocalPlayer.Data.Beli.Value
						local BusoCheck = false
						local SoruCheck = false
						local GeppoCheck = false
						local KenCheck = false
						if Beli >= 885000 then
							repeat wait() 
								local args = {
									[1] = "BuyHaki",
									[2] = "Buso"
								}
		
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
								BusoCheck = true
								local args = {
									[1] = "BuyHaki",
									[2] = "Geppo"
								}
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
								GeppoCheck = true
								local args = {
									[1] = "BuyHaki",
									[2] = "Soru"
								}
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
								SoruCheck = true
								local args = {
									[1] = "KenTalk",
									[2] = "Start"
								}
		
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
		
								local args = {
									[1] = "KenTalk",
									[2] = "Buy"
								}
		
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
								KenCheck = true
							until not BusoCheck and not GeppoCheck and not SoruCheck and not KenCheck or not _G.Settings.Main["Auto Buy Ablility"]
						end
					end
				end)
			end
		end)
	
		page1:Toggle('UpLock Ablility Free \n รับทุกฮาคิ',_G.Settings.Main.UpLockAblility,function(a) -- Mobile
			_G.UpLockAblility = a
			_G.Settings.Main.UpLockAblility = a
			SaveSettings()
		end)
		
		task.spawn(function()
			while wait() do
				if _G.UpLockAblility then
					pcall(function()
						if type("Buso") == 'string' then
							game:GetService("CollectionService"):AddTag(game.Players.LocalPlayer.Character,"Buso")
						elseif type("Buso") == 'table' then
							for i,v in next , {"Buso"} do
								game:GetService("CollectionService"):AddTag(game.Players.LocalPlayer.Character,v)
							end
						end
						if type("Soru") == 'string' then
							game:GetService("CollectionService"):AddTag(game.Players.LocalPlayer.Character,"Soru")
						elseif type("Soru") == 'table' then
							for _,v in next , {"Soru"} do
								game:GetService("CollectionService"):AddTag(game.Players.LocalPlayer.Character,v)
							end
						end
						if type("Geppo") == 'string' then
							game:GetService("CollectionService"):AddTag(game.Players.LocalPlayer.Character,"Geppo")
						elseif type("Geppo") == 'table' then
							for _,v in next , {"Geppo"} do
								game:GetService("CollectionService"):AddTag(game.Players.LocalPlayer.Character,v)
							end
						end
					end)
				end
			end
		end)
	
		elseif W2 then
		page1:Label('World : 2')
		page1:Label('Auto Farm Factory')
		page1:Toggle("Auto Farm Factory \n ตีโรงงาน",_G.Settings.Main.AutoFactory,function(value)
			_G.AutoFactory = value
			_G.Settings.Main.AutoFactory = value
			StopTween(_G.AutoFactory)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					if _G.AutoFactory then
						if game:GetService("Workspace").Enemies:FindFirstChild("Core") then
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if v.Name == "Core" and v.Humanoid.Health > 0 then
									repeat task.wait()
										EquipWeapon(_G.Select_Weapon)           
										toposition(CFrame.new(448.46756, 199.356781, -441.389252))                                  
										game:GetService("VirtualUser"):CaptureController()
										game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
									until v.Humanoid.Health <= 0 or _G.AutoFactory == false
								end
							end
						else
							toposition(CFrame.new(448.46756, 199.356781, -441.389252))
						end
					end
				end)
			end
		end)
		
		page1:Label('Legendary Sword')
		page1:Toggle("Auto Legendary Sword Buy \n ซื้อดาปโซโล",_G.Settings.Main.AutoBuyLegendarySword,function(value)
			_G.AutoBuyLegendarySword = value
			_G.Settings.Main.AutoBuyLegendarySword = value
			SaveSettings()
		end)
		
		page1:Toggle("Auto Legendary Sword Farm \n ฟาร์มดาบโซโล",_G.Settings.Main.Auto_True_Tripls_Katana,function(value)
			_G.Auto_True_Tripls_Katana = value
			_G.Settings.Main.Auto_True_Tripls_Katana = value
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.Auto_True_Tripls_Katana and W2 then
					pcall(function()
						local Beli = game:GetService("Players").LocalPlayer.Data.Beli.Value
						if Box_eventory("True Triple Katana") then
							_G.Settings.Main.Select_Weapon = "Sword"
							RunFarmBata()
						elseif Beli > 6000000 and not _G.Swords.L["Shisui"] and not _G.Swords.L["Saddi"] and not _G.Swords.L["Wando"] then
							if not _G.Swords.L["Shisui"] and not _G.Swords.L["Saddi"] and not _G.Swords.L["Wando"] then
								Do_Katana.Refresh("Doing True Triple Katana : looking for a sword dealer")
								game:GetService("ReplicatedStorage").Remotes["CommF_"]:InvokeServer("MysteriousMan", "2")
								RunFarmBata()
							end
						elseif _G.Swords.L["Shisui"] == true and _G.Swords.L["Saddi"] == true and _G.Swords.L["Wando"] == true then
							pcall(function()
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("MysteriousMan","1")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("MysteriousMan","2")
							end)
						else
							RunFarmBata()
						end
					end)
				end
			end
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoBuyLegendarySword then
					pcall(function()
						local args = {
							[1] = "LegendarySwordDealer",
							[2] = "1"
						}
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
						local args = {
							[1] = "LegendarySwordDealer",
							[2] = "2"
						}
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
						local args = {
							[1] = "LegendarySwordDealer",
							[2] = "3"
						}
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
					end)
				end 
			end
		end)
		page1:Label('Enchancement Colour')
		page1:Toggle("Auto Enchancement Buy \n ซื้อสีฮาคิ",_G.Settings.Main.AutoBuyEnchancement,function(value)
			_G.AutoBuyEnchancement = value
			_G.Settings.Main.AutoBuyEnchancement = value
			SaveSettings()
		end)
		
		page1:Toggle("Auto Enchancement Buy Hop \n ซื้อสีฮาคิออก",_G.Settings.Main.AutoBuyEnchancementColour_Hop,function(value)
			_G.AutoBuyEnchancementColour_Hop = value
			_G.Settings.Main.AutoBuyEnchancementColour_Hop = value
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoBuyEnchancement then
					local args = {
						[1] = "ColorsDealer",
						[2] = "2"
					}
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
					if _G.AutoBuyEnchancementColour_Hop and _G.AutoBuyEnchancementColour and not World1 then
						wait(10)
						Hop()
					end
				end 
			end
		end)
		elseif W3 then
		page1:Label('World : 3')
		page1:Label('Auto Cake Prince')
		
		local Mob_Kill_Cake_Prince = page1:Label("Label")
		
		spawn(function()
			while wait() do wait(0.2)
				pcall(function()
					if string.len(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner")) == 88 then
						Mob_Kill_Cake_Prince.Refresh("Kill : "..string.sub(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner"),39,41).." : More!!!")
					elseif string.len(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner")) == 87 then
						Mob_Kill_Cake_Prince.Refresh("Kill : "..string.sub(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner"),39,40).." : More!!!")
					elseif string.len(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner")) == 86 then
						Mob_Kill_Cake_Prince.Refresh("Kill : "..string.sub(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner"),39,39).." : More!!!")
					else
						Mob_Kill_Cake_Prince.Refresh("Boss Is Spawned!!!")
					end
				end)
			end
		end)
		
		page1:Toggle("Auto Cake PrinceV1 \n ตีโมจิv1",_G.Settings.Main.Auto_Cake_Prince,function(value)
			_G.Auto_Cake_Prince = value
			_G.Settings.Main.Auto_Cake_Prince = value
			StopTween(_G.Auto_Cake_Prince)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.Auto_Cake_Prince then
					pcall(function()
						if game.Workspace.Enemies:FindFirstChild("Cake Prince [Lv. 2300] [Raid Boss]") then   
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if v.Name == "Cake Prince [Lv. 2300] [Raid Boss]" then
									if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.Size = Vector3.new(60, 60, 60)  
											BringMobFarm = true
											if AttackRandomType == 1 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											elseif AttackRandomType == 2 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
											elseif AttackRandomType == 3 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
											else
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											end
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											sethiddenproperty(game.Players.LocalPlayer,"SimulationRadius",math.huge)
										until not _G.Auto_Cake_Prince or not v.Parent or v.Humanoid.Health <= 0
									end
								end
							end
						else if game.Workspace.Enemies:FindFirstChild("Cake Prince [Lv. 2300] [Raid Boss]") then
								toposition(game.Workspace.Enemies:FindFirstChild("Cake Prince [Lv. 2300] [Raid Boss]").HumanoidRootPart.CFrame * CFrame.new(0,_G.Settings.Main.Distance,5))
							else
								if game.Workspace.Enemies:FindFirstChild("Baking Staff [Lv. 2250]") or game.Workspace.Enemies:FindFirstChild("Head Baker [Lv. 2275]") or game.Workspace.Enemies:FindFirstChild("Cake Guard [Lv. 2225]") or game.Workspace.Enemies:FindFirstChild("Cookie Crafter [Lv. 2200]")  then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do  
										if (v.Name == "Baking Staff [Lv. 2250]" or v.Name == "Head Baker [Lv. 2275]" or v.Name == "Cake Guard [Lv. 2225]" or v.Name == "Cookie Crafter [Lv. 2200]") and v.Humanoid.Health > 0 then
											repeat wait()
												PosMon = v.HumanoidRootPart.CFrame
												EquipWeapon(_G.Select_Weapon)
												v.HumanoidRootPart.Size = Vector3.new(60, 60, 60)  
												BringMobFarm = true
												if AttackRandomType == 1 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												elseif AttackRandomType == 2 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
												elseif AttackRandomType == 3 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
												else
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												end
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until _G.Auto_Cake_Prince == false or game:GetService("ReplicatedStorage"):FindFirstChild("Cake Prince [Lv. 2300] [Raid Boss]") or not v.Parent or v.Humanoid.Health <= 0
										end
									end
								else
									BringMobFarm = false
									local CFrameMon = CFrame.new(-1820.0634765625, 210.74781799316406, -12297.49609375)
									repeat wait() toposition(CFrameMon) until (CFrameMon.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.Auto_Cake_Prince
								end
							end
						end
					end)
				end
			end
		end)
		spawn(function()
			while wait() do
				if _G.Auto_Cake_Prince then
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner",true)                    
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner")
				end
			end
		end)
		
		page1:Toggle("Auto Cake PrinceV2 \n ตีโมจิv2",_G.Settings.Main.AutoCakePrinceV2,function(value)
			_G.AutoCakePrinceV2 = value
			_G.Settings.Main.AutoCakePrinceV2 = value
			StopTween(_G.AutoCakePrinceV2)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoCakePrinceV2 then
					pcall(function()
						if game.Players.LocalPlayer.Backpack:FindFirstChild("God's Chalice") or game.Players.LocalPlayer.Character:FindFirstChild("God's Chalice") then
							if string.find(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("SweetChaliceNpc"),"Where") then
								warn("Not Have Enough Material")
							else
								 game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("SweetChaliceNpc")
							end
						elseif game.Players.LocalPlayer.Backpack:FindFirstChild("Sweet Chalice") or game.Players.LocalPlayer.Character:FindFirstChild("Sweet Chalice") then
							if string.find(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner"),"Do you want to open the portal now?") then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("CakePrinceSpawner")
							else
							  local MojoMs = {"Baking Staff [Lv. 2250]","Head Baker [Lv. 2275]","Cake Guard [Lv. 2225]","Cookie Crafter [Lv. 2200]"}
								if game.Workspace.Enemies:FindFirstChild("Baking Staff [Lv. 2250]") or game.Workspace.Enemies:FindFirstChild("Head Baker [Lv. 2275]") or game.Workspace.Enemies:FindFirstChild("Cake Guard [Lv. 2225]") or game.Workspace.Enemies:FindFirstChild("Cookie Crafter [Lv. 2200]") then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do 
									  for x,y in pairs(MojoMs) do
										if (v.Name == y or v.Name == "Baking Staff [Lv. 2250]" or v.Name == "Head Baker [Lv. 2275]" or v.Name == "Cake Guard [Lv. 2225]" or v.Name == "Cookie Crafter [Lv. 2200]") and v.Humanoid.Health > 0 then
											repeat wait()
												EquipWeapon(_G.Select_Weapon)
												BringMobFarm = true
												v.HumanoidRootPart.Size = Vector3.new(60, 60, 60)  
												PosMon = v.HumanoidRootPart.CFrame
												if AttackRandomType == 1 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												elseif AttackRandomType == 2 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
												elseif AttackRandomType == 3 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
												else
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												end
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until _G.AutoCakePrinceV2 == false or game:GetService("ReplicatedStorage"):FindFirstChild("Cake Prince [Lv. 2300] [Raid Boss]") or not v.Parent or v.Humanoid.Health <= 0
										end
									end
								  end
								else
									BringMobFarm = false
									toposition(CFrame.new(-1916.44983, 178.615494, -12701.5049, -0.842750371, -2.00153871e-09, -0.538304567, -3.13815285e-08, 1, 4.54115714e-08, 0.538304567, 5.516344e-08, -0.842750371))
								end
							end						
						elseif game.ReplicatedStorage:FindFirstChild("Dough King [Lv. 2300] [Raid Boss]") or game:GetService("Workspace").Enemies:FindFirstChild("Dough King [Lv. 2300] [Raid Boss]") then
							if game:GetService("Workspace").Enemies:FindFirstChild("Dough King [Lv. 2300] [Raid Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do 
									if v.Name == "Dough King [Lv. 2300] [Raid Boss]" then
										repeat wait()
											EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.Size = Vector3.new(60, 60, 60)
											v.HumanoidRootPart.CanCollide = false
											if AttackRandomType == 1 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											elseif AttackRandomType == 2 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
											elseif AttackRandomType == 3 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
											else
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											end
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
										until _G.AutoCakePrinceV2 == false or not v.Parent or v.Humanoid.Health <= 0
									end    
								end    
							else
								toposition(CFrame.new(-2009.2802734375, 4532.97216796875, -14937.3076171875)) 
							end
						elseif game.Players.LocalPlayer.Backpack:FindFirstChild("Red Key") or game.Players.LocalPlayer.Character:FindFirstChild("Red Key") then
							local args = {
								[1] = "CakeScientist",
								[2] = "Check"
							}
		
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
						else
							if game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == true then
								if string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Diablo") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Deandre") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Urban") then
									if game:GetService("Workspace").Enemies:FindFirstChild("Diablo [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Deandre [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Urban [Lv. 1750]") then
										for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
											if v.Name == "Diablo [Lv. 1750]" or v.Name == "Deandre [Lv. 1750]" or v.Name == "Urban [Lv. 1750]" then
												if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
													repeat wait()
														EquipWeapon(_G.Select_Weapon)
														v.HumanoidRootPart.CanCollide = false
														v.Humanoid.WalkSpeed = 0
														v.HumanoidRootPart.Size = Vector3.new(50,50,50)
														if AttackRandomType == 1 then
															toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
														elseif AttackRandomType == 2 then
															toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
														elseif AttackRandomType == 3 then
															toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
														else
															toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
														end
														if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
															game:GetService("VirtualUser"):CaptureController()
															game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
														end
														sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
													until _G.AutoCakePrinceV2 == false or v.Humanoid.Health <= 0 or not v.Parent or game.Players.LocalPlayer.Backpack:FindFirstChild("God's Chalice") or game.Players.LocalPlayer.Character:FindFirstChild("God's Chalice")
												end
											end
										end
									else
										if game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]") then
											toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,_G.Select_Distance,0))
										elseif game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]") then
											toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,_G.Select_Distance,0))
										elseif game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]") then
											toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,_G.Select_Distance,0))
										end
									end                    
								end
							else
								wait(0.5)
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EliteHunter")
							end
						end
					end)
				end
			end
		end)
		page1:Label('Elite Hunter or AutoYama')
		
		page1:Toggle("Auto Elite Hunter \n ตีบอสอิลท์",_G.Settings.Main.AutoEliteHunter,function(value)
			_G.AutoEliteHunter = value
			_G.Settings.Main.AutoEliteHunter = value
			StopTween(_G.AutoEliteHunter)
			SaveSettings()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EliteHunter")
		end)
		
		page1:Toggle("Auto Elite Hunter Hop \n ตีบอสอิลท์ออก",_G.Settings.Main.AutoEliteHunterHop,function(value)
			_G.AutoEliteHunterHop = value
			_G.Settings.Main.AutoEliteHunterHop = value
			StopTween(_G.AutoEliteHunterHop)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoEliteHunter and W3 then
					pcall(function()
						if game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == true then
							if string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Diablo") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Deandre") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Urban") then
								if game:GetService("Workspace").Enemies:FindFirstChild("Diablo [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Deandre [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Urban [Lv. 1750]") then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == "Diablo [Lv. 1750]" or v.Name == "Deandre [Lv. 1750]" or v.Name == "Urban [Lv. 1750]" then
											if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
												repeat wait()
													EquipWeapon(_G.Select_Weapon)
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid.WalkSpeed = 0
													v.HumanoidRootPart.Size = Vector3.new(50,50,50)
													if AttackRandomType == 1 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
													elseif AttackRandomType == 2 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
													elseif AttackRandomType == 3 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
													else
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
													end
													if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
														game:GetService("VirtualUser"):CaptureController()
														game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
													end
													sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
												until _G.AutoEliteHunter == false or v.Humanoid.Health <= 0 or not v.Parent
											end
										end
									end
								else
									if game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]") then
										toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
									elseif game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]") then
										toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
									elseif game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]") then
										toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
									end
								end                    
							end
						else
							if game.Players.LocalPlayer.Backpack:FindFirstChild("God's Chalice") or game.Players.LocalPlayer.Character:FindFirstChild("God's Chalice") then
								wait(.1)
							else
								if _G.AutoEliteHunterHop and game:GetService("ReplicatedStorage").Remotes["CommF_"]:InvokeServer("EliteHunter") == "I don't have anything for you right now. Come back later." then
									Hop()
								  else
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EliteHunter")
								end
							end
						end
					end)
				end
			end
		end)
		
		page1:Toggle("Auto Yama \n ออโต้ดึงดาบ",_G.Settings.Main.Auto_Yama,function(value)
			_G.Auto_Yama = value
			_G.Settings.Main.Auto_Yama = value
			StopTween(_G.Auto_Yama)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoYama then
					if game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EliteHunter","Progress") >= 30 then
						repeat wait(.1)
							fireclickdetector(game:GetService("Workspace").Map.Waterfall.SealedKatana.Handle.ClickDetector)
						until game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Yama") or not _G.AutoYama
					end
				end
			end
		end)
		
		end
		
		------------------------ \\ Settings // ------------------------
		
		local page2 = tab1:CraftPage('Settings',2)
		
		page2:Label('Settings')
		
		_G.Settings.Main.Select_Weapon = "Melee"
		page2:Dropdown("Select Weapon \n เลือกอาวุธ",{"Melee","Sword","Gun","Fruit"},"Melee",function(a)
			_G.Settings.Main.Select_Weapon = a
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					if _G.Settings.Main.Select_Weapon == "Melee" then
						for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
							if v.ToolTip == "Melee" then
								if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
									_G.Select_Weapon = v.Name
								end
							end
						end
					elseif _G.Settings.Main.Select_Weapon == "Sword" then
						for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
							if v.ToolTip == "Sword" then
								if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
									_G.Select_Weapon = v.Name
								end
							end
						end
					 elseif _G.Settings.Main.Select_Weapon == "Gun" then
						for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
							if v.ToolTip == "Gun" then
								if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
									_G.Select_Weapon = v.Name
								end
							end
						end
					elseif _G.Settings.Main.Select_Weapon == "Fruit" then
						for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
							if v.ToolTip == "Blox Fruit" then
								if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
									_G.Select_Weapon = v.Name
								end
							end
						end
					else
						for i ,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
							if v.ToolTip == "Melee" then
								if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then
									_G.Select_Weapon = v.Name
								end
							end
						end
					end
				end)
			end
		end)
		
		_G.FastAttack = true
		page2:Toggle('Fast Attack \n ตีเร็วปกติ',_G.Settings.Main.FastAttack,function(a)
			_G.FastAttack = a
			_G.Settings.Main.FastAttack = a
			SaveSettings()
		end)
	
		page2:Toggle('Fast Attack Super \n ตีเร็วSuper',false,function(a)
			_G.FastAttackSuper = a
		end)
	
		local plr = game.Players.LocalPlayer
		local CbFw = debug.getupvalues(require(plr.PlayerScripts.CombatFramework))
		local CbFw2 = CbFw[2]
	
		function GetCurrentBlade() 
			local p13 = CbFw2.activeController
			local ret = p13.blades[1]
			if not ret then return end
			while ret.Parent~=game.Players.LocalPlayer.Character do ret=ret.Parent end
			return ret
		end
		function AttackNoCD() 
			local AC = CbFw2.activeController
			for i = 1, 1 do 
				local bladehit = require(game.ReplicatedStorage.CombatFramework.RigLib).getBladeHits(
					plr.Character,
					{plr.Character.HumanoidRootPart},
					60
				)
				local cac = {}
				local hash = {}
				for k, v in pairs(bladehit) do
					if v.Parent:FindFirstChild("HumanoidRootPart") and not hash[v.Parent] then
						table.insert(cac, v.Parent.HumanoidRootPart)
						hash[v.Parent] = true
					end
				end
				bladehit = cac
				if #bladehit > 0 then
					local u8 = debug.getupvalue(AC.attack, 5)
					local u9 = debug.getupvalue(AC.attack, 6)
					local u7 = debug.getupvalue(AC.attack, 4)
					local u10 = debug.getupvalue(AC.attack, 7)
					local u12 = (u8 * 798405 + u7 * 727595) % u9
					local u13 = u7 * 798405
					(function()
						u12 = (u12 * u9 + u13) % 1099511627776
						u8 = math.floor(u12 / u9)
						u7 = u12 - u8 * u9
					end)()
					u10 = u10 + 1
					debug.setupvalue(AC.attack, 5, u8)
					debug.setupvalue(AC.attack, 6, u9)
					debug.setupvalue(AC.attack, 4, u7)
					debug.setupvalue(AC.attack, 7, u10)
					pcall(function()
						for k, v in pairs(AC.animator.anims.basic) do
							v:Play()
						end                  
					end)
					if plr.Character:FindFirstChildOfClass("Tool") and AC.blades and AC.blades[1] then 
						game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("weaponChange",tostring(GetCurrentBlade()))
						game.ReplicatedStorage.Remotes.Validator:FireServer(math.floor(u12 / 1099511627776 * 16777215), u10)
						game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("hit", bladehit, i, "") 
					end
					wait(0.1)
				end
			end
		end
		spawn(function()
			while wait() do 
				if _G.FastAttackSuper then wait(0.05)
					AttackNoCD()
				end
			end
		end)
	
		_G.FastAttack = true
		page2:Toggle('Fast Tween \n เปิดแล้วจะ ฟาร์มไวมากกก[Ban90%]',_G.Settings.Main.FastTween,function(a)
			_G.FastTween = a
			_G.Settings.Main.FastTween = a
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
				pcall(function()
					if _G.FastTween then
						_G.Settings.Main.Tween = 400
					end
				end)
			end
		end)
	
		spawn(function()
			while _G.FastAttack do task.wait()
			
			require(game.ReplicatedStorage.Util.CameraShaker):Stop()
			xShadowFastAttackx = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework)
			xShadowx = debug.getupvalues(xShadowFastAttackx)[2]
			task.spawn(function()
				 while true do task.wait()
					if _G.FastAttack then
						if typeof(xShadowx) == "table" then
							pcall(function()
								xShadowx.activeController.timeToNextAttack = -(math.huge^math.huge^math.huge)
								xShadowx.activeController.timeToNextAttack = 0
								xShadowx.activeController.hitboxMagnitude = 200
								xShadowx.activeController.active = false
								xShadowx.activeController.timeToNextBlock = 0
								xShadowx.activeController.focusStart = 0
								xShadowx.activeController.increment = 4
								xShadowx.activeController.blocking = false
								xShadowx.activeController.attacking = false
								xShadowx.activeController.humanoid.AutoRotate = true
							end)
						end
					end
				end
			end)
			
			local Module = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework)
			local CombatFramework = debug.getupvalues(Module)[2]
			local CameraShakerR = require(game.ReplicatedStorage.Util.CameraShaker)
			
			task.spawn(function()
				while true do task.wait()
					if _G.FastAttack then
						pcall(function()
							CameraShakerR:Stop()
							CombatFramework.activeController.attacking = false
							CombatFramework.activeController.timeToNextAttack = 0
							CombatFramework.activeController.increment = 4
							CombatFramework.activeController.hitboxMagnitude = 100
							CombatFramework.activeController.blocking = false
							CombatFramework.activeController.timeToNextBlock = 0
							CombatFramework.activeController.focusStart = 0
							CombatFramework.activeController.humanoid.AutoRotate = true
						end)
					end
				end
			end)
			
			local SeraphFrame = debug.getupvalues(require(game:GetService("Players").LocalPlayer.PlayerScripts:WaitForChild("CombatFramework")))[2]
			local VirtualUser = game:GetService('VirtualUser')
			local RigControllerR = debug.getupvalues(require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework.RigController))[2]
			local Client = game:GetService("Players").LocalPlayer
			local DMG = require(Client.PlayerScripts.CombatFramework.Particle.Damage)
			
			function SeraphFuckWeapon() 
				local p13 = SeraphFrame.activeController
				local wea = p13.blades[1]
				if not wea then return end
				while wea.Parent~=game.Players.LocalPlayer.Character do wea=wea.Parent end
				return wea
			end
			
			function getHits(Size)
				local Hits = {}
				local Enemies = workspace.Enemies:GetChildren()
				local Characters = workspace.Characters:GetChildren()
				for i=1,#Enemies do local v = Enemies[i]
					local Human = v:FindFirstChildOfClass("Humanoid")
					if Human and Human.RootPart and Human.Health > 0 and game.Players.LocalPlayer:DistanceFromCharacter(Human.RootPart.Position) < Size+55 then
						table.insert(Hits,Human.RootPart)
					end
				end
				for i=1,#Characters do local v = Characters[i]
					if v ~= game.Players.LocalPlayer.Character then
						local Human = v:FindFirstChildOfClass("Humanoid")
						if Human and Human.RootPart and Human.Health > 0 and game.Players.LocalPlayer:DistanceFromCharacter(Human.RootPart.Position) < Size+55 then
							table.insert(Hits,Human.RootPart)
						end
					end
				end
				return Hits
			end
			
			task.spawn(
				function()
				while true do task.wait()
					if  _G.FastAttack then
						if SeraphFrame.activeController then
							if v.Humanoid.Health > 0 then
								SeraphFrame.activeController.timeToNextAttack = -(math.huge^math.huge^math.huge)
								SeraphFrame.activeController.timeToNextAttack = 0
								SeraphFrame.activeController.focusStart = 0
								SeraphFrame.activeController.hitboxMagnitude = 110
								SeraphFrame.activeController.humanoid.AutoRotate = true
								SeraphFrame.activeController.increment = 4
							 end
						end
					end
				end
			end)
			
			function Boost()
				spawn(function()
					if SeraphFrame.activeController then
						game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("weaponChange",tostring(SeraphFuckWeapon()))
					end
				end)
			end
			
			function Unboost()
				spawn(function()
					--print("Unboost ของจริง555")
					--game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("unequipWeapon",tostring(SeraphFuckWeapon()))
				end)
			end
			
			local cdnormal = 0
			local Animation = Instance.new("Animation")
			local CooldownFastAttack = 0.000000
			Attack = function()
				local ac = SeraphFrame.activeController
				if ac and ac.equipped then
					task.spawn(
						function()
						if tick() - cdnormal > 0 then
							ac:attack()
							cdnormal = tick()
						else
							Animation.AnimationId = ac.anims.basic[2]
							ac.humanoid:LoadAnimation(Animation):Play(0.01, 0.01) --ท่าไม่ทำงานแก้เป็น (1,1)
							game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("hit", getHits(60), 1, "")
						end
						wait(0.2)
					end)
				end
			end
			
			b = tick()
			spawn(function()
				while _G.FastAttack do task.wait()
					if _G.FastAttack then
						if b - tick() > 9e9 then
							b = tick()
						end
						pcall(function()
							for i, v in pairs(game.Workspace.Enemies:GetChildren()) do
								if v.Humanoid.Health > 0 then
									if (v.HumanoidRootPart.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 90 then
										Attack()
										wait()
										Boost()
									end
								end
							end
						end)
						wait(0.05)
					end
				end
			end)
			
			k = tick()
			spawn(function()
				while wait() do
					if  _G.FastAttack then
						if k - tick() > 9e9 then
							k = tick()
						end
						pcall(function()
							for i, v in pairs(game.Workspace.Enemies:GetChildren()) do
								if v.Humanoid.Health > 0 then
									if (v.HumanoidRootPart.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 90 then
										Unboost()
									end
								end
							end
						end)
					end
				end
			end)
			
			tjw1 = true
			task.spawn(
				function()
					local a = game.Players.LocalPlayer
					local b = require(a.PlayerScripts.CombatFramework.Particle)
					local c = require(game:GetService("ReplicatedStorage").CombatFramework.RigLib)
					if not shared.orl then
						shared.orl = c.wrapAttackAnimationAsync
					end
					if not shared.cpc then
						shared.cpc = b.play
					end
					if tjw1 then
						pcall(
							function()
								c.wrapAttackAnimationAsync = function(d, e, f, g, h)
									local i = c.getBladeHits(e, f, g)
									if i then
										b.play = function()
										end
										d:Play(0.01,0.01,0.01)
										h(i)
										b.play = shared.cpc
										wait(0.1)
										d:Stop()
									end
								end
							end
						)
					end
				end
			)
			
			local CameRa = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework.CameraShaker)
			CameRa.CameraShakeInstance.CameraShakeState = {FadingIn = 3,FadingOut = 2,Sustained = 0,Inactive =1}
			
			local Client = game.Players.LocalPlayer
			local STOP = require(Client.PlayerScripts.CombatFramework.Particle)
			local STOPRL = require(game:GetService("ReplicatedStorage").CombatFramework.RigLib)
			task.spawn(function()
				pcall(function()
					if not shared.orl then
						shared.orl = STOPRL.wrapAttackAnimationAsync
					end
						if not shared.cpc then
							shared.cpc = STOP.play 
						end
						spawn(function()
							require(game.ReplicatedStorage.Util.CameraShaker):Stop()
							game:GetService("RunService").Stepped:Connect(function()
								STOPRL.wrapAttackAnimationAsync = function(a,b,c,d,func)
									local Hits = STOPRL.getBladeHits(b,c,d)
									if Hits then
										if  _G.FastAttack then
											STOP.play = function() end
											a:Play(21,29,30)
											func(Hits)
											STOP.play = shared.cpc
											wait(a.length * 9e9)
											a:Stop()
										else
											func(Hits)
											STOP.play = shared.cpc
											wait(a.length * 9e9)
											a:Stop()
										end
									end
								end
							end)
						end)
					end)
				end)
			end
		end)
	
		page2:Toggle('Fast Attack Trigon or Fluxus PC \n ตีเร็วปกติ PC',_G.Settings.Main.FastAttackPC,function(a)
			_G.FastAttackPC = a
			_G.OpenFast = a
			_G.Settings.Main.FastAttackPC = a
			SaveSettings()
		end)
	
		page2:Toggle('Fast Attack Noob Mobile\n โจมตีรวดเร็วมือถือกาก',_G.Settings.Main.FastAttackO,function(a)
			_G.FastAttackO = a
			_G.Settings.Main.FastAttackO = a
			SaveSettings()
		end)
		
		
		local YaY = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework)
		local CameraShakerR = require(game.ReplicatedStorage.Util.CameraShaker)
		spawn(function()
			while task.wait() do
				if _G.FastAttackO then
					pcall(function()
						for i,CombatFrameworkR in pairs(debug.getupvalues(YaY)) do
							if i == 2 then
								CameraShakerR:Stop()
								CombatFrameworkR.activeController.attacking = false
								CombatFrameworkR.activeController.timeToNextAttack = 0
								CombatFrameworkR.activeController.increment = 4
								CombatFrameworkR.activeController.increment = 2
								CombatFrameworkR.activeController.hitboxMagnitude = 60
								CombatFrameworkR.activeController.blocking = false
								CombatFrameworkR.activeController.timeToNextBlock = 0
								CombatFrameworkR.activeController.focusStart = 0
								CombatFrameworkR.activeController.humanoid.AutoRotate = true
							end
						end
					end)
				else
					for i,CombatFrameworkR in pairs(debug.getupvalues(YaY)) do
						if i == 2 then
							pcall(function()
								CameraShakerR:Stop()
								CombatFrameworkR.activeController.attacking = false
								CombatFrameworkR.activeController.hitboxMagnitude = 60
								CombatFrameworkR.activeController.blocking = false
								CombatFrameworkR.activeController.focusStart = 0
								CombatFrameworkR.activeController.humanoid.AutoRotate = true
							end)
						end
					end
				end
			end
		end)
		spawn(function()
			game:GetService('RunService').Stepped:Connect(function()
				if _G.FastAttackO then
					for i, v in pairs(game.Workspace["_WorldOrigin"]:GetChildren()) do
						if v.Name == "Sounds" then--or v.Name == "SlashHit"
							v:Destroy() 
						end
					end
				end
			end)
		end)
	
		
		page2:Toggle('Bring Monster \n ดึงมอน',_G.Settings.Main.BringMonster,function(a)
			_G.BringMonster = a
			_G.Settings.Main.BringMonster = a
			SaveSettings()
		end)
		spawn(function()
			while wait() do
				pcall(function()
					if _G.BringMonster and _G.AutoFarm then
						for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
							if v.Name == QuestCheck()[3] and (v.HumanoidRootPart.Position - PosMon.Position).magnitude <= 500 then
								v.HumanoidRootPart.CFrame = PosMon
								v.Humanoid.JumpPower = 0
								v.Humanoid.WalkSpeed = 0
								v.HumanoidRootPart.Size = Vector3.new(60,60,60)
								v.HumanoidRootPart.Transparency = 1
								v.HumanoidRootPart.CanCollide = false
								v.Head.CanCollide = false
								if v.Humanoid:FindFirstChild("Animator") then
									v.Humanoid.Animator:Destroy()
								end
								v.Humanoid:ChangeState(11)
								v.Humanoid:ChangeState(14)
								sethiddenproperty(game.Players.LocalPlayer, "SimulationRadius",  math.huge)
							end
						end
					end
				end)
			end
		end)
		spawn(function()
			while true do wait()
				if setscriptable then
					setscriptable(game.Players.LocalPlayer, "SimulationRadius", true)
				end
				if sethiddenproperty then
					sethiddenproperty(game.Players.LocalPlayer, "SimulationRadius", math.huge)
				end
			end
		end)
		spawn(function()
			while task.wait() do
				pcall(function()
					if _G.BringMonster and BringMobFarm then
						for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
							if not string.find(v.Name,"Boss") and (v.HumanoidRootPart.Position - PosMon.Position).magnitude <= 500 then
								if InMyNetWork(v.HumanoidRootPart) then
									v.HumanoidRootPart.CFrame = PosMon
									v.Humanoid.JumpPower = 0
									v.Humanoid.WalkSpeed = 0
									v.HumanoidRootPart.Size = Vector3.new(60,60,60)
									v.HumanoidRootPart.Transparency = 1
									v.HumanoidRootPart.CanCollide = false
									v.Head.CanCollide = false
									if v.Humanoid:FindFirstChild("Animator") then
										v.Humanoid.Animator:Destroy()
									end
									v.Humanoid:ChangeState(11)
									v.Humanoid:ChangeState(14)
									sethiddenproperty(game.Players.LocalPlayer, "SimulationRadius",  math.huge)
								end
							end
						end
					end
				end)
			end
		end)			
		function InMyNetWork(object)
			if isnetworkowner then
				return isnetworkowner(object)
			else
				if (object.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 200 then 
					return true
				end
				return false
			end
		end
	
		page2:Toggle('Auto Haki \n เปิดฮาคิ',_G.Settings.Main.AutoHaki,function(a)
			AutoHaki = a
			_G.Settings.Main.AutoHaki = a
			SaveSettings()
		end)
		spawn(function()
			while wait() do
				if AutoHaki then
					if not game.Players.LocalPlayer.Character:FindFirstChild("HasBuso") then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Buso")
					end
				end
			end
		end)
		
		local player = game:GetService("Players").LocalPlayer
		
		function blackscreen(enable)
			local playerGui = player:WaitForChild("PlayerGui")
			if not enable then
				local sUi = playerGui:FindFirstChild("Blackscreen")
				if sUi then sUi:Destroy() end
				return
			elseif playerGui:FindFirstChild("Blackscreen") then
				return
			end
			local sUi = Instance.new("ScreenGui", playerGui)
			sUi.Name = "Blackscreen"
			sUi.DisplayOrder = -727
		
			local uiFrame = Instance.new("Frame", sUi)
			uiFrame.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
			uiFrame.Size = UDim2.new(0, 72727, 0, 72727)
			uiFrame.Position = UDim2.new(0, 0, -5, 0)
		end
		
		page2:Toggle('Black Screen \n จอดำ',_G.Settings.Main.BlackScreen,function(a)
			_G.StartBlackScreen = a
			_G.Settings.Main.BlackScreen= a
			if _G.StartBlackScreen then
				blackscreen(true)
				game:GetService("RunService"):Set3dRenderingEnabled(false)
			elseif _G.StartBlackScreen == false then
				blackscreen(false)
				game:GetService("RunService"):Set3dRenderingEnabled(true)
			end
			SaveSettings()
		end)
		
		page2:Toggle('White Screen \n จอขาว',_G.Settings.Main.WhiteScreen,function(a)
			_G.StartWhiteScreen = a
			_G.Settings.Main.WhiteScreen= a
			if _G.StartWhiteScreen then
				game:GetService("RunService"):Set3dRenderingEnabled(false)
			elseif _G.StartWhiteScreen == false then
				game:GetService("RunService"):Set3dRenderingEnabled(true)
			end
			SaveSettings()
		end)
		
		_G.Rejoin = true
		page2:Toggle('Auto Rejoin \n โดนเตะจะเข้าให้ใหม่',_G.Rejoin,function(a)
			_G.Rejoin = a
		end)
		
		spawn(function()
		  while wait() do
			  if _G.Rejoin then
					  getgenv().rejoin = game:GetService("CoreGui").RobloxPromptGui.promptOverlay.ChildAdded:Connect(function(child)
							if child.Name == 'ErrorPrompt' and child:FindFirstChild('MessageArea') and child.MessageArea:FindFirstChild("ErrorFrame") then
								game:GetService("TeleportService"):Teleport(game.PlaceId)
							end
						end)
				  end
			  end
		end)
	
		page2:Toggle('Notifications Remove \n ลบหน้าNotifications',true,function(a)
			if a == true then
				game:GetService("Players").LocalPlayer.PlayerGui.Notifications.Enabled = false
			else
				game:GetService("Players").LocalPlayer.PlayerGui.Notifications.Enabled = true
			end
		end)
	
		page2:Toggle('Remove UI DamageCounter \n ลบ Ui DamageCounter',false,function(a)
			if a == true then
				game:GetService("ReplicatedStorage").Assets.GUI.DamageCounter.Enabled = false
			else
				game:GetService("ReplicatedStorage").Assets.GUI.DamageCounter.Enabled = true
			end
		end)
	
		
		page2:Toggle('Bypass \n วาป+รีตัว',_G.Rejoin,function(a)
			_G.Bypass = a
		end)
		
		page2:Slider("Tween Speed \n ความเร็วในการบิน",true,0,400,_G.Settings.Main.Tween,function(value)
			_G.Settings.Main.Tween = value
			print(value)
			SaveSettings()
		end)
		
		page2:Slider("Mon Aura Point \n พ้อยความใกล",true,0,5000,1000,function(value)
			_G.Settings.Main.Mon_Aura_Point = value
			print(value)
			SaveSettings()
		end)
		
		local page3 = tab1:CraftPage('Mastery Settings',2)
		
		page3:Toggle('Skill Z \n สกิว แซด',_G.Settings.Main.SkillZ,function(a)
			_G.SkillZ = a
			_G.Settings.Main.SkillZ = a
			SaveSettings()
		end)
		
		page3:Toggle('Skill X \n สกิว เอ็ก',_G.Settings.Main.SkillX,function(a)
			_G.SkillX = a
			_G.Settings.Main.SkillX = a
			SaveSettings()
		end)
		
		page3:Toggle('Skill C \n สกิว ซี',_G.Settings.Main.SkillC,function(a)
			_G.SkillC = a
			_G.Settings.Main.SkillC = a
			SaveSettings()
		end)
		
		page3:Toggle('Skill V \n สกิว วี',_G.Settings.Main.SkillV,function(a)
			_G.SkillV = a
			_G.Settings.Main.SkillV = a
			SaveSettings()
		end)
		
		spawn(function()
			while task.wait() do
				pcall(function()
					if UseSkill then
						if _G.SkillZ then
							game:GetService("VirtualInputManager"):SendKeyEvent(true,"Z",false,game)
							game:GetService("VirtualInputManager"):SendKeyEvent(false,"Z",false,game)
						end
						if _G.SkillX then
							game:GetService("VirtualInputManager"):SendKeyEvent(true,"X",false,game)
							game:GetService("VirtualInputManager"):SendKeyEvent(false,"X",false,game)
						end
						if _G.SkillC then
							game:GetService("VirtualInputManager"):SendKeyEvent(true,"C",false,game)
							game:GetService("VirtualInputManager"):SendKeyEvent(false,"C",false,game)
						end
						if _G.SkillV then
							game:GetService("VirtualInputManager"):SendKeyEvent(true,"V",false,game)
							game:GetService("VirtualInputManager"):SendKeyEvent(false,"V",false,game)
						end
					elseif UseSkillGun then
						if _G.SkillZ then
							game:GetService("VirtualInputManager"):SendKeyEvent(true,"Z",false,game)
							game:GetService("VirtualInputManager"):SendKeyEvent(false,"Z",false,game)
						end
						if _G.SkillX then
							game:GetService("VirtualInputManager"):SendKeyEvent(true,"X",false,game)
							game:GetService("VirtualInputManager"):SendKeyEvent(false,"X",false,game)
						end
					end
				end)
			end
		end)
		spawn(function()
			while task.wait() do
				pcall(function()
					if _G.AutoFruitMastery then
						local On = {
							[1] = FruitPos.Position
						}
						game:GetService("Players").LocalPlayer.Character[game:GetService("Players").LocalPlayer.Data.DevilFruit.Value].RemoteEvent:FireServer(unpack(On))
					else
					   local Off = {
							[1] = nil
						}
						game:GetService("Players").LocalPlayer.Character[game:GetService("Players").LocalPlayer.Data.DevilFruit.Value].RemoteEvent:FireServer(unpack(Off)) 
					end
				end)
			end
		end)
		
		page3:Slider("Distance",true,0,100,_G.Settings.Main.Distance,function(value)
			_G.Settings.Main.Distance = value
			print(value)
			SaveSettings()
		end)
		
		page3:Slider("HealthMs",true,0,100,_G.Settings.Main.HealthMs,function(value)
			_G.Settings.Main.HealthMs = value
			print(value)
			SaveSettings()
		end)
		
		local tab7 = Win:CraftTab('AutoItem',5516516)
		local page14 = tab7:CraftPage('AutoItem 1/2',1)
		
		page14:Label('Auto Fighting')
		
		page14:Toggle("Auto Fully Godhuman \n ออโต้ทำหมากกอด",_G.Settings.Main.AutoFullySuperhuman,function(value)
			_G.AutoFullyGodhuman = value
			_G.Settings.Main.AutoFullyGodhuman = value
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoFullyGodhuman then
					pcall(function()
					if tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyGodhuman",true)) then
						_G.Select_Weapon = "Godhuman"
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyGodhuman")
						else
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Death Step") and game.Players.LocalPlayer.Backpack:FindFirstChild("Death Step").Level.Value <= 399 and game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") and game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw").Level.Value <= 399 and  game.Players.LocalPlayer.Backpack:FindFirstChild("Sharkman Karate") and game.Players.LocalPlayer.Backpack:FindFirstChild("Sharkman Karate").Level.Value <= 399 and game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Talon") and game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Talon").Level.Value <= 399 then
							if not W3 then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelZou")
							elseif W3 then
							if CheckMaterial("Fish Tail") <= 20 and W3 then
								for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
								if v.Name == "Fishman Raider [Lv. 1775]" or v.Name == "Fishman Captain [Lv. 1800]" then
										if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat task.wait()
											--EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.CanCollide = false
											v.Humanoid.WalkSpeed = 0
												v.Head.CanCollide = false
												PosMon = v.HumanoidRootPart.CFrame
												BringMobFarm = true
												v.HumanoidRootPart.Size = Vector3.new(100,100,100)
												v.HumanoidRootPart.Transparency = 1
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until not _G.AutoFullyGodhuman or not v.Parent or v.Humanoid.Health <= 0
										end
										else
											BringMobFarm = false
											toposition(CFrame.new( -10993,332, -8940))
									end
								end
								elseif CheckMaterial("Dragon Scale") <= 10 and W3 then
								for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
								if v.Name == "Fishman Raider [Lv. 1775]" or v.Name == "Fishman Captain [Lv. 1800]" then
										if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat task.wait()
											--EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.CanCollide = false
											v.Humanoid.WalkSpeed = 0
												v.Head.CanCollide = false
												PosMon = v.HumanoidRootPart.CFrame
												BringMobFarm = true
												v.HumanoidRootPart.Size = Vector3.new(100,100,100)
												v.HumanoidRootPart.Transparency = 1
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until not _G.AutoFullyGodhuman or not v.Parent or v.Humanoid.Health <= 0
										end
										else
											BringMobFarm = false
											toposition(CFram.new(6594,383,139))
									end
									end
									if CheckMaterial("Dragon Scale") >= 10 and CheckMaterial("Fish Tail") >= 20 then
										game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelDressrosa")
									end
								end
								elseif not W2 then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelDressrosa")
								elseif W2 then
								if CheckMaterial("Magma Ore") <= 20 and W2 then
									for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
								if v.Name == "Magma Ninja [Lv. 1175]" then
										if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat task.wait()
											--EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.CanCollide = false
											v.Humanoid.WalkSpeed = 0
												v.Head.CanCollide = false
												PosMon = v.HumanoidRootPart.CFrame
												BringMobFarm = true
												v.HumanoidRootPart.Size = Vector3.new(100,100,100)
												v.HumanoidRootPart.Transparency = 1
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until not _G.AutoFullyGodhuman or not v.Parent or v.Humanoid.Health <= 0
										end
											else
												BringMobFarm = false
												toposition(CFrame.new( -5428,78, -5959))
										end
										end
								elseif CheckMaterial("Mystic Droplet") <= 10 and W2 then
									for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
								if v.Name == "Water Fighter [Lv. 1450]" then
										if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat task.wait()
											--EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.CanCollide = false
											v.Humanoid.WalkSpeed = 0
												v.Head.CanCollide = false
												PosMon = v.HumanoidRootPart.CFrame
												BringMobFarm = true
												v.HumanoidRootPart.Size = Vector3.new(100,100,100)
												v.HumanoidRootPart.Transparency = 1
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,35,5))
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until not _G.AutoFullyGodhuman or not v.Parent or v.Humanoid.Health <= 0
										end
											else
												BringMobFarm = false
												toposition(CFrame.new( -3385,239, -10542))
											end
										end
										if not W3 then
											game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelZou")
										elseif W3 then
											if CheckMaterial("Mystic Droplet") >= 10 and CheckMaterial("Magma Ore") >= 20 and CheckMaterial("Dragon Scale") >= 10 and CheckMaterial("Fish Tail") >= 20 then
											game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyGodhuman")
											wait(.3)
											_G.AutoFarmLv = true
											print("Succeed")
											if tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyGodhuman",true)) then
											game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyGodhuman")
											wait(.3)
											_G.AutoFarmLv = true
											print("Succeed")
											end
										end
									end
								end
							end
						end
						end
					end)
				end
			end
		end)
		
		page14:Toggle("Auto Fully Superhuman \n ออโต้ทำหมากซุป",_G.Settings.Main.AutoFullySuperhuman,function(value)
			_G.AutoFullySuperhuman = value
			_G.Settings.Main.AutoFullySuperhuman = value
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while task.wait() do 
					if _G.Settings.Main.AutoFullySuperhuman then
						if game.Players.LocalPlayer.Backpack:FindFirstChild("Combat") or game.Players.LocalPlayer.Character:FindFirstChild("Combat") and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 150000 then
							UnEquipWeapon("Combat")
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyBlackLeg")
						end   
						if game.Players.LocalPlayer.Character:FindFirstChild("Superhuman") or game.Players.LocalPlayer.Backpack:FindFirstChild("Superhuman") then
							_G.Select_Weapon = "Superhuman"
						end  
						if game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg") or game.Players.LocalPlayer.Character:FindFirstChild("Black Leg") or game.Players.LocalPlayer.Backpack:FindFirstChild("Electro") or game.Players.LocalPlayer.Character:FindFirstChild("Electro") or game.Players.LocalPlayer.Backpack:FindFirstChild("Fishman Karate") or game.Players.LocalPlayer.Character:FindFirstChild("Fishman Karate") or game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Claw") or game.Players.LocalPlayer.Character:FindFirstChild("Dragon Claw") then
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg") and game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg").Level.Value <= 299 then
								_G.Select_Weapon = "Black Leg"
							end
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Electro") and game.Players.LocalPlayer.Backpack:FindFirstChild("Electro").Level.Value <= 299 then
								_G.Select_Weapon = "Electro"
							end
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Fishman Karate") and game.Players.LocalPlayer.Backpack:FindFirstChild("Fishman Karate").Level.Value <= 299 then
								_G.Select_Weapon = "Fishman Karate"
							end
							if game.Players.LocalPlayer.BackpacUnEquipWeaponk:FindFirstChild("Dragon Claw") and game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Claw").Level.Value <= 299 then
								_G.Select_Weapon = "Dragon Claw"
							end
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg") and game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg").Level.Value >= 300 and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 300000 then
								UnEquipWeapon("Black Leg")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectro")
							end
							if game.Players.LocalPlayer.Character:FindFirstChild("Black Leg") and game.Players.LocalPlayer.Character:FindFirstChild("Black Leg").Level.Value >= 300 and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 300000 then
								UnEquipWeapon("Black Leg")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectro")
							end
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Electro") and game.Players.LocalPlayer.Backpack:FindFirstChild("Electro").Level.Value >= 300 and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 750000 then
								UnEquipWeapon("Electro")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyFishmanKarate")
							end
							if game.Players.LocalPlayer.Character:FindFirstChild("Electro") and game.Players.LocalPlayer.Character:FindFirstChild("Electro").Level.Value >= 300 and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 750000 then
								UnEquipWeapon("Electro")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyFishmanKarate")
							end
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Fishman Karate") and game.Players.LocalPlayer.Backpack:FindFirstChild("Fishman Karate").Level.Value >= 300 and game:GetService("Players")["Localplayer"].Data.Fragments.Value >= 1500 then
								UnEquipWeapon("Fishman Karate")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","1")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","2") 
							end
							if game.Players.LocalPlayer.Character:FindFirstChild("Fishman Karate") and game.Players.LocalPlayer.Character:FindFirstChild("Fishman Karate").Level.Value >= 300 and game:GetService("Players")["Localplayer"].Data.Fragments.Value >= 1500 then
								UnEquipWeapon("Fishman Karate")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","1")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","2")
							else
								local Fragment = game:GetService("Players")["Localplayer"].Data.Fragments.Value
								if Fragment <=1499 then
									_G.KillAura = false
									_G.Next_Islands = false
								else
									if W2 and game:GetService("Players")["LocalPlayer"].PlayerGui.Main.Timer.Visible == false then
										fireclickdetector(game:GetService("Workspace").Map.CircleIsland.RaidSummon2.Button.Main.ClickDetector)
										wait(15)
									elseif W3 and game:GetService("Players")["LocalPlayer"].PlayerGui.Main.Timer.Visible == false then
										fireclickdetector(game:GetService("Workspace").Map["Boat Castle"].RaidSummon2.Button.Main.ClickDetector)
										wait(15)
									end
									_G.KillAura = true
									_G.Next_Islands = true
									BuyChips()
								end
							end
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Claw") and game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Claw").Level.Value >= 300 and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 3000000 then
								UnEquipWeapon("Dragon Claw")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuySuperhuman")
							end
							if game.Players.LocalPlayer.Character:FindFirstChild("Dragon Claw") and game.Players.LocalPlayer.Character:FindFirstChild("Dragon Claw").Level.Value >= 300 and game:GetService("Players")["LocalPlayer"].Data.Beli.Value >= 3000000 then
								UnEquipWeapon("Dragon Claw")
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuySuperhuman")
							end 
						end
					end
				end
			end)
		end)
		
		page14:Toggle("AutoDeathStep \n ออโต้ขาดำ",_G.Settings.Main.AutoDeathStep,function(value)
			_G.AutoDeathStep = value
			_G.Settings.Main.AutoDeathStep = value
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do wait()
				if _G.AutoDeathStep then 
					if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Black Leg") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Black Leg") or game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Death Step") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Death Step") then
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Black Leg") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Black Leg").Level.Value >= 450 then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyDeathStep")
							_G.Select_Weapon = "Death Step"
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Death Step") then 
								local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Death Step") 
								game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
							end
						end  
						if game:GetService("Players").LocalPlayer.Character:FindFirstChild("Black Leg") and game:GetService("Players").LocalPlayer.Character:FindFirstChild("Black Leg").Level.Value >= 450 then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyDeathStep")
							_G.Select_Weapon = "Death Step"
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Death Step") then 
								local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Death Step") 
								game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
							end
						end  
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Black Leg") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Black Leg").Level.Value <= 449 then
							_G.Select_Weapon = "Black Leg"
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg") then 
								local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Black Leg") 
								game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
							end
						end 
					else 
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyBlackLeg")
					end
				end
			end
		end)
		
		page14:Toggle("Auto Sharkman \n ออโต้หมัดฉลาม",_G.Settings.Main.AutoSharkman,function(value)
			_G.AutoSharkman = value
			_G.Settings.Main.AutoSharkman = value
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while wait() do
					if _G.AutoSharkman then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyFishmanKarate")
						if string.find(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuySharkmanKarate"), "keys") then  
							if game:GetService("Players").LocalPlayer.Character:FindFirstChild("Water Key") or game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Water Key") then
								toposition(CFrame.new(-2604.6958, 239.432526, -10315.1982, 0.0425701365, 0, -0.999093413, 0, 1, 0, 0.999093413, 0, 0.0425701365))
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuySharkmanKarate")
							elseif game:GetService("Players").LocalPlayer.Character:FindFirstChild("Fishman Karate") and game:GetService("Players").LocalPlayer.Character:FindFirstChild("Fishman Karate").Level.Value >= 400 then
							else 
								Ms = "Tide Keeper [Lv. 1475] [Boss]"
								if game:GetService("Workspace").Enemies:FindFirstChild(Ms) then   
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == Ms then 
											repeat task.wait()
												EquipWeapon(_G.Select_Weapon)
												v.Head.CanCollide = false
												v.Humanoid.WalkSpeed = 0
												v.HumanoidRootPart.CanCollide = false
												v.HumanoidRootPart.CFrame = OldCFrameShark
												toposition(v.HumanoidRootPart.CFrame*CFrame.new(0,35,0))
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
												sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
											until not v.Parent or v.Humanoid.Health <= 0 or _G.AutoSharkman == false or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Water Key") or game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Water Key")
										end
									end
								else
									toposition(CFrame.new(-3570.18652, 123.328949, -11555.9072, 0.465199202, -1.3857326e-08, 0.885206044, 4.0332897e-09, 1, 1.35347511e-08, -0.885206044, -2.72606271e-09, 0.465199202))
									wait(3)
								end
							end
						else 
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuySharkmanKarate")
						end
					end
				end
			end)
		end)
		
		page14:Toggle("Auto ElectricClaw \n หมัดไฟฟ้า",_G.Settings.Main.AutoElectricClaw,function(value)
			_G.AutoElectricClaw = value
			_G.Settings.Main.AutoElectricClaw = value
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while wait() do 
					if _G.AutoElectricClaw then
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electro") or game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electric Claw") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electric Claw") then
							if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro").Level.Value >= 400 then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw")
								if game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") then 
									local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") 
									game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
								end
							end  
							if game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electro") and game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electro").Level.Value >= 400 then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw")
								if game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") then 
									local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") 
									game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
								end
							end  
							if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro").Level.Value <= 399 then
								wait(.5)
								if game.Players.LocalPlayer.Backpack:FindFirstChild("Electro") then 
									local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Electro") 
									game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
								end
							end 
						else
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectro")
						end
					end
					if _G.AutoElectricClaw then
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electro") then
							if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electro") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Electro").Level.Value >= 400 or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Electro").Level.Value >= 400 then
								if AutoFarm == false then
									toposition(CFrame.new(-10371.4717, 330.764496, -10131.4199))           
									wait(1.1)                
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw","Start")
									wait(.5)
									toposition(CFrame.new(-12550.532226563, 336.22631835938, -7510.4233398438))                            
									wait(.5)
									toposition(CFrame.new(-10371.4717, 330.764496, -10131.4199))                           
									wait(1.1)
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw")
								elseif _G.AutoFarm == true then
									_G.AutoFarm = false
									wait(.5)
									toposition(CFrame.new(-10371.4717, 330.764496, -10131.4199))           
									wait(1.1)                
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw","Start")
									wait(.5)
									toposition(CFrame.new(-12550.532226563, 336.22631835938, -7510.4233398438))                            
									wait(.5)
									toposition(CFrame.new(-10371.4717, 330.764496, -10131.4199))                           
									wait(1.1)
									game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw")
									wait(.5)
									if game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") then 
										local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Electric Claw") 
										game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
									end
									_G.AutoFarm = true
								end
							end
						end
					end
				end
			end)    
		end)
		
		page14:Toggle("Auto DragonTalon \n หมัดมังกร v2",_G.Settings.Main.AutoDragonTalon,function(value)
			_G.AutoDragonTalon = value
			_G.Settings.Main.AutoDragonTalon = value
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoDragonTalon then
					if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Dragon Claw") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Dragon Claw") or game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Dragon Talon") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Dragon Talon") then
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Dragon Claw") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Dragon Claw").Level.Value >= 400 then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyDragonTalon")
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Talon") then 
							local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Talon") 
								game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
							end
						end  
						if game:GetService("Players").LocalPlayer.Character:FindFirstChild("Dragon Claw") and game:GetService("Players").LocalPlayer.Character:FindFirstChild("Dragon Claw").Level.Value >= 400 then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyDragonTalon")
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Talon") then 
							local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Talon") 
								game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
							end
						end  
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Dragon Claw") and game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Dragon Claw").Level.Value <= 399 then
							if game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Claw") then 
							local ToolHumanoid = game.Players.LocalPlayer.Backpack:FindFirstChild("Dragon Claw") 
								game.Players.LocalPlayer.Character.Humanoid:EquipTool(ToolHumanoid) 
							end
						end 
					else 
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","2")
					end
				end
			end
		end)
		
		function ChackMT(OP)
			if W1 then 
				if (OP=="Magma Ore") then 
					MaterialMob={"Military Soldier [Lv. 300]","Military Spy [Lv. 325]"};
				elseif ((OP=="Leather") or (v1=="Scrap Metal")) then 
					MaterialMob={"Brute [Lv. 45]"};
				elseif (OP=="Angel Wings") then 
					MaterialMob={"God's Guard [Lv. 450]"};
				elseif (OP=="Fish Tail") then 
					MaterialMob={"Fishman Warrior [Lv. 375]","Fishman Commando [Lv. 400]"};
				end 
			end 
			if W2 then 
				if (OP=="Magma Ore") then 
					MaterialMob={"Magma Ninja [Lv. 1175]"};
				elseif (OP=="Scrap Metal") then
					MaterialMob={"Swan Pirate [Lv. 775]"};
				elseif (OP=="Radioactive Material") then 
					MaterialMob={"Factory Staff [Lv. 800]"};
				elseif (OP=="Vampire Fang") then 
					MaterialMob={"Vampire [Lv. 975]"};
				elseif (OP=="Mystic Droplet") then 
					MaterialMob={"Water Fighter [Lv. 1450]","Sea Soldier [Lv. 1425]"};
				end 
			end 
			if W3 then 
				if (OP=="Mini Tusk") then 
					MaterialMob={"Mythological Pirate [Lv. 1850]"};
				elseif (OP=="Fish Tail") then 
					MaterialMob={"Fishman Raider [Lv. 1775]","Fishman Captain [Lv. 1800]"};
				elseif (OP=="Scrap Metal") then 
					MaterialMob={"Jungle Pirate [Lv. 1900]"};
				elseif (OP=="Dragon Scale") then 
					MaterialMob={"Dragon Crew Archer [Lv. 1600]","Dragon Crew Warrior [Lv. 1575]"};
				elseif (OP=="Conjured Cocoa") then 
					MaterialMob={"Cocoa Warrior [Lv. 2300]","Chocolate Bar Battler [Lv. 2325]","Sweet Thief [Lv. 2350]","Candy Rebel [Lv. 2375]"};
				elseif (OP=="Demonic Wisp") then 
					MaterialMob={"Demonic Soul [Lv. 2025]"};
				elseif (OP=="Gunpowder") then 
					MaterialMob={"Pistol Billionaire [Lv. 1525]"};
				end 
			end
		end
		
		local KOP
		if W1 then
			KOP = {
				"Magma Ore",
				"Leather",
				"Angel Wings"
			}
		elseif W2 then
			KOP = {
				"Magma Ore",
				"Scrap Metal",
				"Radioactive Material",
				"Vampire Fang",
				"Mystic Droplet"
			}
		elseif W3 then
			KOP = {
				"Mini Tusk",
				"Fish Tail",
				"Scrap Metal",
				"Dragon Scale",
				"Conjured Cocoa",
				"Demonic Wisp",
				"Gunpowder"
			}
		end
		page14:Label("Auto Material")
	
		page14:Dropdown("Select Material \n เลือกวัตถุดิบ",KOP,{},function(a)
			_G.Settings.Main.Select_Material = a
			SaveSettings()
		end)
	
		page14:Toggle("Auto Material Farm \n วัตถุดิบ",false,function(value)
			_G.MaterialFarm = value
			StopTween(_G.MaterialFarm)
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
				ChackMT(_G.Settings.Main.Select_Material)
				if _G.MaterialFarm then
					for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
						if table.find(MaterialMob,v.Name) and v.Humanoid.Health > 0 and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") then
							repeat wait()
								BringMobFarm = true
								EquipWeapon(_G.Select_Weapon)
								PosMon = v.HumanoidRootPart.CFrame
								v.HumanoidRootPart.Size = Vector3.new(60,60,60)
								v.HumanoidRootPart.Transparency = 1
								v.Humanoid.JumpPower = 0
								v.Humanoid.WalkSpeed = 0
								v.HumanoidRootPart.CanCollide = false
								v.Humanoid:ChangeState(11)
								if AttackRandomType == 1 then
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
								elseif AttackRandomType == 2 then
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
								elseif AttackRandomType == 3 then
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
								else
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
								end
								if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
									game:GetService("VirtualUser"):CaptureController()
									game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
								end
							until not _G.MaterialFarm or not v.Parent or v.Humanoid.Health <= 0
							BringMobFarm = false
						else
							getgenv().TP(CFrameMon)
						end
					end
				end
			end
		end)
	
		local Main = tab7:CraftPage('AutoItem 2/2',2)
	
		Main:Label("Auto Item")
	
		Main:Label("Auto Bartilo Quest")
	
		Main:Toggle("Auto Bartilo Quest \n หมวกทอง",_G.Settings.Main.Auto_Bartilo_Quest,function(value)
			_G.Auto_Bartilo_Quest = value
			_G.Settings.Main.Auto_Bartilo_Quest = value
			StopTween(_G.Auto_Bartilo_Quest)
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
			local MyLevel = game.Players.LocalPlayer.Data.Level.Value
			local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
				pcall(function()
					if _G.Auto_Bartilo_Quest and MyLevel >= 850 then
						if _G.AutoFarm == true then
							_G.AutoFarm = false
						end
						if game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BartiloQuestProgress","Bartilo") == 0 then
							if QuestC.Visible == true then
								if game:GetService("Workspace").Enemies:FindFirstChild("Swan Pirate [Lv. 775]") then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == "Swan Pirate [Lv. 775]" then
											if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") then
												repeat task.wait()
													if not string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, "Swan Pirate") then
														game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AbandonQuest")
													else
														PosMon = v.HumanoidRootPart.CFrame
														v.HumanoidRootPart.Size = Vector3.new(60,60,60)
														v.HumanoidRootPart.CanCollide = false
														v.Humanoid.WalkSpeed = 0
														v.Head.CanCollide = false
														BringMobFarm = true
														EquipWeapon(_G.Select_Weapon)
														v.HumanoidRootPart.Transparency = 1
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 30, 5))
	
														if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO then
															game:GetService("VirtualUser"):CaptureController()
															game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
														end
													end
												until not _G.Auto_Bartilo_Quest or not v.Parent or v.Humanoid.Health <= 0 or QuestC.Visible == false or not v:FindFirstChild("HumanoidRootPart")
											end
										end
									end
								else
									for i,v in pairs(workspace._WorldOrigin.EnemySpawns:GetChildren()) do
										if v.Name == "Swan Pirate" then local CFrameEnemySpawns = v.CFrame  wait(0.5)
											toposition(CFrameEnemySpawns * CFrame.new(0, 30, 5))
										end
									end
								end
							else
								repeat wait() toposition(CFrame.new(-461.533203, 72.3478546, 300.311096, 0.050853312, -0, -0.998706102, 0, 1, -0, 0.998706102, 0, 0.050853312)) until (CFrame.new(-461.533203, 72.3478546, 300.311096, 0.050853312, -0, -0.998706102, 0, 1, -0, 0.998706102, 0, 0.050853312).Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.Auto_Bartilo_Quest
								if (CFrame.new(-461.533203, 72.3478546, 300.311096, 0.050853312, -0, -0.998706102, 0, 1, -0, 0.998706102, 0, 0.050853312).Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 1 then
									BringMobFarm = false
									game:GetService('ReplicatedStorage').Remotes.CommF_:InvokeServer("StartQuest", "BartiloQuest", 1)
								end
							end
						elseif  game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BartiloQuestProgress","Bartilo") == 1 then
							if game:GetService("Workspace").Enemies:FindFirstChild("Jeremy [Lv. 850] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Jeremy [Lv. 850] [Boss]" then
										if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") then
											repeat task.wait()
												PosMon = v.HumanoidRootPart.CFrame
												v.HumanoidRootPart.Size = Vector3.new(60,60,60)
												v.HumanoidRootPart.CanCollide = false
												v.Humanoid.WalkSpeed = 0
												v.Head.CanCollide = false
												BringMobFarm = true
												EquipWeapon(_G.Select_Weapon)
												v.HumanoidRootPart.Transparency = 1
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, 30, 5))
	
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until not _G.Auto_Bartilo_Quest or not v.Parent or v.Humanoid.Health <= 0 or QuestC.Visible == false or not v:FindFirstChild("HumanoidRootPart")
										end
									end
								end
							else
								toposition(CFrame.new(2158.97412, 449.056244, 705.411682, -0.754199564, -4.17389057e-09, -0.656645238, -4.47752875e-08, 1, 4.50709301e-08, 0.656645238, 6.3393955e-08, -0.754199564))
							end
						elseif  game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BartiloQuestProgress","Bartilo") == 2 then
							repeat wait() toposition(CFrame.new(-1830.83972, 10.5578213, 1680.60229, 0.979988456, -2.02152783e-08, -0.199054286, 2.20792113e-08, 1, 7.1442483e-09, 0.199054286, -1.13962431e-08, 0.979988456)) until (CFrame.new(-1830.83972, 10.5578213, 1680.60229, 0.979988456, -2.02152783e-08, -0.199054286, 2.20792113e-08, 1, 7.1442483e-09, 0.199054286, -1.13962431e-08, 0.979988456).Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 1 or _G.Auto_Bartilo_Quest == false
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate1.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate2.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate3.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate4.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate5.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate6.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate7.CFrame
							wait(0.7)
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = workspace.Map.Dressrosa.BartiloPlates.Plate8.CFrame
							wait(0.7)
							if _G.Settings.Main.AutoFarm == true then
								_G.AutoFram = true
							end
							wait(2.5) 
							_G.Auto_Bartilo_Quest = false
						end
					end
				end)
			end
		end)
	
		Main:Label("Auto Rainbow Haki")
		
		Main:Toggle("Auto Rainbow Haki \n ฮาคิสีรุ้ง",_G.Settings.Main.Auto_Rainbow_Haki,function(value)
			_G.Auto_Rainbow_Haki = value
			_G.Settings.Main.Auto_Rainbow_Haki = value
			StopTween(_G.Auto_Rainbow_Haki)
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while wait(.1) do
					if _G.Auto_Rainbow_Haki then
						if game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false then
							toposition(CFrame.new(-11892.0703125, 930.57672119141, -8760.1591796875))
							if (Vector3.new(-11892.0703125, 930.57672119141, -8760.1591796875) - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 30 then
								wait(1.5)
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("HornedMan","Bet")
							end
						elseif game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == true and string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, "Stone") then
							if game:GetService("Workspace").Enemies:FindFirstChild("Stone [Lv. 1550] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Stone [Lv. 1550] [Boss]" then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(5,10,7))
											v.HumanoidRootPart.CanCollide = false
											v.HumanoidRootPart.CFrame = OldCFrameRainbow
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
										until _G.Auto_Rainbow_Haki == false or v.Humanoid.Health <= 0 or not v.Parent or game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false
									end
								end
							else
								toposition(CFrame.new(-1086.11621, 38.8425903, 6768.71436, 0.0231462717, -0.592676699, 0.805107772, 2.03251839e-05, 0.805323839, 0.592835128, -0.999732077, -0.0137055516, 0.0186523199))
							end
						elseif game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == true and string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, "Island Empress") then
							if game:GetService("Workspace").Enemies:FindFirstChild("Island Empress [Lv. 1675] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Island Empress [Lv. 1675] [Boss]" then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(5,10,7))
											v.HumanoidRootPart.CanCollide = false
											v.HumanoidRootPart.CFrame = OldCFrameRainbow
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
										until _G.Auto_Rainbow_Haki == false or v.Humanoid.Health <= 0 or not v.Parent or game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false
									end
								end
							else
								toposition(CFrame.new(5713.98877, 601.922974, 202.751251, -0.101080291, -0, -0.994878292, -0, 1, -0, 0.994878292, 0, -0.101080291))
							end
						elseif string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, "Kilo Admiral") then
							if game:GetService("Workspace").Enemies:FindFirstChild("Kilo Admiral [Lv. 1750] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Kilo Admiral [Lv. 1750] [Boss]" then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(5,10,7))
											v.HumanoidRootPart.CanCollide = false
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											v.HumanoidRootPart.CFrame = OldCFrameRainbow
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
										until _G.Auto_Rainbow_Haki == false or v.Humanoid.Health <= 0 or not v.Parent or game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false
									end
								end
							else
								toposition(CFrame.new(2877.61743, 423.558685, -7207.31006, -0.989591599, -0, -0.143904909, -0, 1.00000012, -0, 0.143904924, 0, -0.989591479))
							end
						elseif string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, "Captain Elephant") then
							if game:GetService("Workspace").Enemies:FindFirstChild("Captain Elephant [Lv. 1875] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Captain Elephant [Lv. 1875] [Boss]" then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(5,10,7))
											v.HumanoidRootPart.CanCollide = false
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											v.HumanoidRootPart.CFrame = OldCFrameRainbow
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
										until _G.Auto_Rainbow_Haki == false or v.Humanoid.Health <= 0 or not v.Parent or game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false
									end
								end
							else
								toposition(CFrame.new(-13485.0283, 331.709259, -8012.4873, 0.714521289, 7.98849911e-08, 0.69961375, -1.02065748e-07, 1, -9.94383065e-09, -0.69961375, -6.43015241e-08, 0.714521289))
							end
						elseif string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text, "Beautiful Pirate") then
							if game:GetService("Workspace").Enemies:FindFirstChild("Beautiful Pirate [Lv. 1950] [Boss]") then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == "Beautiful Pirate [Lv. 1950] [Boss]" then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											toposition(v.HumanoidRootPart.CFrame * CFrame.new(5,10,7))
											v.HumanoidRootPart.CanCollide = false
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											v.HumanoidRootPart.CFrame = OldCFrameRainbow
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
										until _G.Auto_Rainbow_Haki == false or v.Humanoid.Health <= 0 or not v.Parent or game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == false
									end
								end
							else
								toposition(CFrame.new(5312.3598632813, 20.141201019287, -10.158538818359))
							end
						else
							toposition(CFrame.new(-11892.0703125, 930.57672119141, -8760.1591796875))
							if (Vector3.new(-11892.0703125, 930.57672119141, -8760.1591796875) - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 30 then
								wait(1.5)
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("HornedMan","Bet")
							end
						end
					end
				end
			end)
		end)
		
		Main:Label("Auto Rengoku")
	
		Main:Toggle("Auto Rengoku \n ดาบเลนโกคุ",_G.Settings.Main.AutoRengoku,function(value)
			_G.AutoRengoku = value
			_G.Settings.Main.AutoRengoku = value
			StopTween(_G.AutoRengoku)
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while wait() do
					if _G.AutoRengoku and Check_Sword("Rengoku") == nil then
						_G.AutoFarm = false
						if game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Hidden Key") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Hidden Key") then
							EquipWeapon("Hidden Key")
							toposition(CFrame.new(6571.1201171875, 299.23028564453, -6967.841796875))
							wait(5)
							if _G.Settings.Main.AutoFarm == true then
								_G.AutoFarm = true
								_G.AutoRengoku = false
								_G.Settings.Main.AutoRengoku = false
								SaveSettings()
							end
						elseif game:GetService("Workspace").Enemies:FindFirstChild("Snow Lurker [Lv. 1375]") or game:GetService("Workspace").Enemies:FindFirstChild("Arctic Warrior [Lv. 1350]") then
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if (v.Name == "Snow Lurker [Lv. 1375]" or v.Name == "Arctic Warrior [Lv. 1350]") and v.Humanoid.Health > 0 then
									repeat task.wait()
										EquipWeapon(_G.Select_Weapon)
										v.HumanoidRootPart.CanCollide = false
										v.HumanoidRootPart.Size = Vector3.new(50,50,50)
										PosMon = v.HumanoidRootPart.CFrame
										toposition(v.HumanoidRootPart.CFrame * CFrame.new(0,30,7))
										if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
											game:GetService("VirtualUser"):CaptureController()
											game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
										end
										BringMobFarm = true
									until game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Hidden Key") or _G.AutoRengoku == false or not v.Parent or v.Humanoid.Health <= 0
									BringMobFarm = false
								end
							end
						else
							BringMobFarm = false
							toposition(CFrame.new(5439.716796875, 84.420944213867, -6715.1635742188))
						end
					elseif _G.AutoRengoku and Check_Sword("Rengoku") == true then
						if _G.Settings.Main.AutoFarm == true then
							_G.AutoFarm = true
							_G.AutoRengoku = false
							_G.Settings.Main.AutoRengoku = false
							SaveSettings()
						end
					end
				end
			end)
		end)
		
		Main:Toggle("Auto Race V2 \n Race V2",_G.Settings.Main.Auto_RaceV2,function(value)
			_G.Auto_RaceV2 = value
			_G.Settings.Main.Auto_RaceV2 = value
			StopTween(_G.Auto_RaceV2)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				local CFrame_Mon = CFrame.new(-2774.83398, 71.664093, -3568.6709, 0.731384635, 0, 0.681965172, 0, 1, 0, -0.681965172, 0, 0.731384635)
				if _G.Auto_RaceV2 then
					pcall(function()
						if game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Alchemist","1") == 1 then
							if not game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Flower 2") and not game:GetService("Players").LocalPlayer.Character:FindFirstChild("Flower 2") then
								toposition(game.Workspace.Flower2.CFrame)
							elseif not game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Flower 1") and not game:GetService("Players").LocalPlayer.Character:FindFirstChild("Flower 1") then
								toposition(game.Workspace.Flower1.CFrame)
							elseif not game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Flower 3") and not game:GetService("Players").LocalPlayer.Character:FindFirstChild("Flower 3") then
								if game:GetService("Workspace").Enemies:FindFirstChild("Zombie [Lv. 950]") then
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == "Zombie [Lv. 950]" then
											if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
												repeat wait()
													BringMobFarm = true
													EquipWeapon(_G.Select_Weapon)
													PosMon = v.HumanoidRootPart.CFrame
													v.HumanoidRootPart.Size = Vector3.new(60,60,60)
													v.HumanoidRootPart.Transparency = 1
													v.Humanoid.JumpPower = 0
													v.Humanoid.WalkSpeed = 0
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid:ChangeState(11)
													if AttackRandomType == 1 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
													elseif AttackRandomType == 2 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
													elseif AttackRandomType == 3 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
													else
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
													end
													if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
														game:GetService("VirtualUser"):CaptureController()
														game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
													end
												until not _G.Auto_RaceV2 or v.Humanoid.Health <= 0 or not v.Parent or v.Humanoid.Health <= 0 or game:GetService("Players").LocalPlayer.Backpack:FindFirstChild("Flower 3") or game:GetService("Players").LocalPlayer.Character:FindFirstChild("Flower 3")
												BringMobFarm = false
											end
										end
									end
								else
									for i,v in pairs(workspace._WorldOrigin.EnemySpawns:GetChildren()) do
										if not _G.AutoFarmFast and v.Name == "Zombie [Lv. 950]" then local CFrameEnemySpawns = v.CFrame  wait(0.5)
											toposition(CFrameEnemySpawns * CFrame.new(0, 30, 5))
										end
									end
								end
							end
						elseif game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Alchemist","1") == 2 then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Alchemist","3")
						else
							toposition(CFrame_Mon)
							if (CFrame_Mon.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 1 then
								BringMobFarm = false
								wait(0.2)
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Alchemist","1") 
								wait(0.5) 
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Alchemist","2")
								wait(0.5)
							end
						end
					end)
				end
			end
		end)
	
		local Ectoplasm = Main:Label("Label")
	
		spawn(function()
			while wait() do
				pcall(function()
					Ectoplasm.Refresh("[ Ectoplasm : "..game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Ectoplasm","Check").." ]")
				end)
			end
		end)
	
		Main:Toggle("Auto Farm Ectoplasm \n เสดอวกแมว",_G.Settings.Main.AutoEctoplasm,function(value)
			_G.AutoEctoplasm = value
			_G.Settings.Main.AutoEctoplasm = value
			StopTween(_G.AutoEctoplasm)
			SaveSettings()
		end)
		
		spawn(function()
			pcall(function()
				while wait() do
					if _G.AutoEctoplasm then
						if game:GetService("Workspace").Enemies:FindFirstChild("Ship Deckhand [Lv. 1250]") or game:GetService("Workspace").Enemies:FindFirstChild("Ship Engineer [Lv. 1275]") or game:GetService("Workspace").Enemies:FindFirstChild("Ship Steward [Lv. 1300]") or game:GetService("Workspace").Enemies:FindFirstChild("Ship Officer [Lv. 1325]") then
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if string.find(v.Name, "Ship") then
									repeat task.wait()
										if string.find(v.Name,"Ship") then
											EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.CanCollide = false
											v.Head.CanCollide = false
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											if AttackRandomType == 1 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											elseif AttackRandomType == 2 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
											elseif AttackRandomType == 3 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
											else
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											end
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											PosMon = v.HumanoidRootPart.CFrame
											BringMobFarm = true
										else
											BringMobFarm = false
											toposition(CFrame.new(911.35827636719, 125.95812988281, 33159.5390625))
										end
									until _G.AutoEctoplasm == false or not v.Parent or v.Humanoid.Health <= 0
								end
							end
						else
							BringMobFarm = false
							local Distance = (Vector3.new(911.35827636719, 125.95812988281, 33159.5390625) - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude
							if Distance > 18000 then
								game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(923.21252441406, 126.9760055542, 32852.83203125))
							end
							toposition(CFrame.new(911.35827636719, 125.95812988281, 33159.5390625))
						end
					end
				end
			end)
		end)
		
		Main:Label("Oder Sword")
		
		Main:Toggle("Auto Oder Sword \n ตีบอส ลอ", _G.AutoOderSword,function(value)
			 _G.AutoOderSword = value
			StopTween( _G.AutoOderSword)
		end)
	
		spawn(function()
			while wait() do
				if  _G.AutoOderSword then
					pcall(function()
						if game:GetService("Workspace").Enemies:FindFirstChild("Order [Lv. 1250] [Raid Boss]") then
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if v.Name == "Order [Lv. 1250] [Raid Boss]" then
									if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat task.wait()
											EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.CanCollide = false
											v.Head.CanCollide = false
											v.HumanoidRootPart.Size = Vector3.new(50,50,50)
											if AttackRandomType == 1 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											elseif AttackRandomType == 2 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
											elseif AttackRandomType == 3 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
											else
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											end
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
											PosMon = v.HumanoidRootPart.CFrame
											BringMobFarm = true
											sethiddenproperty(game.Players.LocalPlayer,"SimulationRadius",math.huge)
										until not  _G.AutoOderSword or not v.Parent or v.Humanoid.Health <= 0
									end
								end
							end
						else
							if game:GetService("ReplicatedStorage"):FindFirstChild("Order [Lv. 1250] [Raid Boss]") then
								toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Order [Lv. 1250] [Raid Boss]").HumanoidRootPart.CFrame * CFrame.new(2,20,2))
							end
						end
					end)
				end
			end
		end)
		
		Main:Button("Buy Microchip Oder Boss \n ซื้อชิพบอส ลอ",function()
			local args = {
			[1] = "BlackbeardReward",
			[2] = "Microchip",
			[3] = "2"
			}
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
		end)
	
		Main:Button("Start Go To Raid Oder Boss \n ไปดันลอ",function()
			if World2 then
				fireclickdetector(game:GetService("Workspace").Map.CircleIsland.RaidSummon.Button.Main.ClickDetector)
			end
		end)
		
	
		if W3 then
		Main:Label('World : 3')
		Main:Label('🦴Auto Farm Bone🦴')
		
		local Bones = Main:Label("Label")
		
		spawn(function()
			while wait() do
				Bones.Refresh("Bone : "..game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Bones","Check"))
			end
		end)
		
		Main:Toggle("Auto Farm Bone \n ฟาร์มกระดูก",_G.Settings.Main.AutoFarmBone,function(value)
			_G.AutoFarmBone = value
			_G.Settings.Main.AutoFarmBone = value
			StopTween(_G.AutoFarmBone)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					if _G.AutoFarmBone then
						if game:GetService("Workspace").Enemies:FindFirstChild("Reborn Skeleton [Lv. 1975]") or game:GetService("Workspace").Enemies:FindFirstChild("Living Zombie [Lv. 2000]") or game:GetService("Workspace").Enemies:FindFirstChild("Demonic Soul [Lv. 2025]") or game:GetService("Workspace").Enemies:FindFirstChild("Posessed Mummy [Lv. 2050]") then
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if v.Name == "Reborn Skeleton [Lv. 1975]" or v.Name == "Living Zombie [Lv. 2000]" or v.Name == "Demonic Soul [Lv. 2025]" or v.Name == "Posessed Mummy [Lv. 2050]" then
									if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat wait()
											BringMobFarm = true
											EquipWeapon(_G.Select_Weapon)
											PosMon = v.HumanoidRootPart.CFrame
											v.HumanoidRootPart.Size = Vector3.new(60,60,60)
											v.HumanoidRootPart.Transparency = 1
											v.Humanoid.JumpPower = 0
											v.Humanoid.WalkSpeed = 0
											v.HumanoidRootPart.CanCollide = false
											v.Humanoid:ChangeState(11)
											if AttackRandomType == 1 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											elseif AttackRandomType == 2 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
											elseif AttackRandomType == 3 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
											else
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											end
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
										until not _G.AutoFarmBone or v.Humanoid.Health <= 0 or not v.Parent or v.Humanoid.Health <= 0
										BringMobFarm = false
									end
								end
							end
						else
							local CFrameMon = CFrame.new(-9504.8564453125, 172.14292907714844, 6057.259765625)
							repeat wait() toposition(CFrameMon) until (CFrameMon.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarmBone
						end
					end
				end)
			end
		end)
	
		function CheckBoneQuest()
			if _G.Settings.Main.Select_Bone_Quest == "Reborn Skeleton" then
				NMon = "Reborn Skeleton [Lv. 1975]"
				NameQ = "HauntedQuest1"
				Lv = 1
				CFrameMon = CFrame.new(-9481.1709, 142.1306, 5565.32422, -0.626888812, -3.51863036e-08, -0.779108763, -4.11875938e-08, 1, -1.20217676e-08, 0.779108763, 2.45533034e-08, -0.626888812)
	
				return {
					[1] = NameQ,
					[2] = Lv,
					[3] = CFrameMon,
					[4] = NMon
				}
			elseif _G.Settings.Main.Select_Bone_Quest == "Living Zombie" then
				NMon = "Living Zombie [Lv. 2000]"
				NameQ = "HauntedQuest1"
				Lv = 2
				CFrameMon = CFrame.new(-9481.1709, 142.1306, 5565.32422, -0.626888812, -3.51863036e-08, -0.779108763, -4.11875938e-08, 1, -1.20217676e-08, 0.779108763, 2.45533034e-08, -0.626888812)
	
				return {
					[1] = NameQ,
					[2] = Lv,
					[3] = CFrameMon,
					[4] = NMon
				}
			elseif _G.Settings.Main.Select_Bone_Quest == "Demonic Soul" then
				NMon = "Demonic Soul [Lv. 2025]"
				NameQ = "HauntedQuest2"
				Lv = 1
				CFrameMon = CFrame.new(-9515.58203, 172.275055, 6078.54443, 0.000217142268, -1.48870256e-11, 1, -4.53071478e-12, 1, 1.48880092e-11, -1, -4.53394744e-12, 0.000217142268)
				return {
					[1] = NameQ,
					[2] = Lv,
					[3] = CFrameMon,
					[4] = NMon
				}
			elseif _G.Settings.Main.Select_Bone_Quest == "Posessed Mummy" then
				NMon = "Posessed Mummy [Lv. 2050]"
				NameQ = "HauntedQuest2"
				Lv = 2
				CFrameMon = CFrame.new(-9515.58203, 172.275055, 6078.54443, 0.000217142268, -1.48870256e-11, 1, -4.53071478e-12, 1, 1.48880092e-11, -1, -4.53394744e-12, 0.000217142268)
	
				return {
					[1] = NameQ,
					[2] = Lv,
					[3] = CFrameMon,
					[4] = NMon
				}
			else
				NMon = "Reborn Skeleton [Lv. 1975]"
				NameQ = "HauntedQuest1"
				Lv = 1
				CFrameMon = CFrame.new(-9481.1709, 142.1306, 5565.32422, -0.626888812, -3.51863036e-08, -0.779108763, -4.11875938e-08, 1, -1.20217676e-08, 0.779108763, 2.45533034e-08, -0.626888812)
	
				return {
					[1] = NameQ,
					[2] = Lv,
					[3] = CFrameMon,
					[4] = NMon
				}
			end
		end
	
		Main:Label('🦴Auto Farm Bone Quest🦴')
	
		Main:Dropdown("Select Bone Quest \n เลือกเควสกระดูก",{"Reborn Skeleton","Living Zombie","Demonic Soul","Posessed Mummy"},_G.Settings.Main.Select_Bone_Quest,function(a)
			_G.Settings.Main.Select_Bone_Quest = a
			SaveSettings()
		end)
	
		Main:Toggle("Auto Farm Bone Quest \n ฟาร์มกระดูก เควส",_G.Settings.Main.AutoFarmBoneQuest,function(value)
			_G.AutoFarmBoneQuest = value
			_G.Settings.Main.AutoFarmBoneQuest = value
			StopTween(_G.AutoFarmBoneQuest)
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
				pcall(function()
					if _G.AutoFarmBoneQuest then
						local QuestC = game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
						BringMobFarm = false
						if QuestC.Visible == false then
							repeat wait() toposition(CheckBoneQuest()[3]) until (CheckBoneQuest()[3].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarmBoneQuest
							if (CheckBoneQuest()[3].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 10 then
								local args = {
									[1] = "StartQuest",
									[2] = CheckBoneQuest()[1],
									[3] = CheckBoneQuest()[2]
								}
								game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("CommF_"):InvokeServer(unpack(args))
							end
						else
							if game:GetService("Workspace").Enemies:FindFirstChild(CheckBoneQuest()[4]) then
								for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
									if v.Name == CheckBoneQuest()[4] then
										if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
											repeat wait()
												BringMobFarm = true
												EquipWeapon(_G.Select_Weapon)
												PosMon = v.HumanoidRootPart.CFrame
												v.HumanoidRootPart.Size = Vector3.new(60,60,60)
												v.HumanoidRootPart.Transparency = 1
												v.Humanoid.JumpPower = 0
												v.Humanoid.WalkSpeed = 0
												v.HumanoidRootPart.CanCollide = false
												v.Humanoid:ChangeState(11)
												if AttackRandomType == 1 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												elseif AttackRandomType == 2 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
												elseif AttackRandomType == 3 then
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
												else
													toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
												end
												if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
													game:GetService("VirtualUser"):CaptureController()
													game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
												end
											until not _G.AutoFarmBoneQuest or v.Humanoid.Health <= 0 or not v.Parent or v.Humanoid.Health <= 0 or QuestC.Visible == false
											BringMobFarm = false
										end
									end
								end
							else
								local CFrameMon = CFrame.new(-9504.8564453125, 172.14292907714844, 6057.259765625)
								if _G.Bypass and (CFrameMon.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude >= 3000 then
									repeat wait() toposition(CFrameMon) until (CFrameMon.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFarmBone
								else
									for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
										if v.Name == "Reborn Skeleton [Lv. 1975]" or v.Name == "Living Zombie [Lv. 2000]" or v.Name == "Demonic Soul [Lv. 2025]" or v.Name == "Posessed Mummy [Lv. 2050]" then
											if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
												repeat wait()
													BringMobFarm = true
													EquipWeapon(_G.Select_Weapon)
													PosMon = v.HumanoidRootPart.CFrame
													v.HumanoidRootPart.Size = Vector3.new(60,60,60)
													v.HumanoidRootPart.Transparency = 1
													v.Humanoid.JumpPower = 0
													v.Humanoid.WalkSpeed = 0
													v.HumanoidRootPart.CanCollide = false
													v.Humanoid:ChangeState(11)
													if AttackRandomType == 1 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
													elseif AttackRandomType == 2 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
													elseif AttackRandomType == 3 then
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
													else
														toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
													end
													if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
														game:GetService("VirtualUser"):CaptureController()
														game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
													end
												until not _G.AutoFarmBoneQuest or v.Humanoid.Health <= 0 or not v.Parent or v.Humanoid.Health <= 0 or QuestC.Visible == false
												BringMobFarm = false
											end
										else
											toposition(CFrameMon)
										end
									end
								end
							end
						end
					end
				end)
			end
		end)
		
		Main:Label('🦴Auto Random Bone🦴')
	
		Main:Toggle("Auto Random Bone \n ออโต้สุมกระดูก",false,function(v)
			RB = v
		end)
	
		spawn(function()
			while wait() do
				if RB then
					local args = {
						[1] = "Bones",
						[2] = "Buy",
						[3] = 1,
						[4] = 1
					}
					
					game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("CommF_"):InvokeServer(unpack(args))				
				end
			end
		end)
		
		Main:Label('Auto Full Moon Hop')
		
		local FM = Main:Label("Label")
		
		task.spawn(function()
			while task.wait() do
				pcall(function()
					if game:GetService("Lighting").Sky.MoonTextureId=="http://www.roblox.com/asset/?id=9709149431" then
						FM.Refresh("Spawn : 🟢")
					else
						FM.Refresh("Not Spawn : 🔴")
					end
				end)
			end
		end)
		
		Main:Toggle("Auto Full Moon Hop \n หาพระจันทร์เต็มดวง",_G.Settings.Main.Auto_Full_Moon_Hop,function(value)
			_G.Auto_Full_MoonHop = value
			_G.Settings.Main.Auto_Full_Moon_Hop = value
			StopTween(_G.Auto_Full_MoonHop)
			SaveSettings()
		end)
		
		task.spawn(function()
			while task.wait() do
				if _G.Auto_Full_MoonHop then
					pcall(function()
						if game:GetService("Lighting").Sky.MoonTextureId=="http://www.roblox.com/asset/?id=9709149431" then
						   wait(15)
							_G.Auto_Full_MoonHop = false
						else
							Hop()
						end
					end)
				end
			end
		end)
		
		Main:Label('Tushita')
		
		Main:Toggle("Auto Holy Torch \n ออโต้จุดคบเพลิง",_G.Settings.Main.AutoHolyTorch,function(value)
			_G.AutoHolyTorch = value
			_G.Settings.Main.AutoHolyTorch = value
			StopTween(_G.Auto_Full_MoonHop)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoHolyTorch then
					pcall(function()
						wait(1)
						toposition(CFrame.new(-10752, 417, -9366))
						wait(3.5)
						toposition(CFrame.new(-11672, 334, -9474))
						wait(4)
						toposition(CFrame.new(-12132, 521, -10655))
						wait(5.5)
						toposition(CFrame.new(-13336, 486, -6985))
						wait(6)
						toposition(CFrame.new(-13489, 332, -7925))
					end)
				end
			end
		end)
		
		Main:Toggle("Auto Tushita Boss \n ออโต้ตีบอสทูชิตะ",_G.Settings.Main.AutoTushitaBoss,function(value)
			_G.AutoTushitaBoss = value
			_G.Settings.Main.AutoTushitaBoss = value
			StopTween(_G.Auto_Full_MoonHop)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.AutoTushitaBoss then
					if game:GetService("Workspace").Enemies:FindFirstChild("Longma [Lv. 2000] [Boss]") then
						for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
							if v.Name == ("Longma [Lv. 2000] [Boss]" or v.Name == "Longma [Lv. 2000] [Boss]") and v.Humanoid.Health > 0 and v:IsA("Model") and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") then
								repeat wait()
									BringMobFarm = true
									EquipWeapon(_G.Select_Weapon)
									PosMon = v.HumanoidRootPart.CFrame
									v.HumanoidRootPart.Size = Vector3.new(60,60,60)
									v.HumanoidRootPart.Transparency = 1
									v.Humanoid.JumpPower = 0
									v.Humanoid.WalkSpeed = 0
									v.HumanoidRootPart.CanCollide = false
									v.Humanoid:ChangeState(11)
									if AttackRandomType == 1 then
										toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
									elseif AttackRandomType == 2 then
										toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
									elseif AttackRandomType == 3 then
										toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
									else
										toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
									end
									if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
										game:GetService("VirtualUser"):CaptureController()
										game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
									end
								until not _G.AutoTushitaBoss or not v.Parent or v.Humanoid.Health <= 0
								BringMobFarm = false
							end
						end
					else
						toposition(CFrame.new(-10238.875976563, 389.7912902832, -9549.7939453125))
					end
				end
			end
		end)
		
		Main:Label('Mirage Island')
		
		local Mirage_Island_Stats = Main:Label("Mirage Island : 🔴")
		
		spawn(function()
			while wait() do
				if game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Mirage Island") then
					Mirage_Island_Stats.Refresh("Mirage Island : 🟢")
				else
					Mirage_Island_Stats.Refresh("Mirage Island : 🔴")
				end
			end
		end)
		
		Main:Toggle("Auto Mirage Island \n เกาะลับ",_G.Settings.Main.Auto_Mirage_Island,function(value)
			Auto_Mirage_Island = value
			Point = value
			Boat = false
			_G.Settings.Main.Auto_Mirage_Island = value
			StopTween(Auto_Mirage_Island)
			SaveSettings()
		end)
		
		function BuyBoat()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyBoat","PirateSloop")
		end
		
		spawn(function()
			while task.wait() do
				pcall(function()
					if Auto_Mirage_Island then
						for i,v in pairs(game:GetService("Workspace")["_WorldOrigin"].Locations:GetChildren()) do
							if v.Name == "Mirage Island" then
								repeat task.wait()
									if game.Players.LocalPlayer.Character.Humanoid.Sit == true then game.Players.LocalPlayer.Character.Humanoid.Sit = false end
									toposition(game:GetService("Workspace")._WorldOrigin.Locations:GetChildren()[28].CFrame * CFrame.new(0,300,0))
								until Auto_Mirage_Island == false or not game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Mirage Island")
							else
								repeat task.wait()
									local HHH = CFrame.new(2316.06592, 3.0538919, 2158.52979, -0.581450701, -9.7121891e-08, 0.813581645, -7.36259906e-08, 1, 6.67566766e-08, -0.813581645, -2.10850377e-08, -0.581450701)
									local HydarBoat = CFrame.new(3230.96802, 9.4578352, 1555.08154, 0.980988681, 1.76605663e-08, 0.1940649, -4.35044489e-09, 1, -6.90121098e-08, -0.1940649, 6.68558329e-08, 0.980988681)
									function Chackboat()
										if tostring(game.Players.LocalPlayer.Team) == "Marines" then
											Boat = game:GetService("Workspace").Boats.MarineSloop
										elseif tostring(game.Players.LocalPlayer.Team) == "Pirates" then
											Boat = game:GetService("Workspace").Boats.PirateSloop
										end
										return {
											[1] = Boat
										} 
									end
									if Auto_Mirage_Island == true and Boat == true then
										repeat task.wait()
											toposition(HHH)
											if (HHH.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 10 then
												tweenModel(Chackboat()[1],game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame)
											end
											wait(1)
											Point = false
										until game.Players.LocalPlayer.Character.Humanoid.Sit == true and Point == false
									elseif Point == true then
										toposition(HydarBoat)
										if (HydarBoat.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 10 then
											BuyBoat()
											wait(0.1)
											Boat = true
										end
									end
								until Auto_Mirage_Island == false or game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Mirage Island")
								game:GetService("VirtualInputManager"):SendKeyEvent(false,"W",false,game)
							end
						end
					end
				end)
			end
		end)
		spawn(function()
			while wait() do 
				if Auto_Mirage_Island == true and game.Players.LocalPlayer.Character.Humanoid.Sit == true then
					game:GetService("VirtualInputManager"):SendKeyEvent(true,"W",false,game)
				end
			end
		end)
		
		--Main:Button("BuyBoat",function()
			--BuyBoat()
		--end)
		
		Main:Toggle("Auto Gear \n เก็บฟันเฟือง",_G.Settings.Main.Auto_Gear,function(value)
			Auto_Gear = value
			_G.Settings.Main.Auto_Gear = value
			StopTween(Auto_Gear)
			SaveSettings()
		end)
	
		task.spawn(function()
			while task.wait(.01) do
				if Auto_Gear then
					for i,v in pairs(game:GetService("Workspace").Map:FindFirstChild('MysticIsland'):GetChildren()) do
						if v.Name == "Part" then
							if v.ClassName == "MeshPart" then
								toposition(v.CFrame)
								v.Transparency = 0
							end
						end
					end
				end
			end
		end)
	
		Main:Toggle("Esp Gear \n มองฟันเฟือง",false,function(value)
			Esp_Gear = value
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
				if Esp_Gear == true then
					ESPGear()
				else
					for i,v in pairs(game:GetService("Workspace").Map:FindFirstChild('MysticIsland'):GetChildren()) do
						if v:FindFirstChild('NameEsp') then
							v:FindFirstChild('NameEsp'):Destroy()
						end
					end
				end
			end
		end)
	
		Main:Toggle("Esp Mirage Island \n มองเกาะลับ",false,function(value)
			ESP_Mirage_Island = value
			ESPMirageIsland()
		end)
	
		spawn(function()
			while wait() do
				if ESP_Mirage_Island == true then
					ESPMirageIsland()
				else
					for i,v in pairs(game:GetService("Workspace").Map:FindFirstChild('MysticIsland'):GetChildren()) do
						if v:FindFirstChild('EspMirage') then
							v:FindFirstChild('EspMirage'):Destroy()
						end
					end
				end
			end
		end)
	
		local Lighting = game:GetService("Lighting")
		local Cam = game.Workspace.CurrentCamera
		local CFNew, CFAng = CFrame.new, CFrame.Angles
		local asin = math.asin
	
		local Camera = workspace.CurrentCamera
		local Player = game.Players.LocalPlayer
		repeat wait() until Player.Character
		local Character = Player.Character
		local Root = Character:WaitForChild("HumanoidRootPart")
		local Neck = Character:FindFirstChild("Neck", true)
		local YOffset = Neck.C0.Y
	
		Main:Toggle("Lock Moon \n มอง พระจันทร์",false,function(v)
			_G.LockMoon = v
			game:GetService("RunService").RenderStepped:Connect(function()
				if _G.LockMoon then
					game:GetService("ReplicatedStorage").Remotes.CommE:FireServer("ActivateAbility")
	
					local pos = Vector3.new(0, 0, 0)
					local lookAt = Lighting:GetMoonDirection()
					local cameraCFrame = CFrame.new(pos, lookAt)
					workspace.CurrentCamera.CFrame = cameraCFrame
					local CameraDirection = Root.CFrame:toObjectSpace(cameraCFrame).lookVector.unit
					if Neck and Lock then
						Neck.C0 = CFNew(0, YOffset, 0) * CFAng(0, -asin(CameraDirection.x), 0) * CFAng(asin(CameraDirection.y), 0, 0)
					end
				else
					Cam.FieldOfView = 70
				end
			end)
		end)
	
		Main:Label('Radc V4')
		
		Main:Button("Go To Room Radc V4 \n ไปห้องทำเผ่า v4",function(value)
			local c = game:GetService("Workspace").Map["Temple of Time"].DoNotEnter.CFrame
			game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.CFrame = c
		end)
		
		Main:Button("Complete Mink Trial \n จบเควสเผ่าไซบอร์ก",function(t)
			TweenButton = true
			wait(.1)
			toposition(game:GetService("Workspace").Map.MinkTrial.Ceiling.CFrame*CFrame.new(0,-20,0))
			wait(.1)
			TweenButton = false
		end)
	
	
		
		Main:Button("Complete Cyborg Trial \n จบเควสเผ่าไซบอร์ก",function(t)
			TweenButton = true
			wait(.1)
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame * CFrame.new(0,150,0)
			wait(60)
			TweenButton = false
		end)
		
	
		Main:Button("Bug Sky \n แก้บัคลอยฟ้า",function(t)
			TweenButton = false
		end)
	
		Main:Label('Teleport Door')
		
		Main:Button("Teleport Cyborg Door \n ไปประตูทำเผ่าไซบอร์ก",function(t)
			TweenButton = true
			wait(.1)
			toposition(CFrame.new(28492.4140625, 14894.4267578125, -422.1100158691406))
			wait(4)
			TweenButton = false
		end)
		
		Main:Button("Teleport Fish Door \n ไปประตูทำเผ่าปลา",function(t)
			TweenButton = true
			wait(.1)
			toposition(CFrame.new(28224.056640625, 14889.4267578125, -210.5872039794922))
			wait(4)
			TweenButton = false
		end)
		
		Main:Button("Teleport Ghoul Door \n ไปประตูทำเผ่ากลูน",function(t)
			TweenButton = true
			wait(.1)
			toposition(CFrame.new(28672.720703125, 14889.1279296875, 454.5961608886719))
			wait(4)
			TweenButton = false
		end)
		
		Main:Button("Teleport Human Door \n ไปประตูทำเผ่ามนุษย์",function(t)
			TweenButton = true
			wait(.1)
			toposition(CFrame.new(29237.294921875, 14889.4267578125, -206.94955444335938))
			wait(4)
			TweenButton = false
		end)
		
		Main:Button("Teleport Mink Door \n ไปประตูทำเผ่ามิ้ง",function(t)
			TweenButton = true
			wait(.1)
			toposition(CFrame.new(29020.66015625, 14889.4267578125, -379.2682800292969))
			wait(4)
			TweenButton = false
		end)
		
		Main:Button("Teleport Sky Door \n ไป}ประตูทำเผ่าสกาย",function(t)
			TweenButton = true
			wait(.1)
			toposition(CFrame.new(28967.408203125, 14918.0751953125, 234.31198120117188))
			wait(4)
			TweenButton = false
		end)
		
		Main:Label("Mini Yoru")
	
		Main:Dropdown("Select Mods God's Chalice \n เลือกโหมด หา ถ้วย",{"TP Chest","Tween Chest","Elite Hunter","Elite Hunter Hop"},"",function(v)
			_G.Select_Mods_Chalice = v
		end)
		 
		Main:Toggle("Auto God's Chalice \n ออโต้ หา ถ้วย",false,function(v)
			_G.Auto_Bowl = v
			StopTween(_G.Auto_Bowl)
		end)
	
		spawn(function()
			while wait() do
				pcall(function()
					if _G.Auto_Bowl then
						if not game.Players.LocalPlayer.Backpack:FindFirstChild("God's Chalice") or not game.Players.LocalPlayer.Character:FindFirstChild("God's Chalice") then
							if _G.Select_Mods_Chalice == "TP Chest" then
								for i,v in pairs(game:GetService("Workspace"):GetChildren()) do 
									if v.Name:find("Chest") then
										if game:GetService("Workspace"):FindFirstChild(v.Name) then
											--if (v.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 9000+_G.Po then
												repeat wait()
													if game:GetService("Workspace"):FindFirstChild(v.Name) then
														game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.CFrame
														wait(0.2)
														game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.CFrame * CFrame.new(0,5,0)
													end
												until _G.Auto_Bowl == false or not v.Parent
												_G.Po = _G.Po + 1000
												break
											--end
										end
									end
								end
							elseif _G.Select_Mods_Chalice == "Tween Chest" then
								for i,v in pairs(game:GetService("Workspace"):GetChildren()) do 
									if v.Name:find("Chest") then
										if game:GetService("Workspace"):FindFirstChild(v.Name) then
											repeat wait()
												if game:GetService("Workspace"):FindFirstChild(v.Name) then
													toposition(v.CFrame)
													wait(0.2)
													toposition(v.CFrame * CFrame.new(0,5,0))
												end
											until _G.Auto_Bowl  == false or not v.Parent
											break
										end
									end
								end
							elseif _G.Select_Mods_Chalice == "Elite Hunter" then
								if game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == true then
									if string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Diablo") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Deandre") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Urban") then
										if game:GetService("Workspace").Enemies:FindFirstChild("Diablo [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Deandre [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Urban [Lv. 1750]") then
											for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
												if v.Name == "Diablo [Lv. 1750]" or v.Name == "Deandre [Lv. 1750]" or v.Name == "Urban [Lv. 1750]" then
													if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
														repeat wait()
															EquipWeapon(_G.Select_Weapon)
															v.HumanoidRootPart.CanCollide = false
															v.Humanoid.WalkSpeed = 0
															v.HumanoidRootPart.Size = Vector3.new(50,50,50)
															if AttackRandomType == 1 then
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
															elseif AttackRandomType == 2 then
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
															elseif AttackRandomType == 3 then
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
															else
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
															end
															if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
																game:GetService("VirtualUser"):CaptureController()
																game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
															end
															sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
														until _G.Auto_Bowl == false or v.Humanoid.Health <= 0 or not v.Parent
													end
												end
											end
										else
											if game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]") then
												toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
											elseif game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]") then
												toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
											elseif game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]") then
												toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
											end
										end                    
									end
								else
									if game.Players.LocalPlayer.Backpack:FindFirstChild("God's Chalice") or game.Players.LocalPlayer.Character:FindFirstChild("God's Chalice") then
										wait(.1)
									else
										if game:GetService("ReplicatedStorage").Remotes["CommF_"]:InvokeServer("EliteHunter") == "I don't have anything for you right now. Come back later." then
											xwd = game:GetService("MarketplaceService"):GetProductInfo(game.PlaceId)
											Gamename = xwd.Name
											local games = {
											[game.PlaceId] = {
												Title = "Blox Fruits",
												Icons = "rbxassetid://9295892168",
												Welcome = function()
													return tostring("Not Have Quest Elite Hunter")
												end
											}
											}
											if games[game.PlaceId] then
												if games[game.PlaceId].Title == "Blox Fruits" then
													local function notify(types, ...)
														if types == "Notify" then
															require(game.ReplicatedStorage.Notification).new(...):Display()
														end
													end
													notify("Notify", games[game.PlaceId].Welcome())
												end
											end
											wait(20)
										else
											game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EliteHunter")
										end
									end
								end
							elseif _G.Select_Mods_Chalice == "Elite Hunter Hop" then
								if game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Visible == true then
									if string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Diablo") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Deandre") or string.find(game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest.Container.QuestTitle.Title.Text,"Urban") then
										if game:GetService("Workspace").Enemies:FindFirstChild("Diablo [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Deandre [Lv. 1750]") or game:GetService("Workspace").Enemies:FindFirstChild("Urban [Lv. 1750]") then
											for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
												if v.Name == "Diablo [Lv. 1750]" or v.Name == "Deandre [Lv. 1750]" or v.Name == "Urban [Lv. 1750]" then
													if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
														repeat wait()
															EquipWeapon(_G.Select_Weapon)
															v.HumanoidRootPart.CanCollide = false
															v.Humanoid.WalkSpeed = 0
															v.HumanoidRootPart.Size = Vector3.new(50,50,50)
															if AttackRandomType == 1 then
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
															elseif AttackRandomType == 2 then
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
															elseif AttackRandomType == 3 then
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
															else
																toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
															end
															if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
																game:GetService("VirtualUser"):CaptureController()
																game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
															end
															sethiddenproperty(game:GetService("Players").LocalPlayer,"SimulationRadius",math.huge)
														until _G.Auto_Bowl == false or v.Humanoid.Health <= 0 or not v.Parent
													end
												end
											end
										else
											if game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]") then
												toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Diablo [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
											elseif game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]") then
												toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Deandre [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
											elseif game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]") then
												toposition(game:GetService("ReplicatedStorage"):FindFirstChild("Urban [Lv. 1750]").HumanoidRootPart.CFrame * CFrame.new(0,30,0))
											end
										end                    
									end
								else
									if game.Players.LocalPlayer.Backpack:FindFirstChild("God's Chalice") or game.Players.LocalPlayer.Character:FindFirstChild("God's Chalice") then
										wait()
									else
										if game:GetService("ReplicatedStorage").Remotes["CommF_"]:InvokeServer("EliteHunter") == "I don't have anything for you right now. Come back later." then
											Hop()
										else
											game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("EliteHunter")
										end
									end
								end
							end
						else
							xwd = game:GetService("MarketplaceService"):GetProductInfo(game.PlaceId)
							Gamename = xwd.Name
							local games = {
							[game.PlaceId] = {
								Title = "Blox Fruits",
								Icons = "rbxassetid://9295892168",
								Welcome = function()
									return tostring("You Have God's Chalice ... 👽👽")
								end
							}
							}
							if games[game.PlaceId] then
								if games[game.PlaceId].Title == "Blox Fruits" then
									local function notify(types, ...)
										if types == "Notify" then
											require(game.ReplicatedStorage.Notification).new(...):Display()
										end
									end
									notify("Notify", games[game.PlaceId].Welcome())
								end
							end
							wait(20)
						end
					end
				end)
			end
		end)
	
		Main:Toggle("Auto Mini Yoru \n โยรุ เล็ก",_G.Settings.Main.Auto_Mini_Yoru,function(value)
			_G.Auto_Mini_Yoru = value
			_G.Settings.Main.Auto_Mini_Yoru = value
			StopTween(_G.Auto_Mini_Yoru)
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					if _G.Auto_Mini_Yoru then
						if game:GetService("Workspace").Enemies:FindFirstChild("rip_indra True Form [Lv. 5000] [Raid Boss]") then
							for i,v in pairs(game:GetService("Workspace").Enemies:GetChildren()) do
								if v.Name == "rip_indra True Form [Lv. 5000] [Raid Boss]" then
									if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
										repeat wait()
											PosMon = v.HumanoidRootPart.CFrame
											EquipWeapon(_G.Select_Weapon)
											v.HumanoidRootPart.Size = Vector3.new(60,60,60)
											v.HumanoidRootPart.Transparency = 1
											v.Humanoid.JumpPower = 0
											v.Humanoid.WalkSpeed = 0
											v.HumanoidRootPart.CanCollide = false
											BringMobFarm = true
											v.Humanoid:ChangeState(11)
											if AttackRandomType == 1 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											elseif AttackRandomType == 2 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(40, _G.Settings.Main.Distance, 0))
											elseif AttackRandomType == 3 then
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
											else
												toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
											end
											if not _G.FastAttack or not _G.FastAttackO or _G.FastAttack or _G.FastAttackO or _G.SuperFastAttack then
												game:GetService("VirtualUser"):CaptureController()
												game:GetService("VirtualUser"):Button1Down(Vector2.new(1280,672))
											end
										until not _G.Auto_Mini_Yoru or v.Humanoid.Health <= 0 or not v.Parent or v.Humanoid.Health <= 0
										BringMobFarm = false
									end
								else
									toposition(CFrame.new(-5010.3125, 314.54129, -3008.48853, -0.297083586, 3.85100698e-08, 0.954851508, 8.86461677e-08, 1, -1.27504105e-08, -0.954851508, 8.08559903e-08, -0.297083586))
								end
							end
						end
					end
				end)
			end
		end)
	end
		
		local tab2 = Win:CraftTab('Stats/TP',7040410130)
		local page4 = tab2:CraftPage('Stats',1)
		
		page4:Label('Stats')
	
		page4:MultiDropdown("Select Stats | เลือกสแตท",{"Melee","Defense","Sword","Gun","Demon Fruit"},{""},function(v)
			_G.Settings.Main.Select_Stats = v
		end)
		
		page4:Toggle('Auto Stats \n อัพสเต็ท',_G.Settings.Main.AutoStats,function(a)
			_G.AutoStats = a
			_G.Settings.Main.AutoStats = a
			SaveSettings()
		end)
	
		spawn(function()
			while wait() do
				pcall(function()
					if _G.AutoStats then
						for _,g in next, _G.Settings.Main.Select_Stats do
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("AddPoint",tostring(g),_G.Settings.Main.PointStats)
						end
					end
				end)
			end
		end)
		
		page4:Slider("Select point \n เลือกจำนวนพ้อย",true,0,100,_G.Settings.Main.PointStats,function(value)
			_G.Settings.Main.PointStats = value
			print(value)
			SaveSettings()
		end)
		
		local page5 = tab2:CraftPage('TP',2)
		
		page5:Label('The World')
		
		page5:Button('Teleport To Old World \n ไปโลก1',function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelMain")
		end)
		
		page5:Button('Teleport To Second Sea \n ไปโลก2',function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelDressrosa")
		end)
		
		page5:Button('Teleport To Third Sea \n ไปโลก3',function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("TravelZou")
		end)
		
		page5:Label('The Island')
		
		if W1 then
		IW1 = {
			"WindMill",
			"Marine",
			"Middle Town",
			"Jungle",
			"Pirate Village",
			"Desert",
			"Snow Island",
			"MarineFord",
			"Colosseum",
			"Sky Island 1",
			"Sky Island 2",
			"Sky Island 3",
			"Prison",
			"Magma Village",
			"Under Water Island",
			"Fountain City",
			"Shank Room",
			"Mob Island"
		}
		
		page5:Dropdown("Select Tp \n เลือกเกาะ",IW1,_G.Settings.Main.SelectIsland,function(a)
			SelectIsland = a
			_G.Settings.Main.SelectIsland = a
			SaveSettings()
		end)
		
		elseif W2 then
		LW2 = {
			"The Cafe",
			"Frist Spot",
			"Dark Area",
			"Flamingo Mansion",
			"Flamingo Room",
			"Green Zone",
			"Factory",
			"Colossuim",
			"Zombie Island",
			"Two Snow Mountain",
			"Punk Hazard",
			"Cursed Ship",
			"Ice Castle",
			"Forgotten Island",
			"Ussop Island",
			"Mini Sky Island"
		}
		
		page5:Dropdown("Select Tp \n เลือกเกาะ",LW2,_G.Settings.Main.SelectIsland,function(a)
			SelectIsland = a
			_G.Settings.Main.SelectIsland = a
			SaveSettings()
		end)
		
		elseif W3 then
		LW3 = {
			"Mansion",
			"Port Town",
			"Great Tree",
			"Castle On The Sea",
			"MiniSky", 
			"Hydra Island",
			"Floating Turtle",
			"Haunted Castle",
			"Ice Cream Island",
			"Peanut Island",
			"Cake Island",
			"Chocolate Island",
			"Cake2 Island [New☃️]"
		}
		page5:Dropdown("Select Tp \n เลือกเกาะ",LW3,_G.Settings.Main.SelectIsland,function(a)
			SelectIsland = a
			_G.Settings.Main.SelectIsland = a
			SaveSettings()
		end)
		end
		
		page5:Toggle('Stast Tween \n เริ่มไปเกาะ',_G.Settings.Main.TeleportIsland,function(a)
			_G.TeleportIsland = a
			_G.Settings.Main.TeleportIsland = a
			if _G.TeleportIsland == true then
			while _G.TeleportIsland do wait()
				repeat wait()
					if SelectIsland == "WindMill" then
						toposition(CFrame.new(979.79895019531, 16.516613006592, 1429.0466308594))
					elseif SelectIsland == "Marine" then
						toposition(CFrame.new(-2566.4296875, 6.8556680679321, 2045.2561035156))
					elseif SelectIsland == "Middle Town" then
						toposition(CFrame.new(-690.33081054688, 15.09425163269, 1582.2380371094))
					elseif SelectIsland == "Jungle" then
						toposition(CFrame.new(-1612.7957763672, 36.852081298828, 149.12843322754))
					elseif SelectIsland == "Pirate Village" then
						toposition(CFrame.new(-1181.3093261719, 4.7514905929565, 3803.5456542969))
					elseif SelectIsland == "Desert" then
						toposition(CFrame.new(944.15789794922, 20.919729232788, 4373.3002929688))
					elseif SelectIsland == "Snow Island" then
						toposition(CFrame.new(1347.8067626953, 104.66806030273, -1319.7370605469))
					elseif SelectIsland == "MarineFord" then
						toposition(CFrame.new(-4914.8212890625, 50.963626861572, 4281.0278320313))
					elseif SelectIsland == "Colosseum" then
						toposition( CFrame.new(-1427.6203613281, 7.2881078720093, -2792.7722167969))
					elseif SelectIsland == "Sky Island 1" then
						toposition(CFrame.new(-4869.1025390625, 733.46051025391, -2667.0180664063))
					elseif SelectIsland == "Sky Island 2" then  
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-4607.82275, 872.54248, -1667.55688))
					elseif SelectIsland == "Sky Island 3" then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-7894.6176757813, 5547.1416015625, -380.29119873047))
					elseif SelectIsland == "Prison" then
						toposition( CFrame.new(4875.330078125, 5.6519818305969, 734.85021972656))
					elseif SelectIsland == "Magma Village" then
						toposition(CFrame.new(-5247.7163085938, 12.883934020996, 8504.96875))
					elseif SelectIsland == "Under Water Island" then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(61163.8515625, 11.6796875, 1819.7841796875))
					elseif SelectIsland == "Fountain City" then
						toposition(CFrame.new(5127.1284179688, 59.501365661621, 4105.4458007813))
					elseif SelectIsland == "Shank Room" then
						toposition(CFrame.new(-1442.16553, 29.8788261, -28.3547478))
						wait(.1)
						toposition(CFrame.new(-1442.16553, 29.8788261, -28.3547478))
					elseif SelectIsland == "Mob Island" then
						toposition(CFrame.new(-2850.20068, 7.39224768, 5354.99268))
					elseif SelectIsland == "The Cafe" then
						toposition(CFrame.new(-380.47927856445, 77.220390319824, 255.82550048828))
					elseif SelectIsland == "Frist Spot" then
						toposition(CFrame.new(-11.311455726624, 29.276733398438, 2771.5224609375))
					elseif SelectIsland == "Dark Area" then
						toposition(CFrame.new(3780.0302734375, 22.652164459229, -3498.5859375))
					elseif SelectIsland == "Flamingo Mansion" then
						toposition(CFrame.new(-483.73370361328, 332.0383605957, 595.32708740234))
					elseif SelectIsland == "Flamingo Room" then
						toposition(CFrame.new(2284.4140625, 15.152037620544, 875.72534179688))
					elseif SelectIsland == "Green Zone" then
						toposition( CFrame.new(-2448.5300292969, 73.016105651855, -3210.6306152344))
					elseif SelectIsland == "Factory" then
						toposition(CFrame.new(424.12698364258, 211.16171264648, -427.54049682617))
					elseif SelectIsland == "Colossuim" then
						toposition( CFrame.new(-1503.6224365234, 219.7956237793, 1369.3101806641))
					elseif SelectIsland == "Zombie Island" then
						toposition(CFrame.new(-5622.033203125, 492.19604492188, -781.78552246094))
					elseif SelectIsland == "Two Snow Mountain" then
						toposition(CFrame.new(753.14288330078, 408.23559570313, -5274.6147460938))
					elseif SelectIsland == "Punk Hazard" then
						toposition(CFrame.new(-6127.654296875, 15.951762199402, -5040.2861328125))
					elseif SelectIsland == "Cursed Ship" then
						toposition(CFrame.new(923.40197753906, 125.05712890625, 32885.875))
					elseif SelectIsland == "Ice Castle" then
						toposition(CFrame.new(6148.4116210938, 294.38687133789, -6741.1166992188))
					elseif SelectIsland == "Forgotten Island" then
						toposition(CFrame.new(-3032.7641601563, 317.89672851563, -10075.373046875))
					elseif SelectIsland == "Ussop Island" then
						toposition(CFrame.new(4816.8618164063, 8.4599885940552, 2863.8195800781))
					elseif SelectIsland == "Mini Sky Island" then
						toposition(CFrame.new(-288.74060058594, 49326.31640625, -35248.59375))
					elseif SelectIsland == "Great Tree" then
						toposition(CFrame.new(2681.2736816406, 1682.8092041016, -7190.9853515625))
					elseif SelectIsland == "Castle On The Sea" then
						toposition(CFrame.new(-5074.45556640625, 314.5155334472656, -2991.054443359375))
					elseif SelectIsland == "MiniSky" then
						toposition(CFrame.new(-260.65557861328, 49325.8046875, -35253.5703125))
					elseif SelectIsland == "Port Town" then
						toposition(CFrame.new(-290.7376708984375, 6.729952812194824, 5343.5537109375))
					elseif SelectIsland == "Hydra Island" then
						toposition(CFrame.new(5228.8842773438, 604.23400878906, 345.0400390625))
					elseif SelectIsland == "Floating Turtle" then
						toposition(CFrame.new(-13274.528320313, 531.82073974609, -7579.22265625))
					elseif SelectIsland == "Mansion" then
						game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-12471.169921875, 374.94024658203, -7551.677734375))
					elseif SelectIsland == "Haunted Castle" then
						toposition(CFrame.new(-9515.3720703125, 164.00624084473, 5786.0610351562))
					elseif SelectIsland == "Ice Cream Island" then
						toposition(CFrame.new(-902.56817626953, 79.93204498291, -10988.84765625))
					elseif SelectIsland == "Peanut Island" then
						toposition(CFrame.new(-2062.7475585938, 50.473892211914, -10232.568359375))
					elseif SelectIsland == "Cake Island" then
						toposition(CFrame.new(-1884.7747802734375, 19.327526092529297, -11666.8974609375))
					 elseif SelectIsland == "Chocolate Island" then
						toposition(CFrame.new(53.0000267, 58.6640739, -12851.6777, -0.203585744, 0, 0.979057133, 0, 1, 0, -0.979057133, 0, -0.203585744))
					  elseif SelectIsland == "Cake2 Island [New☃️]" then
						toposition(CFrame.new(-852.233948, 90.9719009, -14727.04, -0.480660468, -1.84332318e-08, 0.876906812, -5.88126792e-09, 1, 1.77970332e-08, -0.876906812, 3.39700601e-09, -0.480660468))
					end
				until not _G.TeleportIsland
			end
			StopTween(_G.TeleportIsland)
			SaveSettings()
		 end
		end)
		
		page5:Button('BypassTP',function()
			_G.Ummm = nil
			while wait() do
				if SelectIsland == "WindMill" then
					Bypass(CFrame.new(979.79895019531, 16.516613006592, 1429.0466308594),_G.Ummm)
				elseif SelectIsland == "Marine" then
					Bypass(CFrame.new(-2566.4296875, 6.8556680679321, 2045.2561035156),_G.Ummm)
				elseif SelectIsland == "Middle Town" then
					Bypass(CFrame.new(-690.33081054688, 15.09425163269, 1582.2380371094),_G.Ummm)
				elseif SelectIsland == "Jungle" then
					Bypass(CFrame.new(-1612.7957763672, 36.852081298828, 149.12843322754),_G.Ummm)
				elseif SelectIsland == "Pirate Village" then
					Bypass(CFrame.new(-1181.3093261719, 4.7514905929565, 3803.5456542969),_G.Ummm)
				elseif SelectIsland == "Desert" then
					Bypass(CFrame.new(944.15789794922, 20.919729232788, 4373.3002929688),_G.Ummm)
				elseif SelectIsland == "Snow Island" then
					Bypass(CFrame.new(1347.8067626953, 104.66806030273, -1319.7370605469),_G.Ummm)
				elseif SelectIsland == "MarineFord" then
					Bypass(CFrame.new(-4914.8212890625, 50.963626861572, 4281.0278320313),_G.Ummm)
				elseif SelectIsland == "Colosseum" then
					Bypass( CFrame.new(-1427.6203613281, 7.2881078720093, -2792.7722167969),_G.Ummm)
				elseif SelectIsland == "Sky Island 1" then
					Bypass(CFrame.new(-4869.1025390625, 733.46051025391, -2667.0180664063),_G.Ummm)
				elseif SelectIsland == "Sky Island 2" then  
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-4607.82275, 872.54248, -1667.55688))
				elseif SelectIsland == "Sky Island 3" then
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-7894.6176757813, 5547.1416015625, -380.29119873047))
				elseif SelectIsland == "Prison" then
					Bypass( CFrame.new(4875.330078125, 5.6519818305969, 734.85021972656),_G.Ummm)
				elseif SelectIsland == "Magma Village" then
					Bypass(CFrame.new(-5247.7163085938, 12.883934020996, 8504.96875),_G.Ummm)
				elseif SelectIsland == "Under Water Island" then
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(61163.8515625, 11.6796875, 1819.7841796875))
				elseif SelectIsland == "Fountain City" then
					Bypass(CFrame.new(5127.1284179688, 59.501365661621, 4105.4458007813),_G.Ummm)
				elseif SelectIsland == "Shank Room" then
					Bypass(CFrame.new(-1442.16553, 29.8788261, -28.3547478),_G.Ummm)
					wait(.1)
					Bypass(CFrame.new(-1442.16553, 29.8788261, -28.3547478),_G.Ummm)
				elseif SelectIsland == "Mob Island" then
					Bypass(CFrame.new(-2850.20068, 7.39224768, 5354.99268),_G.Ummm)
				elseif SelectIsland == "The Cafe" then
					Bypass(CFrame.new(-380.47927856445, 77.220390319824, 255.82550048828),_G.Ummm)
				elseif SelectIsland == "Frist Spot" then
					Bypass(CFrame.new(-11.311455726624, 29.276733398438, 2771.5224609375),_G.Ummm)
				elseif SelectIsland == "Dark Area" then
					Bypass(CFrame.new(3780.0302734375, 22.652164459229, -3498.5859375),_G.Ummm)
				elseif SelectIsland == "Flamingo Mansion" then
					Bypass(CFrame.new(-483.73370361328, 332.0383605957, 595.32708740234),_G.Ummm)
				elseif SelectIsland == "Flamingo Room" then
					Bypass(CFrame.new(2284.4140625, 15.152037620544, 875.72534179688),_G.Ummm)
				elseif SelectIsland == "Green Zone" then
					Bypass( CFrame.new(-2448.5300292969, 73.016105651855, -3210.6306152344),_G.Ummm)
				elseif SelectIsland == "Factory" then
					Bypass(CFrame.new(424.12698364258, 211.16171264648, -427.54049682617),_G.Ummm)
				elseif SelectIsland == "Colossuim" then
					Bypass( CFrame.new(-1503.6224365234, 219.7956237793, 1369.3101806641),_G.Ummm)
				elseif SelectIsland == "Zombie Island" then
					Bypass(CFrame.new(-5622.033203125, 492.19604492188, -781.78552246094),_G.Ummm)
				elseif SelectIsland == "Two Snow Mountain" then
					Bypass(CFrame.new(753.14288330078, 408.23559570313, -5274.6147460938),_G.Ummm)
				elseif SelectIsland == "Punk Hazard" then
					Bypass(CFrame.new(-6127.654296875, 15.951762199402, -5040.2861328125),_G.Ummm)
				elseif SelectIsland == "Cursed Ship" then
					Bypass(CFrame.new(923.40197753906, 125.05712890625, 32885.875),_G.Ummm)
				elseif SelectIsland == "Ice Castle" then
					Bypass(CFrame.new(6148.4116210938, 294.38687133789, -6741.1166992188),_G.Ummm)
				elseif SelectIsland == "Forgotten Island" then
					Bypass(CFrame.new(-3032.7641601563, 317.89672851563, -10075.373046875),_G.Ummm)
				elseif SelectIsland == "Ussop Island" then
					Bypass(CFrame.new(4816.8618164063, 8.4599885940552, 2863.8195800781),_G.Ummm)
				elseif SelectIsland == "Mini Sky Island" then
					Bypass(CFrame.new(-288.74060058594, 49326.31640625, -35248.59375),_G.Ummm)
				elseif SelectIsland == "Great Tree" then
					Bypass(CFrame.new(2681.2736816406, 1682.8092041016, -7190.9853515625),_G.Ummm)
				elseif SelectIsland == "Castle On The Sea" then
					Bypass(CFrame.new(-5074.45556640625, 314.5155334472656, -2991.054443359375),_G.Ummm)
				elseif SelectIsland == "MiniSky" then
					Bypass(CFrame.new(-260.65557861328, 49325.8046875, -35253.5703125),_G.Ummm)
				elseif SelectIsland == "Port Town" then
					Bypass(CFrame.new(-290.7376708984375, 6.729952812194824, 5343.5537109375),_G.Ummm)
				elseif SelectIsland == "Hydra Island" then
					Bypass(CFrame.new(5228.8842773438, 604.23400878906, 345.0400390625),_G.Ummm)
				elseif SelectIsland == "Floating Turtle" then
					Bypass(CFrame.new(-13274.528320313, 531.82073974609, -7579.22265625),_G.Ummm)
				elseif SelectIsland == "Mansion" then
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("requestEntrance",Vector3.new(-12471.169921875, 374.94024658203, -7551.677734375))
				elseif SelectIsland == "Haunted Castle" then
					Bypass(CFrame.new(-9515.3720703125, 164.00624084473, 5786.0610351562),_G.Ummm)
				elseif SelectIsland == "Ice Cream Island" then
					Bypass(CFrame.new(-902.56817626953, 79.93204498291, -10988.84765625),_G.Ummm)
				elseif SelectIsland == "Peanut Island" then
					Bypass(CFrame.new(-2062.7475585938, 50.473892211914, -10232.568359375),_G.Ummm)
				elseif SelectIsland == "Cake Island" then
					Bypass(CFrame.new(-1884.7747802734375, 19.327526092529297, -11666.8974609375),_G.Ummm)
				elseif SelectIsland == "Chocolate Island" then
					Bypass(CFrame.new(53.0000267, 58.6640739, -12851.6777, -0.203585744, 0, 0.979057133, 0, 1, 0, -0.979057133, 0, -0.203585744),_G.Ummm)
				elseif SelectIsland == "Cake2 Island [New☃️]" then
					Bypass(CFrame.new(-852.233948, 90.9719009, -14727.04, -0.480660468, -1.84332318e-08, 0.876906812, -5.88126792e-09, 1, 1.77970332e-08, -0.876906812, 3.39700601e-09, -0.480660468),_G.Ummm)
				end
			end
		end)
		
		local tab3 = Win:CraftTab('Dungeon',9606629300)
		local page6 = tab3:CraftPage('Dungeon 1/2',1)
		
		if W1 then
		page6:Label("World 1 not working")
		elseif W2 or W3 then
		
		page6:Toggle('Kill Aura \n ฆ่ามอนรอบๆ',_G.Settings.Main.KillAura,function(a)
			_G.KillAura = a
			_G.Settings.Main.KillAura = a
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.KillAura then
					for i,v in pairs(game.Workspace.Enemies:GetDescendants()) do
						if v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
							pcall(function()
								repeat wait()
									v.Humanoid.Health = 0
									v.HumanoidRootPart.CanCollide = false
									v.HumanoidRootPart.Size = Vector3.new(50,50,50)
									v.HumanoidRootPart.Transparency = 1
								until not _G.KillAura or not v.Parent or v.Humanoid.Health <= 0
							end)
						end
					end
				end
			end
		end)
		
		page6:Toggle('Next Islands \n ออโต้ย้ายเกาะ',_G.Settings.Main.Next_Islands,function(a)
			_G.Next_Islands = a
			_G.Settings.Main.Next_Islands = a
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				if _G.Next_Islands then
					if not game.Players.LocalPlayer.PlayerGui.Main.Timer.Visible == false then
						if game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 5") then
							getgenv().TP(game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 5").CFrame * CFrame.new(0,70,100))
						elseif game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 4") then
							getgenv().TP(game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 4").CFrame * CFrame.new(0,70,100))
						elseif game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 3") then
							getgenv().TP(game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 3").CFrame * CFrame.new(0,70,100))
						elseif game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 2") then
							getgenv().TP(game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 2").CFrame * CFrame.new(0,70,100))
						elseif game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 1") then
							getgenv().TP(game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island 1").CFrame * CFrame.new(0,70,100))
						end
					end
				end
			end
		end)
		
		page6:Toggle('Auto Awaken \n ออโต้ทำผลตื่น',_G.Settings.Main.Auto_Awaken,function(a)
			_G.Settings.Main.Auto_Awaken = a
			SaveSettings()
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					if _G.Settings.Main.Auto_Awaken then
						if game.Players.LocalPlayer.PlayerGui.Main.Timer.Visible == false then
							game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Awakener","Awaken")
						end
					end
				end)
			end
		end)
		
		page6:Toggle('Auto Start Raid \n ออโต้ไปดัน',_G.Settings.Main.AutoSartRaid,function(a)
			_G.Settings.Main.AutoSartRaid = a
			SaveSettings()
		end)
		
		spawn(function()
			while wait(2) do
				if _G.Settings.Main.AutoSartRaid then
					if game:GetService("Players")["LocalPlayer"].PlayerGui.Main.Timer.Visible == false then
						if W2 then
							fireclickdetector(game:GetService("Workspace").Map.CircleIsland.RaidSummon2.Button.Main.ClickDetector)
							wait(15)
						elseif W3 then
							fireclickdetector(game:GetService("Workspace").Map["Boat Castle"].RaidSummon2.Button.Main.ClickDetector)
							wait(15)
						end
					end
				end
			end
		end)
		spawn(function()
			while wait(2) do
				pcall(function()
					if _G.Settings.Main.AutoSartRaid then
						if game:GetService("Players")["LocalPlayer"].PlayerGui.Main.Timer.Visible == false then
							_G.Stop_Tween = true
						elseif game:GetService("Players")["LocalPlayer"].PlayerGui.Main.Timer.Visible == true then
							_G.Stop_Tween = false
						end
					end
				end)
			end
		end)
		
		page6:Button("Start Raid \n ไปดัน",function()
		if game:GetService("Players")["LocalPlayer"].PlayerGui.Main.Timer.Visible == false then
			if W2 then
				fireclickdetector(game:GetService("Workspace").Map.CircleIsland.RaidSummon2.Button.Main.ClickDetector)
				wait(15)
			elseif W3 then
				fireclickdetector(game:GetService("Workspace").Map["Boat Castle"].RaidSummon2.Button.Main.ClickDetector)
				wait(15)
			end
		end
		end)
		
		end
		local page7 = tab3:CraftPage('Dungeon 2/2',2)
		if W1 then
		page7:Label("World 1 not working")
		elseif W2 or W3 then
		
		Chips = {
			"Dark",
			"Sand",
			"Magma",
			"Rumble",
			"Flame",
			"Ice",
			"Light",
			"Quake",
			"Human: Buddha",
			"Flame",
			"Bird: Phoenix",
			"Dough"
		}
		
		page7:Dropdown("Select Buy Chips \n เลือกซื้อชิพ",Chips,_G.Settings.Main.Chips,function(a)
			_G.Settings.Main.Chips = a
			SaveSettings()
		end)
		
		function BuyChips()
			local args = {
				[1] = "RaidsNpc",
				[2] = "Select",
				[3] = tostring("Light")
			}
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
		end
		
		page7:Button("Buy Chips \n ซื้อชิพ",function()
			BuyChips()
		end)
		
		page7:Toggle('Auto Buy Chips \n ออโต้ซื้อชิพ',_G.Settings.Main.AutoBuyChips,function(a)
			_G.Settings.Main.AutoBuyChips = a
			while _G.Settings.Main.AutoBuyChips do wait()
				BuyChips()
			end
			SaveSettings()
		end)
		
		page7:Button("Random Fruit \n สุ่มผล",function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Cousin","Buy")
		end)
		end
		
		local tab4 = Win:CraftTab('Shop',7294901968)
		local page8 = tab4:CraftPage('Shop 1/2',1)
		page8:Label("Shop")
		
		page8:Dropdown("Select Buy Fighting \n เลือกซื้อหมัด",{"Black Leg","Electro","Fishman Karate","Dragon Claw","Superhuman","Death Step","Electric Claw","Sharkman Karate","Dragon Talon","Godhuman"},_G.Settings.Main.Select_Buy,function(vu)
			_G.Select_Buy = vu
			_G.Settings.Main.Select_Buy = vu
			SaveSettings()
		end)
		
		page8:Button("Buy \n ซื้อ", function()
			if _G.Select_Buy == "Black Leg" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyBlackLeg")
			elseif _G.Select_Buy == "Electro" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectro")
			elseif _G.Select_Buy == "Fishman Karate" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyFishmanKarate")
			elseif _G.Select_Buy == "Dragon Claw" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","1")
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","DragonClaw","2")
			elseif _G.Select_Buy == "Superhuman" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuySuperhuman")
			elseif _G.Select_Buy == "Death Step" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyDeathStep")
			elseif _G.Select_Buy == "Electric Claw" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyElectricClaw")
			elseif _G.Select_Buy == "Sharkman Karate" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyFishmanKarate")
			elseif _G.Select_Buy == "Dragon Talon" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyDragonTalon")
			elseif _G.Select_Buy == "Godhuman" then
				game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyGodhuman")
			end
		end)
		
		page8:Label("Fragment")
		
		page8:Button("Refund Stat \n รีแสตท", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","Refund","1")
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","Refund","2")
		end)
		
		page8:Button("Refund Race \n รีเผ่า", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","Reroll","1")
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","Reroll","2")
		end)
		
		page8:Label("Abilities")
		
		page8:Button("Haki \n ฮาคิ", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyHaki","Buso")
		end)
		
		page8:Button("Geppo \n เเกพพอ", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyHaki","Geppo")
		end)
		
		page8:Button("Soru \n โซร์", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyHaki","Soru")
		end)
		
		page8:Button("Ken \n เคน", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("KenTalk","Buy")
		end)
		
		page8:Label("Sword")
		
		page8:Button("Katana \n คาตานะ", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Katana")
		end)
		
		page8:Button("Cutlass \n ดาบโจนสลัด", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Cutlass")
		end)
		
		page8:Button("Duel Katana \n สองดาบปลอม", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Duel Katana")
		end)
		
		page8:Button("Iron Mace \n กะโมง", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Iron Mace")
		end)
		
		page8:Button("Pipe \n ท้อ", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Pipe")
		end)
		
		page8:Button("Triple Katana \n สามดาบปลอม", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Triple Katana")
		end)
		
		page8:Button("Dual-Headed Blade \n ไม่รู้", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Dual-Headed Blade")
		end)
		
		page8:Button("Bisento \n บีเสนโต", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Bisento")
		end)
		
		page8:Button("Soul Cane \n ไม่รู้", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Soul Cane")
		end)
		
		local page9 = tab4:CraftPage('Shop 2/2',2)
		
		page9:Label("Gun")
		
		page9:Button("Buy Slingshot \n หนังกะติ๊ก", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Slingshot")
		end)
		
		page9:Button("Buy Musket \n ปืน", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Musket")
		end)
		
		page9:Button("Buy Flintlock \n ปืน", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Flintlock")
		end)
		
		page9:Button("Buy Refined Flintlock \n ปืน", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Refined Flintlock")
		end)
		
		page9:Button("Buy Cannon \n หนังกะติ๊ก", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Cannon")
		end)
		
		page9:Button("Buy Kabucha \n หนังกะติ๊ก", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","Slingshot","1")
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BlackbeardReward","Slingshot","2")
		end)
		
		page9:Label("Accessory")
		
		page9:Button("Buy Black Cape \n ผ้าคลุมสีดำ", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Black Cape")
		end)
		
		page9:Button("Buy Toemo Ring \n ไม่รู้", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Tomoe Ring")
		end)
		
		page9:Button("Buy Swordsman Hat \n หมวกนักดาบ", function()
			game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("BuyItem","Swordsman Hat")
		end)
		
		
		local Job = game.JobId
		_G.JobID = Job
		local tab6 = Win:CraftTab('Server',44891515)
		local page12 = tab6:CraftPage('Server',1)
		
		page12:Textbox("ID Job","",function(v)
			print(v)
			_G.JobID = v
		end)
		
		page12:Button("TP Job ID", function()
			game.ReplicatedStorage['__ServerBrowser']:InvokeServer('teleport',_G.JobID)
		end)
		
		page12:Button("Copy Job ID", function()
			setclipboard(Job)
		end)
		
		local page13 = tab6:CraftPage('Server',2)
		
		page13:Button("rejoin", function()
			game.ReplicatedStorage['__ServerBrowser']:InvokeServer('teleport',game.JobId)
		end)
		
		page13:Button("Hop", function()
			Hop()
		end)
		
		
		local tab5 = Win:CraftTab('Misc',6022668898)
		local page11 = tab5:CraftPage('Combat',1)
	
		page11:Toggle('Aimbot Player \n ล็อกมุมมองไปหาคน',false,function(state)
	
			local Cam = workspace.CurrentCamera
				
			local hotkey = state
			function lookAt(target, eye)
				Cam.CFrame = CFrame.new(target, eye)
			end
				
			function getClosestPlayerToCursor(trg_part)
				local nearest = nil
				local last = math.huge
				for i,v in pairs(game.Players:GetPlayers()) do
					if v ~= game.Players.LocalPlayer and game.Players.LocalPlayer.Character and v.Character and v.Character:FindFirstChild(trg_part) then
						if game.Players.LocalPlayer.Character:FindFirstChild(trg_part) then
							local ePos, vissss = workspace.CurrentCamera:WorldToViewportPoint(v.Character[trg_part].Position)
							local AccPos = Vector2.new(ePos.x, ePos.y)
							local mousePos = Vector2.new(workspace.CurrentCamera.ViewportSize.x / 2, workspace.CurrentCamera.ViewportSize.y / 2)
							local distance = (AccPos - mousePos).magnitude
							if distance < last and vissss and hotkey == true and distance < 400 then
								last = distance
								nearest = v
							end
						end
					end
				end
				return nearest
			end
		
			game:GetService("RunService").RenderStepped:Connect(function()
				local closest = getClosestPlayerToCursor("Head")
				if state and closest and closest.Character:FindFirstChild("Head") then
					lookAt(Cam.CFrame.p, closest.Character:FindFirstChild("Head").Position)
				end
			end)
		
		end)
		
		
	
		local Players = {}
		for i,v in pairs(game.Players:GetPlayers()) do
			table.insert(Players,v.Name)
		end
		
		local SelectedPly = page11:Dropdown("Selected Player \n เลือกคน",Players,'',function(a)
			_G.PlayerName = a
		end)
		
		page11:Button("Refresh Player \n รีคน",function()
			Players = {}
			SelectedPly:Clear()
			for i,v in pairs(game:GetService("Players"):GetChildren()) do  
				SelectedPly:Add(v.Name)
			end
		end)
		
		spawn(function()
			while wait() do
				pcall(function()
					local MaxDistance = math.huge
					for i,v in pairs(game:GetService("Players"):GetPlayers()) do
						if v.Name ~= game:GetService("Players").LocalPlayer.Name then
							local Distance = v:DistanceFromCharacter(game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position)
							if Distance < MaxDistance then
								MaxDistance = Distance
								PlayerSelectAimbot = v.Name
							end
						end
					end
				end)
			end
		end)
		
		page11:Toggle('Aimbot Skill \n ล็อกคนด้ายสกิว',false,function(state)
			_G.Aimbot_Skill = state
		end)
		
		spawn(function()
			pcall(function()
				while task.wait() do
					if _G.Aimbot_Skill and _G.PlayerName ~= nil and game.Players.LocalPlayer.Character:FindFirstChildOfClass("Tool") and game.Players.LocalPlayer.Character[game.Players.LocalPlayer.Character:FindFirstChildOfClass("Tool").Name]:FindFirstChild("MousePos") then
						game:GetService("Players").LocalPlayer.Character:FindFirstChild(game.Players.LocalPlayer.Character:FindFirstChildOfClass("Tool").Name).RemoteEvent:FireServer(game:GetService("Players"):FindFirstChild(PlayerSelectAimbot).Character.HumanoidRootPart.Position)
					end
				end
			end)
		end)
		
		page11:Toggle('Aimbot Gun \n ล็อกคนด้ายปืน',false,function(state)
			_G.Aimbot_Gun = state
		end)
		
		spawn(function()
			while task.wait() do
				if _G.Aimbot_Gun then
					pcall(function()
						local args = {
							[1] = game:GetService("Players"):FindFirstChild(_G.PlayerName).Character.HumanoidRootPart.Position,
							[2] = game:GetService("Players"):FindFirstChild(_G.PlayerName).Character.HumanoidRootPart
						}
						game:GetService("Players").LocalPlayer.Character[SelectWeaponGun].RemoteFunctionShoot:InvokeServer(unpack(args))
						--game:GetService'VirtualUser':CaptureController()
						--game:GetService'VirtualUser':Button1Down(Vector2.new(1280, 672))
					end)
				end
			end
		end)		
	
		local page10 = tab5:CraftPage('Misc',2)
		
		page10:Toggle('ESP Player \n มองผู้เล่น',false,function(a)
			ESPPlayer = a
			UpdatePlayerChams()
		end)
		
		page10:Toggle('ESP Chest \n มองกล่อง',false,function(a)
			ChestESP = a
			UpdateChestChams() 
		end)
		
		page10:Toggle('ESP Devil Fruit \n มองผลไม้',false,function(a)
			DevilFruitESP = a
			UpdateDevilChams() 
		end)
		
		page10:Toggle('ESP Flower \n มองดอกไม้',false,function(a)
			FlowerESP = a
			UpdateFlowerChams() 
		end)
		
		page10:Label('CupCut')
		
		page10:Button('CupCut \n ตักรูป',function(a)
			_G.Kai = {
			["Check Swords"] = {
				["Enabled Check"] = true,
			},
			["Check Fighting Style"] = {
				["Enabled Check"] = true,
			},
			["Check Awakening Fruits"] = {
				["Enabled Check"] = true,
			},
			["Check Fruits"] = {
				["Enabled Check"] = true,
			},
		}
		
		local function formatNumber(number)
			local i, k, j = tostring(number):match("(%-?%d?)(%d*)(%.?.*)")
			return i..k:reverse():gsub("(%d%d%d)", "%1,"):reverse()..j
		end
		
		local Blur = Instance.new("BlurEffect")
		
		Blur.Size = 0
		Blur.Parent = game.Lighting
		
		local UserInputService = game:GetService("UserInputService")
		local TweenService = game:GetService("TweenService")
		local RunService = game:GetService("RunService")
		local LocalPlayer = game:GetService("Players").LocalPlayer
		local Mouse = LocalPlayer:GetMouse()
		
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Mute.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Settings.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.HomeButton.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.CrewButton.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.AlliesButton.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Compass.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.MenuButton.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Energy.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.HP.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Level.Black.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Level.Bar.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Level.Exp.Visible = false
		
		do  local ui = game:GetService("CoreGui"):FindFirstChild("CheckItem")  if ui then ui:Destroy() end end  
		
		local CheckItem = Instance.new("ScreenGui")
		local Check = Instance.new("Frame")
		local CheckSwordUIListout = Instance.new("UIListLayout")
		
		
		CheckItem.Name = "CheckItem"
		CheckItem.Parent = game:GetService("CoreGui")
		CheckItem.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
		
		Check.Parent = CheckItem
		Check.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		Check.AnchorPoint = Vector2.new(0.5,0.5)
		Check.Position = UDim2.new(0.2, 0, 0.1, 0)
		Check.Size = UDim2.new(0.385997534, 0, 0.186046511, 0)
		Check.BackgroundTransparency = 1
		
		PositionX = 0
		PositionY = 0
		ItemCountRed = 0
		ItemCountPurple = 0
		ItemCountFruits = 0
		
		task.spawn(function()
			pcall(function()
				if _G.Kai["Check Swords"]["Enabled Check"] then
					for o,p in pairs(game:GetService("Players").LocalPlayer.PlayerGui.Main.InventoryContainer.Right.Content.ScrollingFrame.Frame:GetChildren()) do
						if p:IsA("Frame") then
							for i,v in pairs(p:GetDescendants()) do
								if v.Name == "Background" then
									if v.BackgroundColor3 == Color3.fromRGB(238, 47, 50) then
										wait(.05)
										local ItemRed = p:Clone()
										ItemRed.Parent = Check
										ItemRed.Position = UDim2.new(0, 0, 0, 0)
									end
									if v.BackgroundColor3 == Color3.fromRGB(213, 43, 228) then
										wait(.05)
										local ItemPurple = p:Clone()
										ItemPurple.Parent = Check
										ItemPurple.Position = UDim2.new(0, 0, 0, 0)
									end
								end
							end
						end
					end
				end
			end)
		end)
		
		local CheckFruit = Instance.new("Frame")
		
		CheckFruit.Name = "CheckFruit"
		CheckFruit.Parent = CheckItem
		CheckFruit.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		CheckFruit.Position = UDim2.new(0.2, 0, 0.3, 0)
		CheckFruit.AnchorPoint = Vector2.new(0.5,0.5)
		CheckFruit.Size = UDim2.new(0.385997534, 0, 0.186, 0)
		CheckFruit.BackgroundTransparency = 1
		
		PositionXFruit = -0.125
		PositionYFruit = 0
		ItemCountFruit = 0
		CheckFruitSuccess = false
		
		task.spawn(function()
			pcall(function()
				if _G.Kai["Check Fruits"]["Enabled Check"] then
					game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Visible = true
					game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Container.Visible = false
					game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Info.Visible = false
					game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.BackgroundTransparency = 1
					game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Title.Visible = false
					game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Size = UDim2.new(0, 0, 0, 0)
					wait(1)
					for o,p in pairs(game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Container.Stored.ScrollingFrame.Frame:GetChildren()) do
						if game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Visible == true then
							if p:IsA("ImageButton") then
								wait(.05)
								local ItemFruit = p:Clone()
								ItemFruit.Parent = CheckFruit
								ItemFruit.Position = UDim2.new(PositionXFruit, 0, PositionYFruit, 0)
								ItemFruit.Size = UDim2.new(0.12, 0, 0.12, 0)
								PositionXFruit = PositionXFruit + 0.122
								ItemCountFruit = ItemCountFruit + 1
								if ItemCountFruit >= 6 then
									PositionYFruit = PositionYFruit + 0.625
									ItemCountFruit = 0
									PositionXFruit = 0
								end
							end
						else
							game:GetService("Players").LocalPlayer.PlayerGui.Main.FruitInventory.Visible = true
						end
					end
				end
			end)
		end)
		
		local CheckFightingStyle = Instance.new("Frame")
		
		CheckFightingStyle.Name = "CheckFightingStyle"
		CheckFightingStyle.Parent = CheckItem
		CheckFightingStyle.AnchorPoint = Vector2.new(0.5, 0.5)
		CheckFightingStyle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		CheckFightingStyle.Position = UDim2.new(0.5, 0, 0.3, 0)
		CheckFightingStyle.Size = UDim2.new(0.385997534, 0, 0.186046511, 0)
		CheckFightingStyle.BackgroundTransparency = 1
		
		local Name = Instance.new("TextLabel")
		Name.Name = "Name"
		Name.Parent = CheckFightingStyle
		Name.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
		Name.BackgroundTransparency = 1.000
		Name.Size = UDim2.new(0, 85, 0, 35)
		Name.Font = Enum.Font.GothamSemibold
		Name.Text = string.upper(game.Players.LocalPlayer.Name)
		Name.TextColor3 = Color3.fromRGB(255, 255, 255)
		Name.TextSize = 21.000
		Name.TextXAlignment = Enum.TextXAlignment.Left
		
		local args = {
			[1] = "BuyDragonTalon",
			[2] = true
		}
		BuyDragonTalon = tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args)))
		if BuyDragonTalon then
			if BuyDragonTalon == 1 then
				HasTalon = true
			end
		end
		local args = {
			[1] = "BuySuperhuman",
			[2] = true
		}
		BuySuperhuman = tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args)))
		if BuySuperhuman then
			if BuySuperhuman == 1 then
				 HasSuperhuman = true
			end
		end
		-- Script generated by SimpleSpy - credits to exx#9394
		local args = {
			[1] = "BuySharkmanKarate",
			[2] = true
		}
		BuySharkmanKarate = tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args)))
		if BuySharkmanKarate then
			if BuySharkmanKarate == 1 then
				HasKarate = true
			end
		end
		local args = {
			[1] = "BuyDeathStep",
			[2] = true
		}
		BuyDeathStep = tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args)))
		if BuyDeathStep then
			if BuyDeathStep == 1 then
				HasDeathStep = true
			end
		end
		local args = {
			[1] = "BuyElectricClaw",
			[2] = true
		}
		BuyElectricClaw = tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args)))
		if BuyElectricClaw then
			if BuyElectricClaw == 1 then
				HasElectricClaw = true
			end
		end
		local args = {
			[1] = "BuyGodhuman",
			[2] = true
		}
		BuyGodhuman = tonumber(game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args)))
		if BuyGodhuman then
			if BuyGodhuman == 1 then
				HasGodHuman = true
			end
		end
		local data = {}
		if HasElectricClaw then
			table.insert(data, {
				['Name'] = 'ElectricClaw',
				['AssetID'] = '6866994470'
			})
		end
		if HasTalon then
			table.insert(data, {
				['Name'] = 'Dragon Talon',
				['AssetID'] = '7831677967'
			})
		end
		if HasSuperhuman then
			table.insert(data, {
				['Name'] = 'Superhuman',
				['AssetID'] = '4831781711'
			})
		end
		if HasKarate then
			table.insert(data, {
				['Name'] = 'Sharkman Karate',
				['AssetID'] = '6525157144'
			})
		end
		if HasDeathStep then
			table.insert(data, {
				['Name'] = 'Death Step',
				['AssetID'] = '6085350468'
			})
		end
		if HasGodHuman then
			table.insert(data, {
				['Name'] = 'Godhuman',
				['AssetID'] = '10338473987'
			})
		end
		
		PositionXFighting = 0
		
		task.spawn(function()
			pcall(function()
				if _G.Kai["Check Fighting Style"]["Enabled Check"] then
					for i, v in pairs(data) do
						wait(.05)
						local Assets = Instance.new("ImageLabel")
						Assets.Name = v.Name
						Assets.Parent = CheckFightingStyle
						Assets.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
						Assets.BackgroundTransparency = 1
						Assets.BorderSizePixel = 0
						Assets.Size = UDim2.new(0, 100, 0, 100)
						Assets.Image = 'rbxassetid://' .. v.AssetID
						Assets.Position =  UDim2.new(PositionXFighting, 0, 0, 0)
						PositionXFighting =  PositionXFighting + 0.15
					end
				end
			end)
		end)
		
		task.spawn(function()
			pcall(function()
				if _G.Kai["Check Awakening Fruits"]["Enabled Check"] then
					local args = {
						[1] = "getAwakenedAbilities"
					}
		
					game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer(unpack(args))
					game.Players.LocalPlayer.PlayerGui.Main.AwakeningToggler.Visible = true
				end
			end)
		end)
		end)
		
		page10:Button('Remove CupCut \n ลบตักรูป',function(a)
		local CheckItem = game.CoreGui:FindFirstChild("CheckItem")
		local CheckFightingStyle = game.CoreGui:FindFirstChild("CheckFightingStyle")
		local CheckFruit = game.CoreGui:FindFirstChild("CheckFruit")
		local Blur = Instance.new("BlurEffect")
		local LightingFF =  game:GetService("Lighting"):FindFirstChild("Blur")
		
		Blur.Size = 0
		Blur.Parent = game.Lighting
		
		game.Players.LocalPlayer.PlayerGui.Main.AwakeningToggler.Visible = false
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Mute.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Settings.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.HomeButton.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.CrewButton.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.AlliesButton.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Compass.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.MenuButton.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Energy.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.HP.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Level.Black.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Level.Bar.Visible = true
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Level.Exp.Visible = true
		
		Blur.Size = 0
		Blur.Parent = game.Lighting
		
		
		LightingFF:Destroy()
		CheckItem:Destroy()
		CheckFightingStyle:Destroy()
		CheckFruit:Destroy()
		Name:Destroy()
	end)
	--Bartilo Quest
	
	------------------------------------------------------------------------------------------------------------
		
	spawn(function()
		while wait() do
			pcall(function()
				if _G.Mon_Aura then
					for i,v in pairs(game.Workspace.Enemies:GetChildren()) do
						if _G.Mon_Aura and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 and (v.HumanoidRootPart.Position-game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).magnitude <= 1000 then
							repeat wait()
								v.HumanoidRootPart.Size = Vector3.new(60,60,60)
								v.HumanoidRootPart.CanCollide = false
								v.Humanoid.WalkSpeed = 0
								v.Head.CanCollide = false
								BringMobFarm = true
								EquipWeapon(_G.Select_Weapon)
								PosMon = v.HumanoidRootPart.CFrame
								v.HumanoidRootPart.Transparency = 1
								if AttackRandomType == 1 then
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
								elseif AttackRandomType == 2 then
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, -40))
								else
									toposition(v.HumanoidRootPart.CFrame * CFrame.new(0, _G.Settings.Main.Distance, 5))
								end
								game:GetService'VirtualUser':CaptureController()
								game:GetService'VirtualUser':Button1Down(Vector2.new(1280, 672))
							until not _G.Mon_Aura or not v.Parent or v.Humanoid.Health <= 0
							BringMobFarm = false
						end
					end
				end
			end)
		end
	end)
	
	
	spawn(function()
		game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.TextLabel.Try.MouseButton1Click:Connect(function()
			if game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.TextLabel.TextBox.Text == "Boot Fast" then
				wait(0.3)
				game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.TextLabel.TextBox.Text = "Ok Wait"
				game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.TextLabel.TextBox.TextColor3 = Color3.fromRGB(0, 255, 0)
				_G.OpenFast2 = true
				spawn(function()
					while _G.OpenFast2 do task.wait()
					_G.FastAttack = true
					
					require(game.ReplicatedStorage.Util.CameraShaker):Stop()
					xShadowFastAttackx = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework)
					xShadowx = debug.getupvalues(xShadowFastAttackx)[2]
					task.spawn(function()
						while true do task.wait()
							if _G.FastAttack then
								if typeof(xShadowx) == "table" then
									pcall(function()
										xShadowx.activeController.timeToNextAttack = -(math.huge^math.huge^math.huge)
										xShadowx.activeController.timeToNextAttack = 0
										xShadowx.activeController.hitboxMagnitude = 200
										xShadowx.activeController.active = false
										xShadowx.activeController.timeToNextBlock = 0
										xShadowx.activeController.focusStart = 0
										xShadowx.activeController.increment = 4
										xShadowx.activeController.blocking = false
										xShadowx.activeController.attacking = false
										xShadowx.activeController.humanoid.AutoRotate = true
									end)
								end
							end
						end
					end)
					
					local Module = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework)
					local CombatFramework = debug.getupvalues(Module)[2]
					local CameraShakerR = require(game.ReplicatedStorage.Util.CameraShaker)
					
					task.spawn(function()
						while true do task.wait()
							if _G.FastAttack then
								pcall(function()
									CameraShakerR:Stop()
									CombatFramework.activeController.attacking = false
									CombatFramework.activeController.timeToNextAttack = 0
									CombatFramework.activeController.increment = 4
									CombatFramework.activeController.hitboxMagnitude = 100
									CombatFramework.activeController.blocking = false
									CombatFramework.activeController.timeToNextBlock = 0
									CombatFramework.activeController.focusStart = 0
									CombatFramework.activeController.humanoid.AutoRotate = true
									
								end)
							end
						end
					end)
					
					local SeraphFrame = debug.getupvalues(require(game:GetService("Players").LocalPlayer.PlayerScripts:WaitForChild("CombatFramework")))[2]
					local VirtualUser = game:GetService('VirtualUser')
					local RigControllerR = debug.getupvalues(require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework.RigController))[2]
					local Client = game:GetService("Players").LocalPlayer
					local DMG = require(Client.PlayerScripts.CombatFramework.Particle.Damage)
					
					function SeraphFuckWeapon() 
						local p13 = SeraphFrame.activeController
						local wea = p13.blades[1]
						if not wea then return end
						while wea.Parent~=game.Players.LocalPlayer.Character do wea=wea.Parent end
						return wea
					end
					
					function getHits(Size)
						local Hits = {}
						local Enemies = workspace.Enemies:GetChildren()
						local Characters = workspace.Characters:GetChildren()
						for i=1,#Enemies do local v = Enemies[i]
							local Human = v:FindFirstChildOfClass("Humanoid")
							if Human and Human.RootPart and Human.Health > 0 and game.Players.LocalPlayer:DistanceFromCharacter(Human.RootPart.Position) < Size+55 then
								table.insert(Hits,Human.RootPart)
							end
						end
						for i=1,#Characters do local v = Characters[i]
							if v ~= game.Players.LocalPlayer.Character then
								local Human = v:FindFirstChildOfClass("Humanoid")
								if Human and Human.RootPart and Human.Health > 0 and game.Players.LocalPlayer:DistanceFromCharacter(Human.RootPart.Position) < Size+55 then
									table.insert(Hits,Human.RootPart)
								end
							end
						end
						return Hits
					end
					
					task.spawn(
						function()
						while true do task.wait()
							if  _G.FastAttack then
								if SeraphFrame.activeController then
									if v.Humanoid.Health > 0 then
										SeraphFrame.activeController.timeToNextAttack = -(math.huge^math.huge^math.huge)
										SeraphFrame.activeController.timeToNextAttack = 0
										SeraphFrame.activeController.focusStart = 0
										SeraphFrame.activeController.hitboxMagnitude = 110
										SeraphFrame.activeController.humanoid.AutoRotate = true
										SeraphFrame.activeController.increment = 4
									end
								end
							end
						end
					end)
					
					function Boost()
						spawn(function()
							if SeraphFrame.activeController then
								game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("weaponChange",tostring(SeraphFuckWeapon()))
							end
						end)
					end
					
					function Unboost()
						spawn(function()
							print("Unboost ของจริง555")
							--game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("unequipWeapon",tostring(SeraphFuckWeapon()))
						end)
					end
					
					local cdnormal = 0
					local Animation = Instance.new("Animation")
					local CooldownFastAttack = 0.000000
					Attack = function()
						local ac = SeraphFrame.activeController
						if ac and ac.equipped then
							task.spawn(
								function()
								if tick() - cdnormal > 0 then
									ac:attack()
									cdnormal = tick()
								else
									Animation.AnimationId = ac.anims.basic[2]
									ac.humanoid:LoadAnimation(Animation):Play(0.01, 0.01) --ท่าไม่ทำงานแก้เป็น (1,1)
									game:GetService("ReplicatedStorage").RigControllerEvent:FireServer("hit", getHits(60), 1, "")
									wait(0.2)
								end
							end)
						end
					end
					
					b = tick()
					spawn(function()
						while _G.FastAttack do task.wait()
							if _G.FastAttack then
								if b - tick() > 9e9 then
									b = tick()
								end
								pcall(function()
									for i, v in pairs(game.Workspace.Enemies:GetChildren()) do
										if v.Humanoid.Health > 0 then
											if (v.HumanoidRootPart.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 50 then
												Attack()
												wait()
												Boost()
											end
										end
									end
								end)
								wait(0.001)
							end
						end
					end)
					
					k = tick()
					spawn(function()
						while wait() do
							if  _G.FastAttack then
								if k - tick() > 9e9 then
									k = tick()
								end
								pcall(function()
									for i, v in pairs(game.Workspace.Enemies:GetChildren()) do
										if v.Humanoid.Health > 0 then
											if (v.HumanoidRootPart.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 50 then
											Unboost()
											end
										end
									end
								end)
							end
						end
					end)
					
					tjw1 = true
					task.spawn(
						function()
							local a = game.Players.LocalPlayer
							local b = require(a.PlayerScripts.CombatFramework.Particle)
							local c = require(game:GetService("ReplicatedStorage").CombatFramework.RigLib)
							if not shared.orl then
								shared.orl = c.wrapAttackAnimationAsync
							end
							if not shared.cpc then
								shared.cpc = b.play
							end
							if tjw1 then
								pcall(
									function()
										c.wrapAttackAnimationAsync = function(d, e, f, g, h)
											local i = c.getBladeHits(e, f, g)
											if i then
												b.play = function()
												end
												d:Play(0.01,0.01,0.01)
												h(i)
												b.play = shared.cpc
												wait(9e9)
												d:Stop()
											end
										end
									end
								)
							end
						end
					)
					
					
					
					
					local CameRa = require(game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework.CameraShaker)
					CameRa.CameraShakeInstance.CameraShakeState = {FadingIn = 3,FadingOut = 2,Sustained = 0,Inactive =1}
					
					local Client = game.Players.LocalPlayer
					local STOP = require(Client.PlayerScripts.CombatFramework.Particle)
					local STOPRL = require(game:GetService("ReplicatedStorage").CombatFramework.RigLib)
					task.spawn(function()
						pcall(function()
							if not shared.orl then
								shared.orl = STOPRL.wrapAttackAnimationAsync
							end
								if not shared.cpc then
									shared.cpc = STOP.play 
								end
								spawn(function()
									require(game.ReplicatedStorage.Util.CameraShaker):Stop()
									game:GetService("RunService").Stepped:Connect(function()
										STOPRL.wrapAttackAnimationAsync = function(a,b,c,d,func)
											local Hits = STOPRL.getBladeHits(b,c,d)
											if Hits then
												if  _G.FastAttack then
													STOP.play = function() end
													a:Play(21,29,30)
													func(Hits)
													STOP.play = shared.cpc
													wait(a.length * 0)
													a:Stop()
												else
													func(Hits)
													STOP.play = shared.cpc
													wait(a.length * 0)
													a:Stop()
												end
											end
										end
									end)
								end)
							end)
						end)
					end
				end)
				wait(0.5)
				game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.TextLabel.TextBox.TextColor3 = Color3.fromRGB(0, 0, 0)
				game:GetService("Players").LocalPlayer.PlayerGui.Main.Code.TextLabel.TextBox.Text = ""
			end
		end)
	end)

------------------------------------------------------------------------------------------------------------
-------------------------------------------// Pixel Piece \\ -----------------------------------------------
------------------------------------------------------------------------------------------------------------
elseif id == 6777872443 then

_G.Color = Color3.fromRGB(255, 0, 0)
_G.Logo = 12781257228

-- Auto Update --
function ChackQ()
	local levelReq
	local NameNpc
	local Tabler = {}
	local NameIslands
	local MyLevel = tonumber(game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.DisplayGui.Bars.LabelLevel.Text)
	
	for i, v in pairs(workspace.Terrain.World.Islands:GetDescendants()) do
		if v.Name == "LevelReq" then
			
			if v.Text == "Level 20+" or v.Text == "Level 45+" or v.Text == "Level 65+" or v.Text == "Level 70+" or v.Text == "Level 75+" or v.Text == "Level 125+" or v.Text == "Level 170+" then
				for l,k in pairs(v.Parent:GetDescendants()) do
					if k.Name == "LevelReq" then
						k.Name = "Boss_LevelReq"
						print(k)
					end
				end
			end
			if v.Text == "" then
				v:Destroy()
			else
				local levelReq1 = string.gsub(v.Text, "Level ", "")
				local levelReq2 = string.gsub(levelReq1, "+", "")
				levelReq = string.gsub(levelReq2, "%s+", "")
				
				if levelReq ~= nil and levelReq ~= "" then
					
					if tonumber(levelReq) <= MyLevel then
						table.insert(Tabler, tonumber(levelReq))
					end
					
					for y,x in pairs(Tabler) do
						if tonumber(x) >= tonumber(levelReq) then
							if tonumber(levelReq) <= tonumber(x) and tonumber(x) >= tonumber(levelReq) then
								levelReq,levelReqr = tonumber(x),tonumber(x)
							end
						end
					end
				end
			end
		end
	end
	
	for i, v in pairs(workspace.Terrain.World.Islands:GetDescendants()) do
		if v.Name == "LevelReq" then
			if v.Text == "" then
				v:Destroy()
			end
			if v.Text == "Level " .. tostring(levelReqr) .. "+" then
				Island_Name = tostring(v.Parent.Parent.Parent.Parent.Parent)
				NameMon = tostring(v.Parent.Parent.Parent.Name)
				CheckCFrame = v.Parent.Parent.CFrame
				CFrameMon = CheckCFrame
			end
			if v.Text == "Level " .. tostring(levelReqr) .. "+" then
				Island_Name = tostring(v.Parent.Parent.Parent.Parent.Parent)
				NameMon = tostring(v.Parent.Parent.Parent.Name)
				CheckCFrame = v.Parent.Parent.CFrame
				CFrameMon = CheckCFrame
			end
		end
	end
	
	return {
		[1] = levelReq,
		[2] = NameMon,
		[3] = CFrameMon,
		[4] = levelReqr,
		[5] = Island_Name,
		[6] = Tabler
	}
end

spawn(function()
	while wait() do
		if _G.AutoFarm or _G.AutoFarmMon or _G.Auto_Make_Haki then
			local args = {
				[1] = {
					["TypeInput"] = "Combat",
					["Type"] = "Click",
				}
			}
			
			game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("InputEvent"):WaitForChild("inputFunc"):InvokeServer(unpack(args))
			local args = {
				[1] = {
					["TypeInput"] = "Combat",
					["Type"] = "Click",
				}
			}
			
			game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("InputEvent"):WaitForChild("inputFunc"):InvokeServer(unpack(args))
		end
	end
end)

function EquipWeapon(ToolSe)
	if game.Players.LocalPlayer.Backpack:FindFirstChild(ToolSe) then
		local tool = game.Players.LocalPlayer.Backpack:FindFirstChild(ToolSe)
		wait(.4)
		game.Players.LocalPlayer.Character.Humanoid:EquipTool(tool)
	end
end

local vu = game:GetService("VirtualUser")
game:GetService("Players").LocalPlayer.Idled:connect(function()
	vu:Button2Down(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
	wait(1)
	vu:Button2Up(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
end)

spawn(function()
	game:GetService("RunService").Stepped:Connect(function()
		if _G.AutoFarm or _G.AutoFarmMon or _G.Auto_Make_Haki then
			for _, v in pairs(game.Players.LocalPlayer.Character:GetDescendants()) do
				if v:IsA("BasePart") then
					v.CanCollide = false
				end
			end
		end
	end)
end)

spawn(function()
	while wait() do
		if _G.AutoFarm == false or _G.AutoFarmMon == false or _G.Auto_Make_Haki == false then
			if game.Players.LocalPlayer.Character:FindFirstChild('Highlight') then
				game.Players.LocalPlayer.Character:FindFirstChild('Highlight'):Destroy()
			end
		else
			if not game.Players.LocalPlayer.Character:FindFirstChild("Highlight") then
				local Highlight = Instance.new("Highlight")
				Highlight.FillColor = Color3.new(0, 0, 255)
				Highlight.OutlineColor = Color3.new(0,0,255)
				Highlight.Parent = game.Players.LocalPlayer.Character
			end
			if not game.Players.LocalPlayer.Character:FindFirstChild("Highlight") then
				local Highlight = Instance.new("Highlight")
				Highlight.FillColor = Color3.new(0, 0, 255)
				Highlight.OutlineColor = Color3.new(0,0,255)
				Highlight.Parent = game.Players.LocalPlayer.Character
								
				spawn(function()
					while wait() do
						pcall(function()
							wait(0.1) 
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(255, 0, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(255, 155, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(255, 255, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(0, 255, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(0, 255, 255)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(0, 155, 255)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(255, 0, 255)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{OutlineColor = Color3.fromRGB(255, 0, 155)}
							):Play() 
							wait(.5)
						end)
					end
				end)
				spawn(function()
					while wait() do
						pcall(function()
							wait(0.1) 
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(255, 0, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(255, 155, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(255, 255, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(0, 255, 0)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(0, 255, 255)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(0, 155, 255)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(255, 0, 255)}
							):Play() 
							wait(.5)            
							game:GetService('TweenService'):Create(
								Highlight,TweenInfo.new(1,Enum.EasingStyle.Linear,Enum.EasingDirection.InOut),
								{FillColor = Color3.fromRGB(255, 0, 155)}
							):Play() 
							wait(.5)
						end)
					end
				end)
			end
		end
	end
end)

spawn(function()
	while task.wait() do
		pcall(function()
			if _G.AutoFarm or _G.AutoFarmMon or _G.Auto_Make_Haki then
				if not game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip") then
					local Noclip = Instance.new("BodyVelocity")
					Noclip.Name = "BodyClip"
					Noclip.Parent = game:GetService("Players").LocalPlayer.Character.HumanoidRootPart
					Noclip.MaxForce = Vector3.new(100000,100000,100000)
					Noclip.Velocity = Vector3.new(0,0,0)
				end
			else
				game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip"):Destroy()
			end
		end)
	end
end)

function UseSkill(Skill,Position)
	local args = {
		[1] = {
			["TypeInput"] = "UseSkill",
			["KeyCode"] = Skill,
			["MousePosition"] = Position
		}
	}

	game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("InputEvent"):WaitForChild("inputFunc"):InvokeServer(unpack(args))

end
function KeyBo(Key)
	game:GetService("VirtualInputManager"):SendKeyEvent(true,Key,false,game)
	wait(0.1)
	game:GetService("VirtualInputManager"):SendKeyEvent(false,Key,false,game)	
end

spawn(function()
	while true do wait()
		if setscriptable then
			setscriptable(game.Players.LocalPlayer, "SimulationRadius", true)
		end
		if sethiddenproperty then
			sethiddenproperty(game.Players.LocalPlayer, "SimulationRadius", math.huge)
		end
	end
end)

function BringMobFarm(Mon, Position)
	for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
		if v.Name == Mon and v:FindFirstChild("EnemyHumanoid") and v:FindFirstChild("HumanoidRootPart") and v.EnemyHumanoid.Health >= 0 and (v.HumanoidRootPart.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 150 then
			setscriptable(game.Players.LocalPlayer, "SimulationRadius", true)
			v.HumanoidRootPart.CFrame = Position
			v.EnemyHumanoid:ChangeState(11)
			v.EnemyHumanoid:ChangeState(14)
			sethiddenproperty(game.Players.LocalPlayer, "SimulationRadius", math.huge)
		end
	end 
	for n,m in pairs(game:GetService("ReplicatedStorage"):GetDescendants()) do
		if m.Name == "NPCs" then
			m:Destroy()
		end
	end
end

------------ // Hop \\ ------------
	
function Hop()
	local PlaceID = game.PlaceId
	local AllIDs = {}
	local foundAnything = ""
	local actualHour = os.date("!*t").hour
	local Deleted = false
	function TPReturner()
		local Site;
		if foundAnything == "" then
			Site = game.HttpService:JSONDecode(game:HttpGet('https://games.roblox.com/v1/games/' .. PlaceID .. '/servers/Public?sortOrder=Asc&limit=100'))
		else
			Site = game.HttpService:JSONDecode(game:HttpGet('https://games.roblox.com/v1/games/' .. PlaceID .. '/servers/Public?sortOrder=Asc&limit=100&cursor=' .. foundAnything))
		end
		local ID = ""
		if Site.nextPageCursor and Site.nextPageCursor ~= "null" and Site.nextPageCursor ~= nil then
			foundAnything = Site.nextPageCursor
		end
		local num = 0;
		for i,v in pairs(Site.data) do
			local Possible = true
			ID = tostring(v.id)
			if tonumber(v.maxPlayers) > tonumber(v.playing) then
				for _,Existing in pairs(AllIDs) do
					if num ~= 0 then
						if ID == tostring(Existing) then
							Possible = false
						end
					else
						if tonumber(actualHour) ~= tonumber(Existing) then
							local delFile = pcall(function()
								AllIDs = {}
								table.insert(AllIDs, actualHour)
							end)
						end
					end
					num = num + 1
				end
				if Possible == true then
					table.insert(AllIDs, ID)
					wait()
					pcall(function()
						wait()
						game:GetService("TeleportService"):TeleportToPlaceInstance(PlaceID, ID, game.Players.LocalPlayer)
					end)
					wait(4)
				end
			end
		end
	end
	function Teleport() 
		while wait() do
			pcall(function()
				TPReturner()
				if foundAnything ~= "" then
					TPReturner()
				end
			end)
		end
	end
	Teleport()
end

local Evil = loadstring(game:HttpGet("https://newpchx2.0xlii.repl.co/AllUI/Protected_9405524596134885.lua"))()

local Win = library:Evil("   Zee","Hub",_G.Logo )

local tab1 = Win:CraftTab('Main')
local tab2 = Win:CraftTab('Local Player')
local tab3 = Win:CraftTab('Teleport')

local page1 = tab1:CraftPage('Main',1)
local page2 = tab1:CraftPage('Settings',2)

local page3 = tab2:CraftPage('Auto Stats',1)
local page4 = tab2:CraftPage('',2)

local page5 = tab3:CraftPage('Teleport',1)
local page6 = tab3:CraftPage('Server',2)

page1:Label('Log')

local Log1 = page1:LabelLog('Loll')

spawn(function()
	while wait() do
		if _G.NameMon and _G.AutoFarm then
			Log1:Refresh("Mon Quest : ".. _G.NameMon)
			Log1:Color(Color3.fromRGB(0, 255, 0))
		else
			Log1:Refresh("Mon Quest : Nil")
			Log1:Color(Color3.fromRGB(255, 255, 255))
		end
	end
end)


page1:Label('Auto Farm')
_G.SelectMethod = nil
page1:Dropdown("Select Method",{"Behind","Below","Upper"},{},function(value)
	_G.SelectMethod = value
end)

page1:Toggle('Auto Farm',false,function(v)
	_G.AutoFarm = v
end)

spawn(function()
	while wait() do
		if _G.AutoFarm then
			pcall(function()
				ChackQ()
				if game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false then
					wait(0.2)
					repeat wait() game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = ChackQ()[3] until (ChackQ()[3].Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFram
					wait(1.5)
					local args = {
						[1] = {
							["NPCIsland"] = ChackQ()[5],
							["Call"] = "Dialog",
							["NPCName"] = ChackQ()[2],
							["MessageType"] = "Finish"
						}
					}
					
					game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("NPCInteract"):InvokeServer(unpack(args))
					wait(1)               
				else
					local MyLevel = tonumber(game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.DisplayGui.Bars.LabelLevel.Text)
					if MyLevel >= 1 and MyLevel <= 9 then
						Ms = "Bit Bandit"
					elseif MyLevel >= 35 and MyLevel <= 54 then
						Ms = "Swordsman Sailor"
					elseif MyLevel >= 55 and MyLevel <= 84 then
						Ms = "Clown Pirate"
					elseif MyLevel >= 210 and MyLevel <= 229 then
						Ms = "Thief"
					else
						TextMs = string.gsub(game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Handle.TemplateText.Label.Text, "s", "")
						TextMs5 = string.gsub(TextMs, "Defeat ", "")
						TextMs2 = string.gsub(TextMs5, " of ", "")
						TextMs3 = string.gsub(TextMs2, "/", "")
						TextMs4 = string.gsub(TextMs3, "%d", "")
						TextMs6 = string.gsub(TextMs4, "  ", " ")
						Ms = string.sub(TextMs6,2,#TextMs6);
					end
					if workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild(Ms) then
						for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
							if v.Name == Ms and v:FindFirstChild("EnemyHumanoid") and v:FindFirstChild("HumanoidRootPart") and v.EnemyHumanoid.Health >= 0 then
								repeat task.wait()
									_G.WaitServer = 0
									_G.NameMon = v.Name
									_G.Open = true
									_G.Pos = v.HumanoidRootPart.Position

									if _G.SelectMethod == "Behind" then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 0, _G.DistanceMob)
									elseif _G.SelectMethod ==  "Below" then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0)
									elseif _G.SelectMethod ==  "Upper" then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(-270), 0, 0) - Vector3.new(0,_G.DistanceMob,0)
									elseif _G.SelectMethod == nil then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 0, _G.DistanceMob)
									end
									
									EquipWeapon(_G.SelectWeapon)
									BringMobFarm(v.Name,v.HumanoidRootPart.CFrame)
								until _G.AutoFarm == false or not v.Parent or v.EnemyHumanoid.Health <= 0 or game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false
								if workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild(Ms) then
									for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
										if v.Name == Ms then
											game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0)
										end
									end
								else
									for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
										if v.Name == Ms then
											game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-300,0)
										end
									end
								end
							end
						end 
					else
						_G.NameMon = nil
						_G.Open = false
						_G.Pos = nil
						for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
							if v.Name == Ms then
								game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-300,0)
							end
						end
					end
				end
				_G.Mon = Ms
				_G.Npc = ChackQ()[2]
				local args = {
					[1] = {
						["NPCIsland"] = ChackQ()[5],
						["Call"] = "Dialog",
						["NPCName"] = "Loro",
						["MessageType"] = "Finish"
					}
				}
				
				game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("NPCInteract"):InvokeServer(unpack(args))
			end)
		end
	end
end)
spawn(function()
	while wait() do
		if game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false then
			_G.NameMon = nil
			_G.Npc = nil
		end
	end
end)

WeaponList = {}
	
for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do  
	if v:IsA("Tool") then
		table.insert(WeaponList ,v.Name)
	end
end

local SelectWeapona = page1:Dropdown("Select Weapon",WeaponList,{},function(value)
	_G.SelectWeapon = value
	print(_G.SelectWeapon)
end)

page1:Button("Refresh Weapon",function()
	SelectWeapona:Clear()
	for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do  
		SelectWeapona:Add(v.Name)
	end
end)

SelectMons = {
	"Arling Bandit",
	"Angry Pirate",
	"Bit Bandit",
	"Byte Brawler",
	"Captain Puggy",
	"Clown Pirate",
	"Corrupt Marine",
	"Kabaji",
	"Norgan",
	"Nuro Bandit",
	"Nuro Pirate",
	"Sailor",
	"Strong Arling Bandit",
	"Strong Nuro Bandit",
	"Swordsman Sailor",
	"Swordsman Sailor Guard",
	"Thief"
}

page1:Dropdown("Select Mons",SelectMons,{},function(value)
	_G.SelectMon = value
end)

page1:Toggle('Auto Farm [Mons]',false,function(v)
	_G.AutoFarmMon = v
end)

spawn(function()
	while task.wait() do
		pcall(function()
			if _G.AutoFarmMon then
				if workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild(_G.AutoFarmMon) or not workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild("EnemyHumanoid") or workspace.Terrain.World.TargetFilter.Characters.Enemys.EnemyHumanoid.Health <= 0 then
					for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
						if v.Name == _G.SelectMon and v:FindFirstChild("EnemyHumanoid") and v:FindFirstChild("HumanoidRootPart") and v.EnemyHumanoid.Health >= 0 then
							OldCFrame = v.HumanoidRootPart.CFrame
							repeat task.wait()
								_G.WaitServer = 0
								_G.NameMon = v.Name
								_G.Open = true
								_G.Pos = v.HumanoidRootPart.Position

								v.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame
								if _G.SelectMethod == "Behind" then
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 0, _G.DistanceMob)
								elseif _G.SelectMethod ==  "Below" then
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0)
								elseif _G.SelectMethod ==  "Upper" then
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(-270), 0, 0) - Vector3.new(0,_G.DistanceMob,0)
								elseif _G.SelectMethod == nil then
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 0, _G.DistanceMob)
								end

								EquipWeapon(_G.SelectWeapon)
								BringMobFarm(v.Name,OldCFrame)
							until _G.AutoFarmMon == false or not v.Parent or v.EnemyHumanoid.Health <= 0 or game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false
							if workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild(_G.AutoFarmMon) then
								for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
									if v.Name == _G.SelectMon then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0)
									end
								end
							else
								for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
									if v.Name == Ms then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-300,0)
									end
								end
							end
						end
					end 
				end
			end
		end)
	end
end)

page1:Toggle('Auto Make Haki (Wait Night)',false,function(v)
	_G.Auto_Make_Haki = v
end)

_G.Gorilla = 0
spawn(function()
	while wait() do 
		if _G.Auto_Make_Haki then
			pcall(function()
				local MyLevel = tonumber(game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.DisplayGui.Bars.LabelLevel.Text)
				local CFrameQ = CFrame.new(-6726.80518, 628.329468, -1867.9397, 0.801019609, -0, -0.598638117, 0, 1, -0, 0.598638117, 0, 0.801019609)
				if MyLevel >= 70 then
					if _G.AutoFarm then
						_G.AutoFarm = false
					end
					if game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false then
						repeat wait() game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrameQ until (CFrameQ.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.AutoFram
						wait(1.5)
						game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("NPCInteract"):InvokeServer({["Call"] = "Dialog",["NPCIsland"] = "Vaill Island",["NPCName"] = "Next",["MessageType"] = "Finish"})
					else
						if workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild("Ancient Gorilla") then
							for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
								if v.Name == "Ancient Gorilla" and v:FindFirstChild("EnemyHumanoid") and v:FindFirstChild("HumanoidRootPart") and v.EnemyHumanoid.Health >= 0 then
									OldCFrame = v.HumanoidRootPart.CFrame
									repeat task.wait()
										_G.Open = true
										v.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 30+_G.DistanceMob+10, _G.DistanceMob+4*2+10)
										EquipWeapon(_G.SelectWeapon)
									until _G.Auto_Make_Haki == false or not v.Parent or v.EnemyHumanoid.Health <= 0 or game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false
									if _G.Auto_Make_Haki then
										wait(0.5)
										Hop()
									end
								else
									game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-5338.99463, 892.570068, 17126.5527, -0.996331394, -1.08686193e-09, 0.0855787918, -1.01004893e-09, 1, 9.4086916e-10, -0.0855787918, 8.50978732e-10, -0.996331394)
								end
							end
						else
							game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-5338.99463, 892.570068, 17126.5527, -0.996331394, -1.08686193e-09, 0.0855787918, -1.01004893e-09, 1, 9.4086916e-10, -0.0855787918, 8.50978732e-10, -0.996331394)
						end
					end
				else
					_G.AutoFarm = true
				end
			end)
		end
	end
end)

page1:Toggle('Auto Puggy',false,function(v)
	_G.Auto_Puggy = v
end)

_G.Gogo = false
spawn(function()
	while wait() do 
		if _G.Auto_Puggy then
			pcall(function()
				if _G.Gogo == false then
					local CFrameNPC = CFrame.new(-3045.3896484375, 600.0927734375, -1769.1929931640625)
					repeat wait() game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrameNPC until (CFrameNPC.Position - game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= 20 or not _G.Auto_Puggy
					wait(1.2)
					local args = {
						[1] = {
							["Type"] = "TeleportUIButtonAccept"
						}
					}
					
					game:GetService("ReplicatedStorage").Files.Remotes.SinglePlayers.MacalovemigTH.Interface:FireServer(unpack(args))
					wait(0.2)
					local args = {
						[1] = {
							["Type"] = "TeleportUIButtonAccept"
						}
					}
					
					game:GetService("ReplicatedStorage").Files.Remotes.SinglePlayers.MacalovemigTH.Interface:FireServer(unpack(args))
					wait(0.2)
					local args = {
						[1] = {
							["Type"] = "TeleportUIButtonAccept"
						}
					}
					
					game:GetService("ReplicatedStorage").Files.Remotes.SinglePlayers.MacalovemigTH.Interface:FireServer(unpack(args))
					wait(3.5)
					_G.Gogo = true
				else
					if workspace.Terrain.World.TargetFilter.Characters.Enemys:FindFirstChild("Captain Puggy") then
						for _,v in pairs(workspace.Terrain.World.TargetFilter.Characters.Enemys:GetChildren()) do
							if v.Name == "Captain Puggy" and v:FindFirstChild("EnemyHumanoid") and v:FindFirstChild("HumanoidRootPart") and v.EnemyHumanoid.Health >= 0 then
								OldCFrame = v.HumanoidRootPart.CFrame
								repeat task.wait()
									_G.WaitServer = 0
									_G.NameMon = v.Name
									_G.Open = true
									_G.Pos = v.HumanoidRootPart.Position

									v.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame
									if _G.SelectMethod == "Behind" then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 0, _G.DistanceMob)
									elseif _G.SelectMethod ==  "Below" then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0)
									elseif _G.SelectMethod ==  "Upper" then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(-270), 0, 0) - Vector3.new(0,_G.DistanceMob,0)
									elseif _G.SelectMethod == nil then
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = v.HumanoidRootPart.CFrame * CFrame.new(0, 0, _G.DistanceMob)
									end

									EquipWeapon(_G.SelectWeapon)
									BringMobFarm(v.Name,OldCFrame)
								until _G.Auto_Puggy == false or not v.Parent or v.EnemyHumanoid.Health <= 0 or game:GetService("Players").LocalPlayer.PlayerGui.PlayerHUD.Interface.Others.QuestUI.Visible == false
							else
								_G.Gogo = false
								game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-2878.026123046875, 389.269287109375, -1444.66748046875)
							end
						end
					else
						game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-2878.026123046875, 389.269287109375, -1444.66748046875)
					end
				end
			end)
		end
	end
end)

--CFrameMon = CFrame.new(-1400.0386962890625, 593.2979125976562, -10435.5732421875)

page2:Toggle('Auto Skill',false,function(a)
	_G.Auto_Skill = a
end)

spawn(function()
	while wait(0.1) do 
		if _G.Auto_Skill and _G.Open then
			KeyBo(32)
			KeyBo("Z")
			UseSkill("Z",_G.Pos)
			wait(0.5)
			KeyBo("X")
			UseSkill("X",_G.Pos)
			wait(0.5)
			KeyBo("C")
			UseSkill("C",_G.Pos)
			wait(0.5)
			KeyBo("V")
			UseSkill("V",_G.Pos)
			wait(0.5)
			KeyBo("B")
			UseSkill("B",_G.Pos)
		end
	end
end)

page2:Toggle('Auto Haki',true,function(a)
	_G.Auto_Haki = a
end)

spawn(function()
	while wait() do
		if _G.Auto_Haki then
			for _, v in ipairs(game.Players.LocalPlayer.Character:GetChildren()) do
				if v.Name == "PresetsRig" then
					local busoModel = v:FindFirstChild("BusoModel")
					if busoModel then else
						print(v)
						local args = {
							[1] = {
								["TypeInput"] = "Busoshoku"
							}
						}
						game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("InputEvent"):FireServer(unpack(args))
					end
				end
			end            
		end
	end
end)

_G.DistanceMob = 6
page2:Slider("DistanceMob",true,0,10,6,function(v)
	_G.DistanceMob = v
end)

page2:Button("Load All Island",function()
	local Island = {}
	for i, v in pairs(workspace.Terrain.World.Islands:GetChildren()) do  
		table.insert(Island, v.Name)
	end
	_G.AllIsland = Island
	for i, v in next ,_G.AllIsland do wait(1.5)
		local islandObject = workspace.Terrain.World.Islands[v]
		for _, g in pairs(islandObject:GetChildren()) do
			if g.Name == "NPCs" then
				local CFramePos = g.WorldPivot
				game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFramePos
			end
		end
	end
end)

page3:Label("Stats")

page3:MultiDropdown("Select Stats",{"Melee","Defense","Blade","Gun","DevilFruit"},{"Melee","Defense"},function(v)
	_G.Select_Stats = v
end)

page3:Toggle('Auto Stats',false,function(a)
	Auto_Stats = a
	_G.Open = false
end)

spawn(function()
	while wait() do
		pcall(function()
			if Auto_Stats then
				for _,g in next, _G.Select_Stats do
					local args = {
						[1] = {
							["Call"] = "EvolveStats",
							["Attribute"] = g
						}
					}
					
					game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("RemoteStatus"):WaitForChild("statusFunc"):InvokeServer(unpack(args))                    
				end
			end
		end)
	end
end)

Island = {}
for i,v in pairs(workspace.Terrain.World.Islands:GetChildren()) do  
	table.insert(Island ,v.Name)
end
page5:Dropdown("Select Island",Island,{},function(value)
	_G.Select_Island = value
end)

page5:Button("Teleport",function()
	for i,v in pairs(workspace.Terrain.World.Islands:GetChildren()) do 
		if v.Name == _G.Select_Island then 
			for _,g in pairs(v:GetChildren()) do
				if g.Name == "NPCs" then
					local CFramePos = g.WorldPivot
					game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFramePos
				end
			end
		end
	end
end)

page6:Textbox("ID Job","",function(v)
	print(v)
	_G.JobID = v
end)

page6:Button("TP Job ID", function()
	game:GetService("TeleportService"):TeleportToPlaceInstance(game.PlaceId, _G.JobID, game:GetService("Players").LocalPlayer)
end)

page6:Button("Copy Job ID", function()
	setclipboard(game.JobId)
end)

page6:Button("rejoin", function()
	game:GetService("TeleportService"):TeleportToPlaceInstance(game.PlaceId, game.JobId, game:GetService("Players").LocalPlayer)
end)

page6:Button("Hop", function()
	Hop()
end)

repeat wait() until game.Players.LocalPlayer.Character

local args = {
	[1] = {
		["NPCIsland"] = ChackQ()[5],
		["Call"] = "Dialog",
		["NPCName"] = "Loro",
		["MessageType"] = "Finish"
	}
}

game:GetService("ReplicatedStorage"):WaitForChild("Files"):WaitForChild("Remotes"):WaitForChild("SinglePlayers"):WaitForChild(game.Players.LocalPlayer.Name):WaitForChild("NPCInteract"):InvokeServer(unpack(args))
	
------------------------------------------------------------------------------------------------------------
-------------------------------------------// KL \\ -----------------------------------------------
------------------------------------------------------------------------------------------------------------

elseif id == 4520749081 or id == 6381829480 or id == 5931540094 then
	
function EquipWeapon(ToolSe)
	if game.Players.LocalPlayer.Backpack:FindFirstChild(ToolSe) then
		local tool = game.Players.LocalPlayer.Backpack:FindFirstChild(ToolSe)
		wait(.4)
		game.Players.LocalPlayer.Character.Humanoid:EquipTool(tool)
	end
end

function CheckQuest()
    QUEST = {}
    LVLREAL = {}
    local MyLevel = game.Players.LocalPlayer.PlayerStats.lvl.Value
    
    if MyLevel >= 0 and MyLevel <= 9 then
    	LevelQuest = 0
	    return {
	        [1] = LevelQuest,
	    }

    end
    
	if MyLevel >= 3350 and MyLevel <= 3374 then
		LevelQuest = 3325
		return {
			[1] = LevelQuest,
		}
	end

	if MyLevel >= 3375 and MyLevel <= 3399 then
		local Material = CheckMaterial("MagmaCrystal")
    	if Material == "MagmaCrystal" then
        	LevelQuest = 3375
			return {
				[1] = LevelQuest,
			}
    	else
			LevelQuest = nil
        	for _,v in pairs(game:GetService("Workspace").Monster:GetDescendants()) do
				if v.Name == "The Volcano [Lv. 3325]" and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
					repeat task.wait();
						TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
						EquipWeapon(_G.Weapon)
						_G.Open = true
						_G.Pos = v.HumanoidRootPart.CFrame
					until not v.Parent or v.Humanoid.Health <= 0 or not _G.AutoFarm or Material == "MagmaCrystal"
					return
				end
			end
			return {
				[1] = LevelQuest,
			}
    	end
	end

	if MyLevel >= 3475 and MyLevel <= 3499 then
		local Material = CheckMaterial("DarkBeardsTotem")
    	if Material == "DarkBeardsTotem" then
        	LevelQuest = 3475
			return {
				[1] = LevelQuest,
			}
    	else
			LevelQuest = nil
        	for _,v in pairs(game:GetService("Workspace").Monster:GetDescendants()) do
				if v.Name == "Dark Beard Servant [Lv. 3400]" and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
					repeat task.wait();
						TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
						EquipWeapon(_G.Weapon)
						_G.Open = true
						_G.Pos = v.HumanoidRootPart.CFrame
					until not v.Parent or v.Humanoid.Health <= 0 or not _G.AutoFarm or Material == "DarkBeardsTotem"
					return
				else
					TP(CFrame.new(-9357.27441, 97.5559387, -4522.19092, 0.939733207, -5.11258946e-09, -0.341908544, -2.02941042e-09, 1, -2.05309103e-08, 0.341908544, 1.99874517e-08, 0.939733207))
				end
			end
			return {
				[1] = LevelQuest,
			}
    	end
	end

	if MyLevel >= 3575 and MyLevel <= 3599 then
		LevelQuest = 3550
		return {
			[1] = LevelQuest,
		}
	end

    for i,v in pairs(game:GetService("Workspace").AllNPC:GetChildren()) do 
        if string.find(v.Name,"QuestLvl") then
            table.insert(QUEST,v.Name)
        end
    end
    for i,v in pairs(game:GetService("ReplicatedStorage").MAP:GetChildren()) do 
        if string.find(v.Name,"QuestLvl") then
            table.insert(QUEST,v.Name)
        end
    end
    for i,v in pairs(QUEST) do
        values = v:split("QuestLvl")
        LVL = values[2]
        if MyLevel >= tonumber(LVL) then
            table.insert(LVLREAL,LVL)
        end
    end
    LevelQuest = math.max(unpack(LVLREAL))
    return {
        [1] = LevelQuest,
    }

end


spawn(function()
	game:GetService("RunService").Stepped:Connect(function()
		if _G.AutoFarm or _G.Kaido or _G.Ms_Mother or _G.GhostShip or _G.AutoSecondSea or _G.AutoFarmBoss then
			for _, v in pairs(game.Players.LocalPlayer.Character:GetDescendants()) do
				if v:IsA("BasePart") then
					v.CanCollide = false
				end
			end
		end
	end)
end)

spawn(function()
	while task.wait() do
		pcall(function()
			if _G.AutoFarm or _G.Kaido or _G.Ms_Mother or _G.GhostShip or _G.AutoSecondSea or _G.AutoFarmBoss then
				if not game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip") then
					local Noclip = Instance.new("BodyVelocity")
					Noclip.Name = "BodyClip"
					Noclip.Parent = game:GetService("Players").LocalPlayer.Character.HumanoidRootPart
					Noclip.MaxForce = Vector3.new(100000,100000,100000)
					Noclip.Velocity = Vector3.new(0,0,0)
				end
			else
				game:GetService("Players").LocalPlayer.Character.HumanoidRootPart:FindFirstChild("BodyClip"):Destroy()
			end
		end)
	end
end)

spawn(function()
    while wait() do
        if _G.AutoFarm or _G.Kaido or _G.Ms_Mother or _G.GhostShip or _G.AutoSecondSea or _G.AutoFarmBoss then
            pcall(function()
				_G.Start = true
                game:GetService("Players").LocalPlayer.Character.Services.Client.KenEvent:InvokeServer(true)	
            end)
		else
			_G.Start = false
        end
    end
end)

local vu = game:GetService("VirtualUser")
game:GetService("Players").LocalPlayer.Idled:connect(function()
	vu:Button2Down(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
	wait(1)
	vu:Button2Up(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
end)

spawn(function()
	while wait() do
		if _G.Start then
			Click()
		end
	end
end)

function TP(Point)
	game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = Point
end

function CheckItem(String)
    local t = {}
    
    local cleanedString = string.gsub(String, "[{}]", "")
    cleanedString = string.gsub(cleanedString, ":[%s]*%a+[%s]*[,}]", "")  -- Remove ":<word>," or ":<word>}"
    cleanedString = string.gsub(cleanedString, "%d+", "")   -- Remove numeric values
    cleanedString = string.gsub(cleanedString, ":", "")  -- Remove colons ":"
    cleanedString = string.gsub(cleanedString, "%s+", "") -- Remove spaces
    cleanedString = string.gsub(cleanedString, "'", "") -- Remove '
    cleanedString = string.gsub(cleanedString, '"', "") -- Remove "

    for v in string.gmatch(cleanedString, "[^,]+") do
        table.insert(t, v)
    end
    
    return t
end

function CheckMaterial(M)
    local CheckMaterials = CheckItem(game:GetService("Players").LocalPlayer.PlayerStats.Material.Value)
    local Material
    
    for i = 1, 200 do
        if CheckMaterials[i] == M then
           Material = CheckMaterials[i]
        break
        else
            Material = nil
        end
    end
    
    return Material
end

function Click()
	local args = {
		[1] = "FS_".._G.Weapon.."_M1"
	}
	
	game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("Functions"):WaitForChild("SkillAction"):InvokeServer(unpack(args))
	local args = {
		[1] = "SW_".._G.Weapon.."_M1"
	}
	
	game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("Functions"):WaitForChild("SkillAction"):InvokeServer(unpack(args))
		
end

function AddQuest(value)
	if value then
		if not game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]) then
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame
		else
			game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame
		end
		game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
		game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
		for i,v in pairs(game.Players.LocalPlayer.PlayerGui:GetDescendants()) do
			if v.Name == "Dialogue" then
				v.Accept.Size = UDim2.new(0, 10000, 0, 10000);
				v.Accept.Position = UDim2.new(-2, 0, -5, 0);
				v.Accept.ImageTransparency = 1;
				game:GetService("ReplicatedStorage").Remotes.Functions.CheckQuest:InvokeServer();
			end
		end
	end
end

function UseSkill(skill,pos)
	if _G.SelectWeapon == "Melee" then
		for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do 
			if v.ClassName == "Tool" then 
				if v.ToolTip == "Combat" then 
					if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then 
						local args = {
							[1] = "FS_"..tostring(v.Name).."_"..tostring(skill),
							[2] = {
								["MouseHit"] = pos,
								["Type"] = "Down"
							}
						}
					
						game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("Functions"):WaitForChild("SkillAction"):InvokeServer(unpack(args))
					end
				end
			end
		end
	elseif _G.SelectWeapon == "Sword" then
		for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do 
			if v.ClassName == "Tool" then 
				if v.ToolTip == "Sword" then 
					if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then 
						local args = {
							[1] = "SW_"..tostring(v.Name).."_"..tostring(skill),
							[2] = {
								["MouseHit"] = pos,
								["Type"] = "Down"
							}
						}
						
						game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("Functions"):WaitForChild("SkillAction"):InvokeServer(unpack(args))
					end
				end
			end
		end
	else
		for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do 
			if v.ClassName == "Tool" then 
				if v.ToolTip == "Combat" then 
					if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then 
						local args = {
							[1] = "FS_"..tostring(v.Name).."_"..tostring(skill),
							[2] = {
								["MouseHit"] = pos,
								["Type"] = "Down"
							}
						}
					
						game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("Functions"):WaitForChild("SkillAction"):InvokeServer(unpack(args))
					end
				end
			end
		end
	end
end

function KeyBo(Key)
	game:GetService("VirtualInputManager"):SendKeyEvent(true,Key,false,game)
	wait(0.1)
	game:GetService("VirtualInputManager"):SendKeyEvent(false,Key,false,game)	
end

_G.Color = Color3.fromRGB(255, 0, 0)
----------------------------------------------------------------------------------------------------------------------------------------------
local Evil = loadstring(game:HttpGet("https://saverkeyzeehub.0xlii.repl.co/UI.lua"))()
local Win = library:Evil(Check["Name Hub"],"| Youtuber : zuwz ",Check["Logo"])

local Tab = Win:CraftTab("Main","")

local page1 = Tab:CraftPage("Main",1)
local page2 = Tab:CraftPage("Main",2)

page1:Toggle('Auto Farm [Level] \n ออโต้ ฟาร์ม',false,function(a)
    _G.AutoFarm = a
	_G.Start = a
    _G.Open = false
end)

spawn(function()
	while task.wait() do
		if _G.AutoFarm then
			pcall(function()
				game.Players.LocalPlayer.Character.Humanoid.Sit = false;
				if game.Players.LocalPlayer.PlayerGui.Quest.QuestBoard.Visible == false then
					game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame * CFrame.new(0,-2,-2)
					wait(.2)
					game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame * CFrame.new(0,2,0)
					game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
					game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
					wait(0.5)
					for i,v in pairs(game.Players.LocalPlayer.PlayerGui:GetDescendants()) do
						if v.Name == "Dialogue" then
							v.Accept.Size = UDim2.new(0, 10000, 0, 10000);
							v.Accept.Position = UDim2.new(-2, 0, -5, 0);
							v.Accept.ImageTransparency = 1;
							game:GetService("ReplicatedStorage").Remotes.Functions.CheckQuest:InvokeServer();
						end
					end
					wait(1.5)
					game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
					game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
					wait(0.1)
					--game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame * CFrame.new(0,300,0)
				elseif game.Players.LocalPlayer.PlayerGui.Quest.QuestBoard.Visible == true then
					if game.Players.LocalPlayer.PlayerStats.lvl.Value >= 3800 and game.Players.LocalPlayer.PlayerStats.lvl.Value <= 3849 then
						Ms = "Dead Troupe [Lv. 3800]"
					elseif game.Players.LocalPlayer.PlayerStats.lvl.Value >= 3850 and game.Players.LocalPlayer.PlayerStats.lvl.Value <= 3949 then
						Ms = "Dead Troupe Captain [Lv. 3850]"
					else
						Ms = string.sub(game:GetService("Players").LocalPlayer.PlayerGui.Quest.QuestBoard.QuestCount.Text,5,#game:GetService("Players").LocalPlayer.PlayerGui.Quest.QuestBoard.QuestCount.Text);
					end
					if game:GetService("Workspace").Monster.Mon:FindFirstChild(Ms) or game:GetService("Workspace").Monster.Boss:FindFirstChild(Ms) then
						for _,v in pairs(game:GetService("Workspace").Monster:GetDescendants()) do
							if v.Name == Ms and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
								repeat task.wait();
									_G.Open = true
									_G.Pos = v.HumanoidRootPart.CFrame
									TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
									EquipWeapon(_G.Weapon)
								until not v.Parent or v.Humanoid.Health <= 0 or not _G.AutoFarm or game:GetService("Players").LocalPlayer.PlayerGui.Quest.QuestBoard.Visible == false;
							end
						end
					else
						_G.Open = false
						game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame
					end
				else
					_G.Open = false
					--game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game:GetService("Workspace").AllNPC:FindFirstChild("QuestLvl"..CheckQuest()[1]).CFrame * CFrame.new(0,300,0)
				end
			end)
		end
	end
end)

Waspon = {"Melee","Sword"}
SelectWeapon = "Melee"
page2:Dropdown("Select ToolSe \n เลือก ของ",Waspon,"Melee",function(v)
	_G.SelectWeapon = v
end)


spawn(function() 
    while wait() do
		pcall(function()
			if _G.SelectWeapon == "Melee" then
				for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do 
					if v.ClassName == "Tool" then 
						if v.ToolTip == "Combat" then 
							if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then 
								_G.Weapon = tostring(v.Name)
							end
						end
					end
				end
			elseif _G.SelectWeapon == "Sword" then
				for i,v in pairs(game:GetService("Players").LocalPlayer.Backpack:GetChildren()) do 
					if v.ClassName == "Tool" then 
						if v.ToolTip == "Sword" then 
							if game.Players.LocalPlayer.Backpack:FindFirstChild(tostring(v.Name)) then 
								_G.Weapon = tostring(v.Name)
							end
						end
					end
				end
			end
		end)
	end
end)

page1:Toggle('Auto SecondSea \n ไปโลก 2',false,function(a)
    _G.AutoSecondSea = a
    _G.Open = false
	_G.Start = a
end)

spawn(function()
    while wait() do
        pcall(function()
            if _G.AutoSecondSea and id == 4520749081 then
				local Material = CheckMaterial("Map")

                if game:GetService("Players").LocalPlayer.PlayerStats.SecondSeaProgression.Value == "Yes" then
                    game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-2405.7236328125, 16.19996452331543, -4363.087890625)
                    game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
					game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
					for i,v in pairs(game.Players.LocalPlayer.PlayerGui:GetDescendants()) do
						if v.Name == "Dialogue" then
							v.Accept.Size = UDim2.new(0, 10000, 0, 10000);
							v.Accept.Position = UDim2.new(-2, 0, -5, 0);
							v.Accept.ImageTransparency = 1;
							game:GetService("ReplicatedStorage").Remotes.Functions.CheckQuest:InvokeServer();
						end
					end
					game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
					game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
                else
                    if game.Players.LocalPlayer.PlayerStats.lvl.Value >= 2250  then
                        _G.AutoFarm = false
                        if Material == "Map" then
                            game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(2607.74609375, 211.32774353027344, -1808.31884765625)
                            game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
							game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
							for i,v in pairs(game.Players.LocalPlayer.PlayerGui:GetDescendants()) do
								if v.Name == "Dialogue" then
									v.Accept.Size = UDim2.new(0, 10000, 0, 10000);
									v.Accept.Position = UDim2.new(-2, 0, -5, 0);
									v.Accept.ImageTransparency = 1;
									game:GetService("ReplicatedStorage").Remotes.Functions.CheckQuest:InvokeServer();
								end
							end
							game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
							game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
                        else
                            if game:GetService("Players").LocalPlayer.PlayerGui.Quest.QuestBoard.Visible == false then
                                game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(2607.74609375, 211.32774353027344, -1808.31884765625)
                                game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
								game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
                                wait(.5)
                                for i,v in pairs(game.Players.LocalPlayer.PlayerGui:GetDescendants()) do
                                    if v.Name == "Dialogue" then
                                        v.Accept.Size = UDim2.new(0, 10000, 0, 10000)
                                        v.Accept.Position = UDim2.new(-2, 0, -5, 0)
                                        v.Accept.ImageTransparency = 1
                                        game:GetService("ReplicatedStorage").Remotes.Functions.CheckQuest:InvokeServer()
                                    end
                                end
								game:GetService'VirtualUser':Button1Down(Vector2.new(1,1));
								game:GetService'VirtualUser':Button1Up(Vector2.new(1,1));
                            else
                                if game:GetService("Workspace").Monster.Boss:FindFirstChild("Seasoned Fishman [Lv. 2200]") then
                                    for i,v in pairs(game:GetService("Workspace").Monster.Boss:GetChildren()) do
                                        if v.Name == "Seasoned Fishman [Lv. 2200]" and v.Humanoid.Health > 0 then
                                            repeat task.wait()
                                                if game.Players.LocalPlayer.Character.Haki.Value ~= 1 then
													game:GetService("Players").LocalPlayer.Character.Services.Client.Armament:FireServer();
												end
												TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
												EquipWeapon(_G.Weapon)
												_G.Open = true
												_G.Pos = v.HumanoidRootPart.CFrame
                                            until v.Humanoid.Health <= 0 or not _G.AutoSecondSea or game.Players.LocalPlayer.Character:FindFirstChild("Map") or game.Players.LocalPlayer.Backpack:FindFirstChild("Map")
											_G.Open = false
											return
                                        end
                                    end
                                else
									_G.Open = false
                                    if game:GetService("ReplicatedStorage").MOB:FindFirstChild("Seasoned Fishman [Lv. 2200]") then
                                        TP(game:GetService("ReplicatedStorage").MOB:FindFirstChild("Seasoned Fishman [Lv. 2200]").HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
									else
										game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1864.8843994140625, 51.35000991821289, 6721.7578125)
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end)
    end
end)

Boss = {}
for _,g in pairs(game:GetService("Workspace").Monster.Boss:GetChildren()) do
	if not g.Name then
		wait()
	else
		table.insert(Boss,g.Name)
	end
end

page1:Label("Boss")

local BossName = page1:Dropdown("Select Boss \n เลือก บอส",Boss,"",function(v)
	SelectBoss = v
end)

page1:Toggle('Auto Farm [Boss]',false,function(a)
    _G.AutoFarmBoss = a
    _G.Open = false
	_G.Start = a
end)

page1:Button('Refresh Boss \n รี เซ็ท',function(a)
    BossName:Clear()
	for _,g in pairs(game:GetService("Workspace").Monster.Boss:GetChildren()) do
		if not g.Name then
			wait()
		else
			BossName:Add(g.Name) 
		end
	end
end)

spawn(function()
	while task.wait() do
		pcall(function()
			if _G.AutoFarmBoss then
				if game:GetService("Workspace").Monster.Boss:FindFirstChild(SelectBoss) then
					for _,v in pairs(game:GetService("Workspace").Monster:GetDescendants()) do
						if v.Name == SelectBoss and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
							repeat task.wait();
								TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
								EquipWeapon(_G.Weapon)
								_G.Pos = v.HumanoidRootPart.CFrame
								_G.Open = true
							until not v.Parent or v.Humanoid.Health <= 0 or not _G.AutoFarmBoss
							return
						end
					end
				else
					_G.Open = false
				end
			end
		end)
	end
end)

page1:Label("Saber")

page1:Toggle("Auto Saber \n หา Saber",_G.AutoSaber,function(value)
    _G.AutoSaber = value
	_G.Start = value
    StopNoClip(_G.AutoSaber)
end)

page1:Toggle("Auto Saber Hop\n หา Saber ออก",_G.AutoSaber_Hop,function(value)
    _G.AutoSaber_Hop = value
end)

spawn(function()
    while wait() do
        if _G.AutoSaber then
            pcall(function()
                if game:GetService("Workspace").Monster.Boss:FindFirstChild("Expert Swordman [Lv. 3000]") then
                    for i,v in pairs(game:GetService("Workspace").Monster.Boss:GetChildren()) do
                        if v.Name == "Expert Swordman [Lv. 3000]" and v.Humanoid.Health > 0 then
                            repeat task.wait()
                                AutoHaki()
                                EquipWeapon(_G.SelectWeapon)
								_G.Open = true
                                TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
								_G.Pos = v.HumanoidRootPart.CFrame
                            until v.Humanoid.Health <= 0 or not _G.AutoSaber
							return
                        end
                    end
                else
					_G.Open = false
                    if game:GetService("ReplicatedStorage").MOB:FindFirstChild("Expert Swordman [Lv. 3000]") then
                        TP(game:GetService("ReplicatedStorage").MOB:FindFirstChild("Expert Swordman [Lv. 3000]").HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
                    else
                        if _G.AutoSaber_Hop then
                            Hop()
                        end
                    end
                end
            end)
        end
    end
end)

page1:Label("Ghost Ship")

page1:Toggle('Auto Farm [Ghost Ship] \n หา เรือผี',false,function(a)
    _G.GhostShip = a
	_G.Start = a
    _G.Open = false
end)

local CheckGhost = game.Workspace:FindFirstChild("GhostMonster")
spawn(function()
	while task.wait() do
		pcall(function()
			if _G.GhostShip then
				if CheckGhost then
					for _,v in pairs(CheckGhost:GetDescendants()) do
						if v.Name == "Ghost Ship" and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
							repeat task.wait();
								TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
								EquipWeapon(_G.Weapon)
								_G.Open = true
								_G.Pos = v.HumanoidRootPart.CFrame
							until not v.Parent or v.Humanoid.Health <= 0 or not _G.GhostShip
							return
						else
							_G.Open = false
							for _,k in pairs(game.Workspace:GetChildren())do 
								if k.Name:find("Chest")then 
									HaveChest=true
								end
							end
                   		if HaveChest then 
                           	for _,p in pairs(game.Workspace:GetChildren())do 
                           		 if p.Name:find("Chest")then 
                            		    TP(p.PrimaryPart.CFrame)
                                		wait(0.3)
                                	end
                           		end
                      	    end
						end
					end
				end
			end
		end)
	end
end)

page1:Label("Big Mom")

page1:Toggle('Auto Farm [Big Mom] \n หา บิ็ก มอม',false,function(a)
    _G.Ms_Mother = a
	_G.Start = a
    _G.Open = false
end)

spawn(function()
	while task.wait() do
		pcall(function()
			if _G.Ms_Mother then
				if game:GetService("Workspace").Monster.Boss:FindFirstChild("Ms. Mother [Lv.7500]") then
					for _,v in pairs(game:GetService("Workspace").Monster:GetDescendants()) do
						if v.Name == "Ms. Mother [Lv.7500]" and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
							repeat task.wait();
								TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
								EquipWeapon(_G.Weapon)
								_G.Pos = v.HumanoidRootPart.CFrame
								_G.Open = true
							until not v.Parent or v.Humanoid.Health <= 0 or not _G.Ms_Mother
							return
						end
					end
				else
					_G.Open = false
				end
			end
		end)
	end
end)

page1:Label("Kaido")

page1:Toggle('Auto Farm [Kaido] \n ตีไคโด',false,function(a)
    _G.Kaido = a
	_G.Start = a
    _G.Open = false
end)

spawn(function()
	while task.wait() do
		pcall(function()
			if _G.Kaido then
				if game:GetService("Workspace").Monster.Boss:FindFirstChild("Dragon [Lv. 5000]") then
					for _,v in pairs(game:GetService("Workspace").Monster:GetDescendants()) do
						if v.Name == "Dragon [Lv. 5000]" and v:FindFirstChild("Humanoid") and v:FindFirstChild("HumanoidRootPart") and v.Humanoid.Health > 0 then
							repeat task.wait();
								TP(v.HumanoidRootPart.CFrame * CFrame.Angles(math.rad(270), 0, 0) - Vector3.new(0,-_G.DistanceMob,0))
								EquipWeapon(_G.Weapon)
								_G.Pos = v.HumanoidRootPart.CFrame
								_G.Open = true
							until not v.Parent or v.Humanoid.Health <= 0 or not _G.Kaido
							return
						end
					end
				else
					_G.Open = false
				end
			end
		end)
	end
end)

page1:Label("Sea King")

page1:Toggle("Auto Sea King \n ตีเจ้า",false,function(value)
   _G.AutoSeaking = value
   _G.Start = value
end)
    
page1:Toggle("Auto Sea King Hop \n ตีเจ้า ออก",false,function(value)
    _G.AutoSeaking_Hop = value
end)

spawn(function()
    while wait() do
        if _G.AutoSeaking then
            pcall(function()
                for i,v in pairs(game:GetService("Workspace").SeaMonster:GetChildren()) do
                    if game:GetService("Workspace").SeaMonster:FindFirstChild("SeaKing") and v.Humanoid.Health > 0 then
                        if v.Name == "SeaKing" then
                            repeat task.wait()
								TP(v.HumanoidRootPart.CFrame * CFrame.new(0,0,_G.DistanceMob)) -- * CFrame.Angles(math.rad(-90),0,0))
								EquipWeapon(_G.Weapon)
								_G.Open = true
								_G.Pos = v.HumanoidRootPart.CFrame
                            until v.Humanoid.Health <= 0 or not _G.AutoSeaking or not game:GetService("Workspace").SeaMonster:FindFirstChild("SeaKing")             
							return          
                        end
                    else
						_G.Open = false
                        if game:GetService("Workspace").Island:FindFirstChild("Legacy Island1") then
                            TP(game:GetService("Workspace").Island:FindFirstChild("Legacy Island1").ChestSpawner.CFrame * CFrame.new(0,0,2))
                        elseif game:GetService("Workspace").Island:FindFirstChild("Legacy Island2") then
                            TP(game:GetService("Workspace").Island:FindFirstChild("Legacy Island2").ChestSpawner.CFrame * CFrame.new(0,0,2))
                        elseif game:GetService("Workspace").Island:FindFirstChild("Legacy Island3") then
                            TP(game:GetService("Workspace").Island:FindFirstChild("Legacy Island3").ChestSpawner.CFrame * CFrame.new(0,0,2))
                        elseif game:GetService("Workspace").Island:FindFirstChild("Legacy Island4") then
                            TP(game:GetService("Workspace").Island:FindFirstChild("Legacy Island4").ChestSpawner.CFrame * CFrame.new(0,0,2))
                        end
                    end
                end
            end)
        end
    end
end)

spawn(function()
    while wait() do
        if _G.AutoSeaking and _G.AutoSeaking_Hop then
            pcall(function()
                if not game:GetService("Workspace").SeaMonster:FindFirstChild("SeaKing") then
                    wait(3)
                    Hop()
                end
            end)
        end
    end
end)

_G.DistanceMob = 7
page2:Slider("DistanceMob \n ระยะทางม็อบ",true,0,40,7,function(v)
	_G.DistanceMob = v
end)


page2:Toggle('Auto Skill / ออโต้ สกิว',false,function(a)
    _G.Auto_Skill = a
end)

spawn(function()
	while wait(0.1) do 
		if _G.Auto_Skill and _G.Open then
			KeyBo(32)
			KeyBo("Z")
			UseSkill("Z",_G.Pos)
			wait(0.5)
			KeyBo("X")
			UseSkill("X",_G.Pos)
			wait(0.5)
			KeyBo("C")
			UseSkill("C",_G.Pos)
			wait(0.5)
			KeyBo("V")
			UseSkill("V",_G.Pos)
			wait(0.5)
			KeyBo("B")
			UseSkill("B",_G.Pos)
		end
	end
end)
	

page2:Toggle('Auto Buso Haki \n ออโต้ ฮาคิ',true,function(a)
    _G.BusoHaki = a
    _G.Open = false
end)

spawn(function()
    while wait(1) do
        if _G.BusoHaki then
            pcall(function()
                if game.Players.LocalPlayer.Character.Haki.Value == 0 then
                    game.Players.LocalPlayer.Character.Haki.Value = 1
                    game:GetService("Players").LocalPlayer.Character.Services.Client.Armament:FireServer()
                end
            end)
        end
    end
end)

local player = game:GetService("Players").LocalPlayer
	
function blackscreen(enable)
	local playerGui = player:WaitForChild("PlayerGui")
	if not enable then
		local sUi = playerGui:FindFirstChild("Blackscreen")
		if sUi then sUi:Destroy() end
		return
	elseif playerGui:FindFirstChild("Blackscreen") then
		return
	end
	local sUi = Instance.new("ScreenGui", playerGui)
	sUi.Name = "Blackscreen"
	sUi.DisplayOrder = -727

	local uiFrame = Instance.new("Frame", sUi)
	uiFrame.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
	uiFrame.Size = UDim2.new(0, 72727, 0, 72727)
	uiFrame.Position = UDim2.new(0, 0, -5, 0)
	--uiFrame.Respawn = true
end

page2:Toggle('Black Screen \n จอดำ',_G.StartBlackScreen,function(a)
	_G.StartBlackScreen = a
	if _G.StartBlackScreen then
		blackscreen(true)
		game:GetService("RunService"):Set3dRenderingEnabled(false)
	elseif _G.StartBlackScreen == false then
		blackscreen(false)
		game:GetService("RunService"):Set3dRenderingEnabled(true)
	end
end)

page2:Button("Redeem All Code \n ทุกโคด",function()
	local AllCode = {
		[1] = "2BVisits",
		[2] = "3xilescha1r",
		[3] = "1MLikes",
		[4] = "Update4.6YAY",
		[5] = "Thx4Waiting",
		[6] = "Peodiz",
		[7] = "DinoxLive",
		[8] = "2023"
   }
   for i = 1,100 do 
	   game:GetService("ReplicatedStorage"):WaitForChild("Remotes"):WaitForChild("Functions"):WaitForChild("redeemcode"):InvokeServer(AllCode[i])
   end
end)


local Tab2 = Win:CraftTab("Stats/TP","")

local page3 = Tab2:CraftPage("Stats",1)

page3:Label("Stats")

page3:MultiDropdown("Select Stats \n เลือก สเต็ด",{"Melee","Defense","Sword","Devil Fruit"},{"Melee","Defense"},function(v)
    _G.Select_Stats = v
end)

page3:Toggle('Auto Stats',false,function(a)
    Auto_Stats = a
    _G.Open = false
end)

spawn(function()
	while wait() do
		pcall(function()
			if Auto_Stats then
				for _,g in next, _G.Select_Stats do
    				game:GetService("Players").LocalPlayer.PlayerGui.Stats.Button.StatsFrame.RemoteEvent:FireServer(tostring(g),_G.PointStats)
					wait(0.5)
				end
			end
		end)
	end
end)

_G.PointStats = 1
page3:Slider("Point Stats \n เลือก พ้อย",true,0,40,1,function(v)
	_G.PointStats = v
end)

local page4 = Tab2:CraftPage("TP",2)

page4:Label("MaP")

Island = {}
for _,g in pairs(game:GetService("Workspace").Areas:GetChildren()) do
	table.insert(Island,g.Name)
end

page4:Dropdown("Select Island \n เลือก เกาะ",Island,{""},function(v)
	SelectIsland = v
end)

page4:Toggle('Tp \n ไป',false,function(a)
	if a then
  	  TP(game:GetService("Workspace").Areas[SelectIsland].CFrame * CFrame.new(0,500,0))
	end
end)

local Job = game.JobId
_G.JobID = Job
local tab6 = Win:CraftTab('Server')
local page12 = tab6:CraftPage('Server',1)

page12:Textbox("ID Job","",function(v)
	print(v)
	_G.JobID = v
end)

page12:Button("TP Job ID", function()
	game:GetService("TeleportService"):TeleportToPlaceInstance(game.PlaceId, _G.JobID, game:GetService("Players").LocalPlayer)
end)

page12:Button("Copy Job ID", function()
	setclipboard(Job)
end)

local page13 = tab6:CraftPage('Server',2)

page13:Button("rejoin", function()
	game:GetService("TeleportService"):TeleportToPlaceInstance(game.PlaceId, game.JobId, game:GetService("Players").LocalPlayer)
end)

page13:Button("Hop", function()
	Hop()
end)

------------------------------------------------------------------------------------------------------------
else
	print(".....")
end

